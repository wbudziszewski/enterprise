﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Budziszewski.Enterprise
{
    public class IsValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return (value.Equals(parameter));
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return false;
        }
    }

    public class IsNotValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return (!value.Equals(parameter));
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return false;
        }
    }

    public class BooleanToBackgroundColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return (bool)value ? new SolidColorBrush(Colors.Gray) : null; //new SolidColorBrush(System.Windows.SystemColors.WindowColor)
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class IntegerToBackgroundColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if ((int)parameter == 0 || (int)value == 0) return null;
            byte c = (byte)(255.0 - 128.0 * (int)value / (int)parameter);
            return new SolidColorBrush(Color.FromRgb(c, c, c));
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class BooleanToVisibilityFalseAsHiddenConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (Boolean.Parse(parameter.ToString()))
            {
                return ((bool)value ? System.Windows.Visibility.Hidden : System.Windows.Visibility.Visible);
            }
            else
            {
                return ((bool)value ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden);
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return false;
        }
    }

    public class BooleanToVisibilityFalseAsCollapsedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (Boolean.Parse(parameter.ToString()))
            {
                return ((bool)value ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible);
            }
            else
            {
                return ((bool)value ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed);
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return false;
        }
    }

    public class BooleanToFontWeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return ((bool)value ? System.Windows.FontWeights.Bold : System.Windows.FontWeights.Normal);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
