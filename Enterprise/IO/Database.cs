﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace Budziszewski.Enterprise
{
    /// <summary>
    /// Handles data management of the program. Database holds persistent values which are read for the purpose of valuation.
    /// </summary>
    public class Database
    {
        /// <summary>
        /// Default location of the database on the disk in Application Data folder, used if /local switch is not set.
        /// </summary>
        private static readonly string RoamingDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData, Environment.SpecialFolderOption.Create), "Enterprise", "Data");

        /// <summary>
        /// Alternative location of the database on the disk, along with the program files, used if /local switch is set.
        /// </summary>
        private static readonly string LocalDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data");

        /// <summary>
        /// Accesses currently valid location of the database on the disk.
        /// </summary>
        private static string Dir
        {
            get
            {
                return Environment.GetCommandLineArgs().Count(x => x.ToLower() == "/local") > 0 ? LocalDir : RoamingDir;
            }
        }

        /// <summary>
        /// Gets an existing database data object.
        /// </summary>
        /// <param name="type">Type of data contained under this database object</param>
        /// <param name="id">String identifier of the database object</param>
        public static IEnumerable<T> Get<T>() where T : DataPoint
        {
            string path;
            string id = typeof(T).Name;
            path = GetLastFile(Dir, id + "_*.xml");
            if (path == null || !File.Exists(path))
            {
                return Enumerable.Empty<T>();
            }
            try
            {
                return Deserialize<T>(path);
            }
            catch
            {
                return Enumerable.Empty<T>();
            }
        }

        /// <summary>
        /// Replaces existing database data with a new object.
        /// </summary>
        /// <param name="type">Type of data contained under this database object</param>
        /// <param name="id">String identifier of the database object</param>
        /// <param name="o">New data</param>
        public static void Set<T>(IEnumerable<DataPoint> o) where T : DataPoint
        {
            DateTime timestamp = DateTime.Now;
            string ts = timestamp.ToString("yyyyMMddHHmmss");
            string id = typeof(T).Name;

            Database.Invalidate<T>();

            string path = Path.Combine(Dir, id + "_" + ts + ".xml");
            try
            {
                Serialize<T>(o.Cast<T>().ToArray(), path);
            }
            catch (Exception ex)
            {
                Log.Report(Severity.CriticalError, "Cannot serialize database object to: <" + path + ">.", o, ex);
            }
        }

        /// <summary>
        /// Cleans up database, i.e. deletes any file of a given database object older than the latest one.
        /// </summary>
        public static void Purge()
        {
            var ids = GetListOfIDs(Dir);
            if (ids == null) return;
            foreach (var id in ids)
            {
                var paths = GetOldFiles(Dir, id + "_*.xml");
                foreach (var path in paths)
                {
                    File.Delete(path);
                }
            }
        }

        /// <summary>
        /// Serializes object into a file.
        /// </summary>
        /// <param name="obj">Data to be serialized</param>
        private static void Serialize<T>(object obj, string path) where T : DataPoint
        {
            if (obj == null || path == null) return;
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            if (!Directory.Exists(Dir))
                Directory.CreateDirectory(Dir);
            using (TextWriter tw = new StreamWriter(path))
            {
                serializer.Serialize(tw, obj);
            }
        }

        /// <summary>
        /// Deserializes object from a file.
        /// </summary>
        /// <returns>Data deserialized into an object</returns>
        private static T[] Deserialize<T>(string path) where T : DataPoint
        {
            if (path == null || !File.Exists(path)) return null;
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(T[]));
                using (TextReader reader = new StreamReader(path))
                {
                    var l = (T[])deserializer.Deserialize(reader);
                    return l;
                }
            }
            catch (Exception ex)
            {
                Log.Report(Severity.CriticalError, "Cannot deserialize database object from: <" + path + ">.", null, ex);
                return null;
            }
        }

        /// <summary>
        /// Gets the last (alphabetically) file in the path.
        /// </summary>
        /// <param name="path">Path which will be searched for files</param>
        /// <param name="filter">Filter to applied to files names</param>
        /// <returns></returns>
        private static string GetLastFile(string path, string filter)
        {
            if (!Directory.Exists(path)) return null;
            var files = Directory.GetFiles(path, filter); // Directory.GetFiles(Dir, id + "_*.dat");
            if (files == null || files.Length == 0) return null;
            Array.Sort(files);
            return files.Last();
        }

        /// <summary>
        /// Gets all but the last (alphabetically) file in the path.
        /// </summary>
        /// <param name="path">Path which will be searched for files</param>
        /// <param name="filter">Filter to applied to files names</param>
        /// <returns></returns>
        private static IEnumerable<string> GetOldFiles(string path, string filter)
        {
            if (!Directory.Exists(path)) return null;
            var files = Directory.GetFiles(path, filter); // Directory.GetFiles(Dir, id + "_*.dat");
            if (files == null || files.Length <= 1) return new string[] { };
            Array.Sort(files);
            return files.Take(files.Length - 1);
        }

        /// <summary>
        /// Gets list of unique identifiers of database objects.
        /// </summary>
        /// <param name="path">Path where to search for the database objects</param>
        /// <returns>List of unique identifiers</returns>
        private static IEnumerable<string> GetListOfIDs(string path)
        {
            if (!Directory.Exists(path)) return null;
            var files = Directory.GetFiles(path, "*.xml"); // Directory.GetFiles(Dir, id + "_*.dat");
            if (files == null || files.Length == 0) return new string[] { };
            List<string> ids = new List<string>();
            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    string filename = Path.GetFileName(files[i]);
                    ids.Add(filename.Substring(0, filename.IndexOf("_")));
                }
                catch
                {
                }
            }
            return ids.Distinct();
        }

        /// <summary>
        /// Database objects which are read into memory.
        /// </summary>
        private static Dictionary<Type, IEnumerable<DataPoint>> databaseObjects = new Dictionary<Type, IEnumerable<DataPoint>>();

        private static IEnumerable<T> GetDatabaseObject<T>() where T : DataPoint
        {
            if (!databaseObjects.ContainsKey(typeof(T)))
            {
                databaseObjects[typeof(T)] = Database.Get<T>();
            }
            return (IEnumerable<T>)(databaseObjects[typeof(T)]);
        }

        public static IEnumerable<Definitions.Price> Prices
        {
            get { return GetDatabaseObject<Definitions.Price>(); }
        }

        public static IEnumerable<Definitions.Coupon> Coupons
        {
            get { return GetDatabaseObject<Definitions.Coupon>(); }
        }

        public static IEnumerable<Definitions.Instrument> Instruments
        {
            get { return GetDatabaseObject<Definitions.Instrument>(); }
        }

        public static IEnumerable<Definitions.BookAccount> BookAccounts
        {
            get { return GetDatabaseObject<Definitions.BookAccount>(); }
        }

        public static IEnumerable<Definitions.Dividend> Dividends
        {
            get { return GetDatabaseObject<Definitions.Dividend>(); }
        }

        public static IEnumerable<Definitions.Transaction> Transactions
        {
            get { return GetDatabaseObject<Definitions.Transaction>(); }
        }

        public static IEnumerable<Definitions.CurveNode> CurveNodes
        {
            get { return GetDatabaseObject<Definitions.CurveNode>(); }
        }

        public static IEnumerable<Definitions.AccountingScheme> SchemeAccounts
        {
            get { return GetDatabaseObject<Definitions.AccountingScheme>(); }
        }

        static Dictionary<(DateTime, string, string), Financial.Curve> curves = new Dictionary<(DateTime, string, string), Financial.Curve>();

        public static Financial.Curve GetCurve(DateTime date, string currency, string label = "")
        {
            if (!curves.TryGetValue((date, currency, label), out Financial.Curve c))
            {
                var lastDate = CurveNodes.LastOrDefault(x => x.Date <= date && x.Currency == currency && (label == "" && String.IsNullOrEmpty(x.Label) || x.Label == label));
                if (lastDate == null) return null;

                var nodes = CurveNodes.Where(x => x.Date == lastDate.Date && x.Currency == currency && (label == "" && String.IsNullOrEmpty(x.Label) || x.Label == label));
                if (nodes == null || nodes.Count() == 0) return null;
                c = new Financial.Curve(Financial.CurveModelType.Linear, String.IsNullOrEmpty(label) ? String.Format("{0}_{1}", currency, date.ToString("yyyyMMdd")) : String.Format("{0}_{1}_{2}", currency, label, date.ToString("yyyyMMdd")));
                curves.Add((date, currency, label), c);
            }
            return c;
        }

        public static double GetFXRate(string currency1, string currency2, DateTime date)
        {
            if (currency1 == currency2) return 1.0;
            return Prices.Where(x => x.IsFXRate).FirstOrDefault(x => x.ID == (currency1 + currency2))?.Value ?? 0.0;
        }

        public static double GetFXRate(string currency, DateTime date)
        {
            return GetFXRate(currency, Common.DomesticCurrency, date);
        }

        public static Definitions.Price GetPrice(Investments.StandardizedSecurity inv, DateTime date)
        {
            return Prices.LastOrDefault(x => (x.InstrumentID == inv.ID) && x.Date <= date);
        }

        public static void Invalidate()
        {
            databaseObjects.Clear();
            Common.ResetBooks();
        }

        public static void Invalidate<T>() where T : DataPoint
        {
            databaseObjects.Remove(typeof(T));
            Common.ResetBooks();
        }
    }
}
