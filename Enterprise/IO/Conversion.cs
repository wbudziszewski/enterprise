﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise
{
    public static class Conversion
    {
        public static decimal ToDecimal(string input)
        {
            if (input == "" || input == null) return 0m;
            if (Decimal.TryParse(input, NumberStyles.Any, CultureInfo.CurrentCulture, out decimal o1))
            {
                return o1;
            }
            if (Decimal.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal o2))
            {
                return o2;
            }
            throw new Exception("Error converting string value to Decimal.");
        }

        public static double ToDouble(string input)
        {
            if (input == "" || input == null) return 0.0;
            if (Double.TryParse(input, NumberStyles.Any, CultureInfo.CurrentCulture, out double o1))
            {
                return o1;
            }
            if (Double.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out double o2))
            {
                return o2;
            }
            if (input.EndsWith("%"))
            {
                // percentage value
                input = input.TrimEnd('%', ' ');
                if (Double.TryParse(input, NumberStyles.Any, CultureInfo.CurrentCulture, out double o3))
                {
                    return Math.Round(o3 / 100.0, 12);
                }
                return o3;
            }
            throw new Exception("Error converting string value to Double.");
        }

        public static float ToSingle(string input)
        {
            if (input == "" || input == null) return 0f;
            if (Single.TryParse(input, NumberStyles.Any, CultureInfo.CurrentCulture, out float o1))
            {
                return o1;
            }
            if (Single.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out float o2))
            {
                return o2;
            }
            throw new Exception("Error converting string value to Single.");
        }

        public static long ToInt64(string input)
        {
            if (input == "" || input == null) return 0;
            if (Int64.TryParse(input, NumberStyles.Any, CultureInfo.CurrentCulture, out long o1))
            {
                return o1;
            }
            if (Int64.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out long o2))
            {
                return o2;
            }
            throw new Exception("Error converting string value to Int64.");
        }

        public static int ToInt32(string input)
        {
            if (input == "" || input == null) return 0;
            if (Int32.TryParse(input, NumberStyles.Any, CultureInfo.CurrentCulture, out int o1))
            {
                return o1;
            }
            if (Int32.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out int o2))
            {
                return o2;
            }
            throw new Exception("Error converting string value to Int32.");
        }

        public static DateTime ToDateTime(string input, string format, CultureInfo ci)
        {
            if (input == "" || input == null) return new DateTime();
            if (DateTime.TryParseExact(input, format, ci, DateTimeStyles.None, out DateTime o))
            {
                return o;
            }
            throw new Exception("Error converting string value to DateTime.");
        }

        public static DateTime? ToDateTimeOrNull(string input)
        {
            if (input == "" || input == null) return null;
            if (DateTime.TryParseExact(input, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o1))
            {
                return o1;
            }
            else if (DateTime.TryParseExact(input, "yyyy-MM-d", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o2))
            {
                return o2;
            }
            else if (DateTime.TryParseExact(input, "yyyy-M-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o3))
            {
                return o3;
            }
            else if (DateTime.TryParseExact(input, "yyyy-M-d", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o4))
            {
                return o4;
            }
            else if (DateTime.TryParseExact(input, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o5))
            {
                return o5;
            }
            else if (DateTime.TryParseExact(input, "dd.M.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o6))
            {
                return o6;
            }
            else if (DateTime.TryParseExact(input, "d.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o7))
            {
                return o7;
            }
            else if (DateTime.TryParseExact(input, "d.M.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o8))
            {
                return o8;
            }
            else if (DateTime.TryParseExact(input, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime o9))
            {
                return o9;
            }
            return null;
        }

        public static DateTime ToDateTime(string input)
        {
            return ToDateTimeOrNull(input) ?? throw new Exception("Error converting string value to DateTime.");
        }

        public static bool ToBoolean(string input)
        {
            if (input == "" || input == null) return false;
            if (Boolean.TryParse(input, out bool o))
            {
                return o;
            }
            else if (input.Trim() == "1")
            {
                return true;
            }
            else if (input.Trim() == "0")
            {
                return false;
            }
            throw new Exception("Error converting string value to Boolean.");
        }

        public static string ToString(object input)
        {
            if (input == null) return "";
            if (input is DateTime)
            {
                return ((DateTime)input).ToString("yyyy-MM-dd");
            }
            return input.ToString();
        }
    }
}
