﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise
{
    public class CSV
    {
        public string Path { get; private set; }

        public string Name { get; private set; }

        public string[] Headers { get; private set; }

        public List<string[]> Lines { get; private set; }

        public string Delimiter { get; set; }

        public CSV()
        {
            Path = "";
            Name = "";
            Delimiter = ";";
        }

        public CSV(string path)
        {
            Path = path;
            Name = System.IO.Path.GetFileNameWithoutExtension(path);
            Delimiter = ";";
        }

        public async System.Threading.Tasks.Task ReadAsync(string contents)
        {
            await System.Threading.Tasks.Task.Run(() => Read(contents));
        }

        public void Read(string contents)
        {
            List<string> headers = new List<string>();
            List<string[]> lines = new List<string[]>();

            try
            {
                if (contents.IndexOf('\n') >= 0)
                {
                    Delimiter = GuessDelimiter(contents.Substring(0, contents.IndexOf('\n'))).ToString();
                }
                else
                {
                    Delimiter = GuessDelimiter(contents).ToString();
                }
            }
            catch
            {
                throw new Exception("Error reading contents.");
            }

            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(contents));

            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(ms))
            {
                try
                {
                    parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                    parser.SetDelimiters(Delimiter);
                    // headers
                    if (!parser.EndOfData)
                    {
                        //Processing row
                        headers = parser.ReadFields().ToList();
                    }
                    // cells
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string[] fields = parser.ReadFields();
                        lines.Add(fields);
                    }
                }
                catch
                {
                    throw new Exception("Error reading file.");
                }
            }

            Headers = headers.ToArray();
            Lines = lines;
        }

        public async System.Threading.Tasks.Task ReadAsync()
        {
            await System.Threading.Tasks.Task.Run(() => Read());
        }

        public void Read()
        {
            if (!(File.Exists(Path)))
            {
                throw new IOException("File not exists.");
            }

            List<string> headers = new List<string>();
            List<string[]> lines = new List<string[]>();
            string line;

            using (StreamReader sr = new StreamReader(new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                try
                {
                    if ((line = sr.ReadLine()) != null)
                    {
                        Delimiter = GuessDelimiter(line).ToString();
                    }
                }
                catch
                {
                    throw new Exception("Error reading file.");
                }
            }

            using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(Path))
            {
                try
                {
                    parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                    parser.SetDelimiters(Delimiter);
                    // headers
                    if (!parser.EndOfData)
                    {
                        //Processing row
                        headers = parser.ReadFields().ToList();
                    }
                    // cells
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string[] fields = parser.ReadFields();
                        lines.Add(fields);
                    }
                }
                catch
                {
                    throw new Exception("Error reading file.");
                }
            }

            Headers = headers.ToArray();
            Lines = lines;
        }

        public void Prepare(IEnumerable data)
        {
            if (data == null || (data.Cast<object>()).Count() == 0) return;
            Type type = data.Cast<object>().ElementAt(0).GetType();

            var typeProperties = type.GetProperties().Where(x => x.IsExternal()).ToArray();
            Headers = new string[typeProperties.Length];

            for (int i = 0; i < Headers.Length; i++)
            {
                Headers[i] = typeProperties[i].Name;
            }

            Lines = new List<string[]>();
            foreach (var item in data)
            {
                string[] line = new string[Headers.Length];
                for (int j = 0; j < typeProperties.Length; j++)
                {
                    line[j] = Conversion.ToString(typeProperties[j].GetValue(item));
                }
                Lines.Add(line);
            }

            this.Delimiter = "\t";
        }

        public void Prepare<T>(IEnumerable<T> data)
        {
            var typeProperties = typeof(T).GetProperties().Where(x => x.IsExternal()).ToArray();
            Headers = new string[typeProperties.Length];

            for (int i = 0; i < Headers.Length; i++)
            {
                Headers[i] = typeProperties[i].Name;
            }

            Lines = new List<string[]>();
            for (int i = 0; i < data.Count(); i++)
            {
                string[] line = new string[Headers.Length];
                for (int j = 0; j < typeProperties.Length; j++)
                {
                    line[j] = Conversion.ToString(typeProperties[j].GetValue(data.ElementAt(i)));
                }
                Lines.Add(line);
            }
        }

        public void Write()
        {
            if (Path == "" || Path == null) throw new IOException("Path not specified for writing.");
            Write(Path);
        }

        public void Write(string path)
        {
            using (StreamWriter sw = new StreamWriter(new FileStream(Path, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)))
            {
                try
                {
                    // Write headers
                    for (int i = 0; i < Headers.Length; i++)
                    {
                        sw.Write(Headers[i]);
                        if (i < Headers.Length - 1) sw.Write(Delimiter);
                    }
                    sw.WriteLine();

                    // Write cells
                    for (int i = 0; i < Lines.Count; i++)
                    {
                        for (int j = 0; j < Lines[i].Length; j++)
                        {
                            sw.Write(Lines[i][j]);
                            if (j < Lines[i].Length - 1) sw.Write(Delimiter);
                        }
                        sw.WriteLine();
                    }
                }
                catch
                {
                    throw new Exception("Error writing to file.");
                }
            }
        }

        public IEnumerable<T> Interpret<T>(bool autoNumbering = false) where T : DataPoint
        {
            PropertyInfo[] pi = GetFieldProperties<T>();

            long index = 0;
            for (int i = 0; i < Lines.Count; i++)
            {
                T newItem = null;
                //if (typeIndex >= 0)
                //{
                //    try
                //    {
                //        var types = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsSubclassOf(typeof(DataPoint)));
                //        Type type = types.SingleOrDefault(x => String.Compare(x.Name, Lines[i][typeIndex], true) == 0);
                //        if (autoNumbering)
                //            newItem = (T)(type.GetConstructor(new Type[] { typeof(long) }).Invoke(new object[] { ++index }));
                //        else
                //            newItem = (T)(type.GetConstructor(new Type[] { }).Invoke(new object[] { }));
                //    }
                //    catch (Exception ex)
                //    {
                //        Log.Report(Severity.Error, "Error indentifying type of DataPoint in an external file: <" + Path + ">.", Lines[i][typeIndex], ex);
                //    }
                //}
                //else
                //{
                //newItem = (T)(typeof(T).GetConstructor(new Type[] { }).Invoke(new object[] { }));

                if (autoNumbering)
                    newItem = (T)(typeof(T).GetConstructor(new Type[] { typeof(long) }).Invoke(new object[] { ++index }));
                else
                    newItem = (T)(typeof(T).GetConstructor(new Type[] { }).Invoke(new object[] { }));

                if (newItem == null) continue;
                newItem.ImportFromCSV(Lines[i], pi);

                yield return newItem;
            }
        }


        public void CopyToClipboard()
        {
            StringBuilder sb = new StringBuilder();

            // Write headers
            for (int i = 0; i < Headers.Length; i++)
            {
                sb.Append(Headers[i]);
                if (i < Headers.Length - 1) sb.Append('\t');
            }
            sb.AppendLine();

            // Write cells
            for (int i = 0; i < Lines.Count; i++)
            {
                for (int j = 0; j < Lines[i].Length; j++)
                {
                    sb.Append(Lines[i][j]);
                    if (j < Lines[i].Length - 1) sb.Append('\t');
                }
                sb.AppendLine();
            }

            System.Windows.Clipboard.SetText(sb.ToString());
        }

        PropertyInfo[] GetFieldProperties<T>() where T : DataPoint
        {
            PropertyInfo[] properties = new PropertyInfo[Headers.Length];
            PropertyInfo[] allProperties = typeof(T).GetProperties().Where(x => x.IsExternal()).ToArray();

            for (int i = 0; i < properties.Length; i++)
            {
                try
                {
                    properties[i] = allProperties.First(x => x.HasHeader(Headers[i]));
                }
                catch (Exception ex)
                {
                    Log.Report(Severity.Warning, "Cannot identify field by its header (" + Headers[i] + ") in CSV file: <" + Path + ">", null, ex);
                }
            }

            return properties;
        }

        private char GuessDelimiter(string text)
        {
            char d = ';';
            bool insideQuote = false;

            // if there is unquoted semicolon or tab character in text, it always prevails as a delimiter
            // colon is taken as delimiter only unless there is no semicolon or tab present
            // semicolon is default delimiter if no character is found 

            foreach (char c in text)
            {
                if (c == '"') insideQuote = !insideQuote;
                if (insideQuote) continue;
                if (c == ';') { d = ';'; return d; }
                if (c == '\t') { d = '\t'; return d; }
                if (c == ',') d = ',';
            }

            return d;
        }
    }
}
