﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Budziszewski.Enterprise
{
    /// <summary>
    /// Manages checking objects properties if they meet specific conditions set.
    /// </summary>
    public class ConditionEngine
    {
        private static char[] specialChars = new char[] { '!', '=', '<', '>', '(', ')', '&', '|', '^' };
        private static string[] specialStrings = new string[] { "!", "!=", "=", "==", ">=", ">", "<=", "<", "(", ")", "&", "|", "^" };

        public bool IgnoreCase { get; set; }

        private Queue<Token> Queue = new Queue<Token>();

        /// <summary>
        /// Creates a new conditions set.
        /// </summary>
        /// <param name="input">Conditions to be checked</param>
        public ConditionEngine(string input)
        {
            List<(string text, bool escaped)> strings = new List<(string, bool)>();
            List<Token> tokens = new List<Token>();

            int idx = 0;
            int start = 0;
            while (idx < input.Length)
            {
                if (input[idx] == '\"')
                {
                    if (start != -1) strings.Add((input.Substring(start, idx - start).Trim(), false));

                    idx++;
                    start = idx;
                    while (input[idx] != '\"')
                    {
                        idx++;
                    }
                    strings.Add((input.Substring(start, idx - start - 1).Trim(), true));
                    idx++;
                    continue;
                }
                if (specialChars.Any(x => x == input[idx]))
                {
                    if (start != -1) strings.Add((input.Substring(start, idx - start).Trim(), false));

                    start = idx;
                    while (specialChars.Any(x => x == input[idx]))
                    {
                        idx++;
                        strings.Add((input.Substring(start, idx - start).Trim(), false));
                    }
                    start = idx;
                    continue;
                }
                idx++;
            }
            if (idx >= start) strings.Add((input.Substring(start, idx - start).Trim(), false));

            int depth = 0;
            for (int i = 0; i < strings.Count; i++)
            {
                if (strings[i].text == "!")
                {
                    tokens.Add(new UnaryNegation(i, depth));
                }
                else if (strings[i].text == "!=")
                {
                    tokens.Add(new NonEqualityOperator(i, depth));
                }
                else if (strings[i].text == "=" || strings[i].text == "==")
                {
                    tokens.Add(new EqualityOperator(i, depth));
                }
                else if (strings[i].text == ">=")
                {
                    tokens.Add(new GreaterThanOrEqualOperator(i, depth));
                }
                else if (strings[i].text == ">")
                {
                    tokens.Add(new GreaterThanOperator(i, depth));
                }
                else if (strings[i].text == "<=")
                {
                    tokens.Add(new LessThanOrEqualOperator(i, depth));
                }
                else if (strings[i].text == "<")
                {
                    tokens.Add(new LessThanOperator(i, depth));
                }
                else if (strings[i].text == "(")
                {
                    depth++;
                }
                else if (strings[i].text == ")")
                {
                    depth--;
                }
                else if (strings[i].text == "&" || strings[i].text == "&&")
                {
                    tokens.Add(new AndOperator(i, depth));
                }
                else if (strings[i].text == "|" || strings[i].text == "||")
                {
                    tokens.Add(new OrOperator(i, depth));
                }
                else if (strings[i].text == "^" || strings[i].text == "^^")
                {
                    tokens.Add(new XorOperator(i, depth));
                }
                else if (!strings[i].escaped && strings[i].text.StartsWith("$"))
                {
                    tokens.Add(new Variable(i, depth, strings[i].text.TrimStart('$')));
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(strings[i].text))
                    {
                        tokens.Add(new Constant(i, depth, strings[i].text));
                    }
                }
            }

            if (tokens.Count == 0)
            {
                return;
            }
            // Establish order of calculation
            do
            {
                depth = tokens.Max(x => x.Depth);
                var level = tokens.Where(x => x.Depth == depth);

                bool found = false;
                IEnumerable<Token> solvable = Enumerable.Empty<Token>();

                if (!found)
                {
                    solvable = level.Where(x => x is UnaryNegation).Where(x => x.CheckSolvability(level));
                    if (solvable.Count() > 0)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    solvable = level.Where(x => x is EqualityOperator || x is NonEqualityOperator || x is GreaterThanOrEqualOperator || x is GreaterThanOperator || x is LessThanOrEqualOperator || x is LessThanOperator).Where(x => x.CheckSolvability(level));
                    if (solvable.Count() > 0)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    solvable = level.Where(x => x is AndOperator || x is OrOperator || x is XorOperator).Where(x => x.CheckSolvability(level));
                    if (solvable.Count() > 0)
                    {
                        found = true;
                    }
                }
                if (solvable.Count() == 0 || !found)
                {
                    solvable = level;
                }

                var s = solvable.ElementAt(0);
                s.FreezeReferences(tokens);
                tokens.ForEach(x => x.ReplaceReferences(s));
                Queue.Enqueue(s);
                tokens.Remove(s);
                tokens.Add(new Result(s.ID, s.Depth));
            } while (tokens.Count > 1);
        }

        /// <summary>
        /// Solves the conditions set against the given object and its properties.
        /// </summary>
        /// <param name="obj">Object to be checked</param>
        /// <returns>True if specified conditions in the condition set are met for the given object, otherwise false.</returns>
        public bool Solve(object obj)
        {
            if (Queue.Count == 0) return true;
            Queue<Token> q = new Queue<Token>(Queue);
            while (q.Count > 1)
            {
                var step = q.Dequeue();
                int id = step.ID;
                bool result = step.Solve(obj, IgnoreCase);
                foreach (var e in q)
                {
                    e.EnterResult(id, result);
                }
            }

            return q.Dequeue().Solve(obj, IgnoreCase);
        }

        private bool IsToken(string input)
        {
            return specialStrings.Any(x => x == input);
        }
    }

    /// <summary>
    /// Represents a part of the conditions set (name of property, number or string).
    /// </summary>
    internal abstract class Token
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        protected internal int ID { get; protected set; }

        /// <summary>
        /// Depth in order of operations tree
        /// </summary>
        internal int Depth { get; private set; }

        /// <summary>
        /// Creates a new token
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="depth">Depth in order of operations tree</param>
        internal Token(int id, int depth)
        {
            ID = id;
            Depth = depth;
        }

        internal abstract bool CheckSolvability(IEnumerable<Token> set);

        internal abstract void FreezeReferences(List<Token> set);

        internal abstract void ReplaceReferences(Token token);

        internal abstract void EnterResult(int id, bool result);

        internal abstract bool Solve(object obj, bool ignoreCase);

        protected object GetValue(object obj, Variable v)
        {
            if (obj == null) return null;
            var pi = obj.GetType().GetProperty(v.Identifier);
            if (pi != null)
            {
                return pi.GetValue(obj);
            }
            var fi = obj.GetType().GetField(v.Identifier);
            if (fi != null)
            {
                return fi.GetValue(obj);
            }
            return null;
        }

        protected static bool IsSignedInteger(object obj)
        {
            if (Equals(obj, null)) { return false; }
            var t = obj.GetType();
            return t == typeof(short) || t == typeof(int) || t == typeof(long);
        }

        protected static bool IsUnsignedInteger(object obj)
        {
            if (Equals(obj, null)) { return false; }
            var t = obj.GetType();
            return t == typeof(byte) || t == typeof(ushort) || t == typeof(uint) || t == typeof(ulong);
        }

        protected static bool IsFloat(object obj)
        {
            if (Equals(obj, null)) { return false; }
            var t = obj.GetType();
            return t == typeof(float) || t == typeof(double) || t == typeof(decimal);
        }

        protected static bool IsEnum(object obj)
        {
            if (Equals(obj, null)) { return false; }
            var t = obj.GetType();
            return t.IsEnum;
        }
    }

    internal abstract class Operator : Token
    {
        internal Operator(int id, int depth) : base(id, depth)
        { }
    }

    internal abstract class UnaryOperator : Operator
    {
        internal Token Argument { get; set; }

        internal UnaryOperator(int id, int depth) : base(id, depth)
        {
            Argument = null;
        }

        internal override bool CheckSolvability(IEnumerable<Token> set)
        {
            if (Argument == null)
            {
                var arg = set.FirstOrDefault(x => x.ID > ID);
                if (arg is Variable || arg is Constant || arg is Result)
                {
                    Argument = arg;
                }
            }
            return (Argument is Variable || Argument is Constant);
        }

        internal override void FreezeReferences(List<Token> set)
        {
            set.Remove(Argument);
        }

        internal override void ReplaceReferences(Token token)
        {
            if (Argument != null && Argument.ID == token.ID)
            {
                Argument = token;
            }
        }

        internal override void EnterResult(int id, bool result)
        {
            if (Argument is Result r)
            {
                if (r.ID == id) r.Value = result;
            }
        }
    }

    internal class UnaryNegation : UnaryOperator
    {
        internal UnaryNegation(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            if (Argument is Result r)
            {
                return !(r.Value);
            }
            else if (Argument is Constant c)
            {
                Boolean.TryParse(c.Value, out bool b);
                return !b;
            }
            else if (Argument is Variable v)
            {
                try
                {
                    object o = GetValue(obj, v);
                    if (o is bool) return !(bool)o;
                    if (o is string)
                    {
                        Boolean.TryParse((string)o, out bool b);
                        return !b;
                    }
                }
                catch
                {
                    throw new Exception(String.Format("Cannot perform UnaryNegation on Variable: {0} in object: {1}", v, obj));
                }
            }
            return false;
        }
    }

    internal abstract class BinaryOperator : Operator
    {
        internal Token LHSArgument { get; set; }
        internal Token RHSArgument { get; set; }

        internal BinaryOperator(int id, int depth) : base(id, depth)
        {
            LHSArgument = null;
            RHSArgument = null;
        }

        internal override bool CheckSolvability(IEnumerable<Token> set)
        {
            if (LHSArgument == null)
            {
                var arg = set.LastOrDefault(x => x.ID < ID);
                if (arg is Variable || arg is Constant || arg is Result)
                {
                    LHSArgument = arg;
                }
            }

            if (RHSArgument == null)
            {
                var arg = set.FirstOrDefault(x => x.ID > ID);
                if (arg is Variable || arg is Constant || arg is Result)
                {
                    RHSArgument = arg;
                }
            }
            return (LHSArgument is DataToken && RHSArgument is DataToken);
        }

        internal override void FreezeReferences(List<Token> set)
        {
            set.Remove(LHSArgument);
            set.Remove(RHSArgument);
        }

        internal override void ReplaceReferences(Token token)
        {
            if (LHSArgument != null && LHSArgument.ID == token.ID)
            {
                LHSArgument = token;
            }
            if (RHSArgument != null && RHSArgument.ID == token.ID)
            {
                RHSArgument = token;
            }
        }

        internal override void EnterResult(int id, bool result)
        {
            if (LHSArgument is Result r1)
            {
                if (r1.ID == id) r1.Value = result;
            }
            if (RHSArgument is Result r2)
            {
                if (r2.ID == id) r2.Value = result;
            }
        }

        protected (object lhs, object rhs) PrepareArguments(object obj)
        {
            object lhs = null;
            object rhs = null;

            if (LHSArgument is Result lr)
            {
                lhs = lr.Value;
            }
            else if (LHSArgument is Constant lc)
            {
                lhs = lc.Value;
            }
            else if (LHSArgument is Variable lv)
            {
                if (obj.GetType().GetProperty(lv.Identifier) != null)
                { lhs = obj.GetType().GetProperty(lv.Identifier).GetValue(obj); }
                else if (obj.GetType().GetField(lv.Identifier) != null)
                { lhs = obj.GetType().GetField(lv.Identifier).GetValue(obj); }
                else { lhs = null; }
                if (IsEnum(lhs))
                {
                    lhs = Enum.GetName(lhs.GetType(), lhs);
                }
            }

            if (RHSArgument is Result rr)
            {
                rhs = rr.Value;
            }
            else if (RHSArgument is Constant rc)
            {
                rhs = rc.Value;
            }
            else if (RHSArgument is Variable rv)
            {
                if (obj.GetType().GetProperty(rv.Identifier) != null)
                { rhs = obj.GetType().GetProperty(rv.Identifier).GetValue(obj); }
                else if (obj.GetType().GetField(rv.Identifier) != null)
                { rhs = obj.GetType().GetField(rv.Identifier).GetValue(obj); }
                else { rhs = null; }
                if (IsEnum(rhs))
                {
                    rhs = Enum.GetName(lhs.GetType(), lhs);
                }
            }

            return (lhs, rhs);
        }
    }

    internal class AndOperator : BinaryOperator
    {
        public AndOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is bool && rhs is bool) return (bool)lhs && (bool)rhs;
            if (lhs is string && rhs is string)
            {
                Boolean.TryParse((string)lhs, out bool b1);
                Boolean.TryParse((string)rhs, out bool b2);
                return b1 && b2;
            }
            return false;
        }
    }

    internal class OrOperator : BinaryOperator
    {
        internal OrOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is bool && rhs is bool) return (bool)lhs || (bool)rhs;
            if (lhs is string && rhs is string)
            {
                Boolean.TryParse((string)lhs, out bool b1);
                Boolean.TryParse((string)rhs, out bool b2);
                return b1 || b2;
            }
            return false;
        }
    }

    internal class XorOperator : BinaryOperator
    {
        public XorOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is bool && rhs is bool) return (bool)lhs ^ (bool)rhs;
            if (lhs is string && rhs is string)
            {
                Boolean.TryParse((string)lhs, out bool b1);
                Boolean.TryParse((string)rhs, out bool b2);
                return b1 ^ b2;
            }
            return false;
        }
    }

    internal class EqualityOperator : BinaryOperator
    {
        public EqualityOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is bool && rhs is bool) return (bool)lhs == (bool)rhs;
            if (lhs is string && rhs is string)
            {
                if (decimal.TryParse((string)lhs, out decimal lhsd) && decimal.TryParse((string)rhs, out decimal rhsd))
                {
                    return lhsd == rhsd;
                }
                else
                {
                    return String.Compare((string)lhs, (string)rhs, ignoreCase) == 0;
                }
            }
            if ((IsSignedInteger(lhs) || IsUnsignedInteger(lhs) || IsFloat(lhs)) && (IsSignedInteger(rhs) || IsUnsignedInteger(rhs) || IsFloat(rhs)))
            {
                if (IsSignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs == (long)rhs;
                if (IsUnsignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs == (long)rhs;
                if (IsFloat(lhs) && IsSignedInteger(rhs)) return (decimal)lhs == (long)rhs;

                if (IsSignedInteger(lhs) && IsUnsignedInteger(rhs)) return (long)lhs == (long)rhs;
                if (IsUnsignedInteger(lhs) && IsUnsignedInteger(rhs)) return (ulong)lhs == (ulong)rhs;
                if (IsFloat(lhs) && IsUnsignedInteger(rhs)) return (decimal)lhs == (ulong)rhs;

                if (IsSignedInteger(lhs) && IsFloat(rhs)) return (long)lhs == (decimal)rhs;
                if (IsUnsignedInteger(lhs) && IsFloat(rhs)) return (ulong)lhs == (decimal)rhs;
                if (IsFloat(lhs) && IsFloat(rhs)) return (decimal)lhs == (decimal)rhs;

                if (IsEnum(lhs) && IsEnum(rhs)) return String.Compare(Enum.GetName(lhs.GetType(), lhs), Enum.GetName(rhs.GetType(), rhs), ignoreCase) == 0;
            }
            return false;
        }
    }

    internal class NonEqualityOperator : BinaryOperator
    {
        public NonEqualityOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is bool && rhs is bool) return (bool)lhs != (bool)rhs;
            if (lhs is string && rhs is string)
            {
                if (decimal.TryParse((string)lhs, out decimal lhsd) && decimal.TryParse((string)rhs, out decimal rhsd))
                {
                    return lhsd != rhsd;
                }
                else
                {
                    return (string)lhs != (string)rhs;
                }
            }
            if ((IsSignedInteger(lhs) || IsUnsignedInteger(lhs) || IsFloat(lhs)) && (IsSignedInteger(rhs) || IsUnsignedInteger(rhs) || IsFloat(rhs)))
            {
                if (IsSignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs != (long)rhs;
                if (IsUnsignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs != (long)rhs;
                if (IsFloat(lhs) && IsSignedInteger(rhs)) return (decimal)lhs != (long)rhs;

                if (IsSignedInteger(lhs) && IsUnsignedInteger(rhs)) return (long)lhs != (long)rhs;
                if (IsUnsignedInteger(lhs) && IsUnsignedInteger(rhs)) return (ulong)lhs != (ulong)rhs;
                if (IsFloat(lhs) && IsUnsignedInteger(rhs)) return (decimal)lhs != (ulong)rhs;

                if (IsSignedInteger(lhs) && IsFloat(rhs)) return (long)lhs != (decimal)rhs;
                if (IsUnsignedInteger(lhs) && IsFloat(rhs)) return (ulong)lhs != (decimal)rhs;
                if (IsFloat(lhs) && IsFloat(rhs)) return (decimal)lhs != (decimal)rhs;

                if (IsEnum(lhs) && IsEnum(rhs)) return String.Compare(Enum.GetName(lhs.GetType(), lhs), Enum.GetName(rhs.GetType(), rhs), ignoreCase) != 0;
            }
            return false;
        }
    }

    internal class GreaterThanOrEqualOperator : BinaryOperator
    {
        public GreaterThanOrEqualOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is string && rhs is string)
            {
                if (decimal.TryParse((string)lhs, out decimal lhsd) && decimal.TryParse((string)rhs, out decimal rhsd))
                {
                    return lhsd >= rhsd;
                }
            }
            if ((IsSignedInteger(lhs) || IsUnsignedInteger(lhs) || IsFloat(lhs)) && (IsSignedInteger(rhs) || IsUnsignedInteger(rhs) || IsFloat(rhs)))
            {
                if (IsSignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs >= (long)rhs;
                if (IsUnsignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs >= (long)rhs;
                if (IsFloat(lhs) && IsSignedInteger(rhs)) return (decimal)lhs >= (long)rhs;

                if (IsSignedInteger(lhs) && IsUnsignedInteger(rhs)) return (long)lhs >= (long)rhs;
                if (IsUnsignedInteger(lhs) && IsUnsignedInteger(rhs)) return (ulong)lhs >= (ulong)rhs;
                if (IsFloat(lhs) && IsUnsignedInteger(rhs)) return (decimal)lhs >= (ulong)rhs;

                if (IsSignedInteger(lhs) && IsFloat(rhs)) return (long)lhs >= (decimal)rhs;
                if (IsUnsignedInteger(lhs) && IsFloat(rhs)) return (ulong)lhs >= (decimal)rhs;
                if (IsFloat(lhs) && IsFloat(rhs)) return (decimal)lhs >= (decimal)rhs;
            }
            return false;
        }
    }

    internal class GreaterThanOperator : BinaryOperator
    {
        public GreaterThanOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is string && rhs is string)
            {
                if (decimal.TryParse((string)lhs, out decimal lhsd) && decimal.TryParse((string)rhs, out decimal rhsd))
                {
                    return lhsd > rhsd;
                }
            }
            if ((IsSignedInteger(lhs) || IsUnsignedInteger(lhs) || IsFloat(lhs)) && (IsSignedInteger(rhs) || IsUnsignedInteger(rhs) || IsFloat(rhs)))
            {
                if (IsSignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs > (long)rhs;
                if (IsUnsignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs > (long)rhs;
                if (IsFloat(lhs) && IsSignedInteger(rhs)) return (decimal)lhs > (long)rhs;

                if (IsSignedInteger(lhs) && IsUnsignedInteger(rhs)) return (long)lhs > (long)rhs;
                if (IsUnsignedInteger(lhs) && IsUnsignedInteger(rhs)) return (ulong)lhs > (ulong)rhs;
                if (IsFloat(lhs) && IsUnsignedInteger(rhs)) return (decimal)lhs > (ulong)rhs;

                if (IsSignedInteger(lhs) && IsFloat(rhs)) return (long)lhs > (decimal)rhs;
                if (IsUnsignedInteger(lhs) && IsFloat(rhs)) return (ulong)lhs > (decimal)rhs;
                if (IsFloat(lhs) && IsFloat(rhs)) return (decimal)lhs > (decimal)rhs;
            }
            return false;
        }
    }

    internal class LessThanOrEqualOperator : BinaryOperator
    {
        internal LessThanOrEqualOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is string && rhs is string)
            {
                if (decimal.TryParse((string)lhs, out decimal lhsd) && decimal.TryParse((string)rhs, out decimal rhsd))
                {
                    return lhsd <= rhsd;
                }
            }
            if ((IsSignedInteger(lhs) || IsUnsignedInteger(lhs) || IsFloat(lhs)) && (IsSignedInteger(rhs) || IsUnsignedInteger(rhs) || IsFloat(rhs)))
            {
                if (IsSignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs <= (long)rhs;
                if (IsUnsignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs <= (long)rhs;
                if (IsFloat(lhs) && IsSignedInteger(rhs)) return (decimal)lhs <= (long)rhs;

                if (IsSignedInteger(lhs) && IsUnsignedInteger(rhs)) return (long)lhs <= (long)rhs;
                if (IsUnsignedInteger(lhs) && IsUnsignedInteger(rhs)) return (ulong)lhs <= (ulong)rhs;
                if (IsFloat(lhs) && IsUnsignedInteger(rhs)) return (decimal)lhs <= (ulong)rhs;

                if (IsSignedInteger(lhs) && IsFloat(rhs)) return (long)lhs <= (decimal)rhs;
                if (IsUnsignedInteger(lhs) && IsFloat(rhs)) return (ulong)lhs <= (decimal)rhs;
                if (IsFloat(lhs) && IsFloat(rhs)) return (decimal)lhs <= (decimal)rhs;
            }
            return false;
        }
    }

    internal class LessThanOperator : BinaryOperator
    {
        internal LessThanOperator(int id, int depth) : base(id, depth)
        { }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            var (lhs, rhs) = PrepareArguments(obj);

            if (lhs is string && rhs is string)
            {
                if (decimal.TryParse((string)lhs, out decimal lhsd) && decimal.TryParse((string)rhs, out decimal rhsd))
                {
                    return lhsd < rhsd;
                }
            }
            if ((IsSignedInteger(lhs) || IsUnsignedInteger(lhs) || IsFloat(lhs)) && (IsSignedInteger(rhs) || IsUnsignedInteger(rhs) || IsFloat(rhs)))
            {
                if (IsSignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs < (long)rhs;
                if (IsUnsignedInteger(lhs) && IsSignedInteger(rhs)) return (long)lhs < (long)rhs;
                if (IsFloat(lhs) && IsSignedInteger(rhs)) return (decimal)lhs < (long)rhs;

                if (IsSignedInteger(lhs) && IsUnsignedInteger(rhs)) return (long)lhs < (long)rhs;
                if (IsUnsignedInteger(lhs) && IsUnsignedInteger(rhs)) return (ulong)lhs < (ulong)rhs;
                if (IsFloat(lhs) && IsUnsignedInteger(rhs)) return (decimal)lhs < (ulong)rhs;

                if (IsSignedInteger(lhs) && IsFloat(rhs)) return (long)lhs < (decimal)rhs;
                if (IsUnsignedInteger(lhs) && IsFloat(rhs)) return (ulong)lhs < (decimal)rhs;
                if (IsFloat(lhs) && IsFloat(rhs)) return (decimal)lhs < (decimal)rhs;
            }
            return false;
        }
    }

    /// <summary>
    /// Represents token other than operator, i.e. a property name (variable), a constant or a result of a previously calculated condition.
    /// </summary>
    internal abstract class DataToken : Token
    {
        public DataToken(int id, int depth) : base(id, depth)
        { }

        internal override bool CheckSolvability(IEnumerable<Token> set)
        {
            return false;
        }

        internal override void FreezeReferences(List<Token> set)
        {

        }

        internal override void ReplaceReferences(Token token)
        {

        }

        internal override void EnterResult(int id, bool result)
        {

        }
    }

    /// <summary>
    /// Represents property of an object (variable) in the condition set. Variable name must begin with a dollar sign.
    /// </summary>
    internal class Variable : DataToken
    {
        internal string Identifier { get; set; }

        internal Variable(int id, int depth, string value) : base(id, depth)
        {
            Identifier = value.TrimStart('$');
        }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            object o = GetValue(obj, this);
            if (o is bool) return !(bool)o;
            if (o is string)
            {
                Boolean.TryParse((string)o, out bool b);
                return !b;
            }
            return false;
        }
    }

    /// <summary>
    /// Represents constant amount (string or a number given as string) in the condition set.
    /// </summary>
    internal class Constant : DataToken
    {
        internal string Value { get; set; }

        internal Constant(int id, int depth, string value) : base(id, depth)
        {
            Value = value;
        }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            if (Boolean.TryParse(Value, out bool b))
            {
                return b;
            }
            return false;
        }
    }

    /// <summary>
    /// Represents boolean value of an already solved condition.
    /// </summary>
    internal class Result : DataToken
    {
        public bool Value { get; set; }

        public Result(int id, int depth) : base(id, depth)
        {
            ID = id;
        }

        internal override bool Solve(object obj, bool ignoreCase)
        {
            return Value;
        }
    }
}
