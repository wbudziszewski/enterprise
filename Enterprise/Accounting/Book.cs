﻿using Budziszewski.Enterprise.Investments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Budziszewski.Enterprise.Accounting
{
    public enum ExpenseMethod { FIFO, LIFO, Average };

    public enum BooksClosingQuery { None = 0, IncludePreviousYears = 1, IncludeAll = 2, IncludeOnly = 3 }

    public class Book
    {
        public string Name { get; set; }

        public bool ApplyTaxRules { get; set; } = false;

        public bool CalculateTimeValueOfMoney { get; set; } = true;

        public bool CalculateUnrealizedFXEffects { get; set; } = true;

        public ExpenseMethod ExpenseMethod { get; set; } = ExpenseMethod.FIFO;

        public string Currency { get; set; } = Common.DomesticCurrency;

        private string taxBook;
        public Book TaxBook
        {
            get
            {
                return String.IsNullOrEmpty(taxBook) ? this : (Common.Books.FirstOrDefault(x => x.Name == taxBook) ?? this);
            }
            set
            {
                taxBook = value.Name;
            }
        }

        public int Priority { get; set; }

        private Dictionary<string, AccountingScheme> schemes = new Dictionary<string, AccountingScheme>();

        public Dictionary<string, Account> Accounts { get; private set; } = new Dictionary<string, Account>();

        public Dictionary<DateTime, Dictionary<string, double>> PortfolioWeights { get; set; } = new Dictionary<DateTime, Dictionary<string, double>>();

        public bool IsProcessed { get; private set; }

        public Book(string name)
        {
            Name = name;

            // Create accounts
            foreach (var def in Database.BookAccounts)
            {
                Accounts.Add(def.ID, new Account(def));
            }

            // Assign accounting schemes
            foreach (var accountScheme in Database.SchemeAccounts)
            {
                if (accountScheme.BookID != Name) continue;
                string scheme = accountScheme.Name;

                try
                {
                    if (!schemes.ContainsKey(scheme))
                    {
                        schemes[scheme] = (AccountingScheme)Type.GetType("Budziszewski.Enterprise.Accounting." + accountScheme.Name).GetConstructor(new Type[] { typeof(Book), typeof(Definitions.AccountingScheme) }).Invoke(new object[] { this, accountScheme });
                    }
                    schemes[scheme].AddActivity(accountScheme);
                }
                catch (Exception ex)
                {
                    Log.Report(Severity.Error, "Error creating accounting scheme.", exception: ex);
                }
            }
        }

        public void Process()
        {
            foreach (var account in Accounts.Values)
            {
                account.Clear();
            }

            var dates = Financial.Calendar.GenerateReportingDates(Common.StartDate, Common.EndDate, Financial.Calendar.TimeStep.Monthly).ToArray();
            AssetSpecificAccountingScheme[] assetSpecificSchemes = schemes.Select(x => x.Value).OfType<AssetSpecificAccountingScheme>().OrderByDescending(
                            x => (x.GetType().GetAttribute<PriorityAttribute>()?.Priority) ?? 0).ToArray();
            GeneralLedgerAccountingScheme[] generalLedgerSchemes = schemes.Select(x => x.Value).OfType<GeneralLedgerAccountingScheme>().OrderByDescending(
                            x => (x.GetType().GetAttribute<PriorityAttribute>()?.Priority) ?? 0).ToArray();

            PortfolioWeights.Clear();
            for (int i = 1; i < dates.Length; i++)
            {
                // Get portfolio weights
                PortfolioWeights.Add(dates[i], GetPortfolioWeights(dates[i - 1], dates[i], investments));

                // Process accounting schemes
                foreach (var inv in investments)
                {
                    if (!inv.IsActive(dates[i - 1], dates[i])) continue;

                    foreach (var a in assetSpecificSchemes)
                    {
                        a.Process(inv, dates[i - 1], dates[i]);
                    }
                }

                foreach (var a in generalLedgerSchemes)
                {
                    a.Process(dates[i - 1], dates[i]);
                }

            }

            IsProcessed = true;
        }

        #region Investments

        List<Investment> investments;

        public List<Investment> Investments
        {
            get
            {
                return investments;
            }
        }

        long globalInvestmentIndex = 0;
        private long GetInvestmentIndex()
        {
            return ++globalInvestmentIndex;
        }

        public void GenerateInvestments(DateTime endDate)
        {
            investments = new List<Investment>();
            var pendingEvents = new HashSet<Events.Event>(); // list of events that may result in new investments appearing (cash flows)
            Definitions.Instrument def;

            // First get transactions
            Queue<Definitions.Transaction> q = new Queue<Definitions.Transaction>(Database.Transactions);

            // Iterate through transactions one by one
            while (q.Count > 0)
            {
                Definitions.Transaction transaction = q.Dequeue();

                // Go through pending events that come before (influence) current transaction - e.g. dividends, coupons that may add new cash
                foreach (var e in pendingEvents.Where(x => x.settleDate <= transaction.SettlementDate))
                {
                    AddInvestment(typeof(Cash), e);
                }
                pendingEvents.RemoveWhere(x => x.settleDate <= transaction.SettlementDate);

                if (transaction.TradeDate > endDate && transaction.SettlementDate > endDate) continue;

                #region Purchase
                {
                    if (transaction.Type == Definitions.TransactionType.Buy)
                    {
                        def = Definitions.Instrument.GetInstrument(transaction.InstrumentID);
                        if (def != null)
                        {
                            RegisterPurchase(def, new Events.Purchase(transaction), pendingEvents); // add purchased instrument
                            if (transaction.TradeDate != transaction.SettlementDate)
                            {
                                RegisterPayable(new Events.Inflow(transaction, false), new Events.Outflow(transaction, true)); // create payable if necessary
                            }
                            RegisterCashDeduction(new Events.Outflow(transaction, true)); // subtract cash
                        }
                        continue;
                    }
                }
                #endregion

                #region Sale
                {
                    if (transaction.Type == Definitions.TransactionType.Sell)
                    {
                        def = Definitions.Instrument.GetInstrument(transaction.InstrumentID);
                        if (def != null)
                        {
                            RegisterSale(def, new Events.Sale(transaction), pendingEvents); // register sale of instrument(s)
                            if (transaction.TradeDate != transaction.SettlementDate)
                            {
                                RegisterReceivable(new Events.Inflow(transaction, false), new Events.Outflow(transaction, true)); // create payable if necessary
                            }
                            RegisterCashAddition(new Events.Inflow(transaction, true)); // add cash
                        }
                        continue;
                    }
                }
                #endregion

                #region Cash transfer
                {
                    if (transaction.Type == Definitions.TransactionType.Cash)
                    {
                        if (!String.IsNullOrEmpty(transaction.AccountSrc)) RegisterCashDeduction(new Events.Outflow(transaction, false));
                        if (!String.IsNullOrEmpty(transaction.AccountDst)) RegisterCashAddition(new Events.Inflow(transaction, false));
                        continue;
                    }
                }
                #endregion

                Log.Report(Severity.Error, "Unknown type of transaction encountered: " + transaction.Type + ".");
            }
        }

        /// <summary>
        /// Registers new purchase of an instrument.
        /// </summary>
        void RegisterPurchase(Definitions.Instrument def, Events.Purchase e, HashSet<Events.Event> pendingEvents)
        {
            StandardizedSecurity inv = null;
            Type type = Definitions.Instrument.GetInstrumentType(def.InstrumentType);

            if (ExpenseMethod == Accounting.ExpenseMethod.Average)
            {
                inv = investments.Where(x => x.GetType() == type).FirstOrDefault(y => y.ID == e.Instrument && y.CustodyAccount == e.CustodyAccount && y.Portfolio == e.Portfolio && y.ValuationClass == e.ValuationClass) as StandardizedSecurity;
                if (inv != null)
                {
                    inv.RegisterAddition(e);
                }
            }

            if (inv == null)
            {
                try
                {
                    inv = AddInvestment(type, e, def.ID) as StandardizedSecurity;
                    foreach (var evt in (inv as Investment).GetFlowEvents())
                    {
                        pendingEvents.Add(evt);
                    }
                }
                catch (Exception ex)
                {
                    Log.Report(Severity.Error, "Error creating new investment.", def, ex);
                }
            }
        }

        /// <summary>
        /// Registers new sale of an instrument.
        /// </summary>
        /// <param name="e">Sale event</param>
        void RegisterSale(Definitions.Instrument def, Events.Sale e, HashSet<Events.Event> pendingEvents)
        {
            Type type = Definitions.Instrument.GetInstrumentType(def.InstrumentType);

            var src = investments.Where(x => x.GetType() == type).Where(y => y.ID == e.Instrument && y.CustodyAccount == e.CustodyAccount && y.Portfolio == e.Portfolio && y.ValuationClass == e.ValuationClass).OfType<StandardizedSecurity>();
            if (src == null || src.Count() < 1)
            {
                Log.Report(Severity.Error, "No possible source for sale.", def);
                return;
            }

            if (ExpenseMethod == Accounting.ExpenseMethod.LIFO) src = src.Reverse();

            foreach (var inv in src)
            {
                if ((inv as Investment).GetCount(Core.TimeArg.BeforeEvent(e)) == 0) continue;
                e.Count -= inv.RegisterDeduction(e.Copy<Events.Sale>());
                if (e.Count <= 0) break;
            }

            if (e.Count > 0)
            {
                Log.Report(Severity.Error, "Not enough bonds for deduction.", e);
            }
        }

        void RegisterCashAddition(Events.Inflow e)
        {
            AddInvestment(typeof(Cash), e);
        }

        void RegisterCashDeduction(Events.Outflow e)
        {
            decimal originalamount = e.Amount;
            var invs = investments.OfType<Cash>().Where(y => y.Currency == e.Currency && y.AssociatedCashAccount == e.CashAccount && y.Portfolio == e.Portfolio);
            if (invs == null || invs.Count() < 1)
            {
                Log.Report(Severity.Error, "No possible source for cash deduction.", e);
            }

            foreach (var i in invs)
            {
                var currentAmount = i.GetNominalAmount(Core.TimeArg.BeforeEvent(e), false, false);
                if (currentAmount == 0) continue;
                var outflow = e.Copy<Events.Outflow>();
                outflow.Amount = Math.Min(currentAmount, e.Amount);
                e.Amount -= i.RegisterDeduction(outflow);
                if (e.Amount <= 0) break;
            }

            if (e.Amount > 0)
            {
                Log.Report(Severity.Error, "No possible source for cash deduction.", e);
            }
        }

        void RegisterReceivable(Events.Inflow creation, Events.Outflow payment)
        {
            var inv = AddInvestment(typeof(Receivable), creation);
            inv.AddEvent(payment);
        }

        void RegisterPayable(Events.Inflow creation, Events.Outflow payment)
        {
            var inv = AddInvestment(typeof(Payable), creation);
            inv.AddEvent(payment);
        }

        /// <summary>
        /// Adds a new created investment to the investments list.
        /// </summary>
        /// <param name="investments"></param>
        /// <param name="inv"></param>
        Investment AddInvestment(Type type, Events.Event e, string instrument = null)
        {
            Investment inv = null;
            try
            {
                Definitions.Instrument def = Definitions.Instrument.GetInstrument(instrument);
                if (def != null) // defined security
                {
                    type = Definitions.Instrument.GetInstrumentType(def.InstrumentType);
                    if (e is Events.Purchase purchase)
                    {
                        inv = type.GetConstructor(new Type[] { typeof(long), typeof(Definitions.Instrument), typeof(Events.Purchase) }).Invoke(new object[] { GetInvestmentIndex(), def, purchase }) as Investment;
                    }
                    if (e is Events.Dividend dividend)
                    {
                        inv = new Cash(GetInvestmentIndex(), dividend);
                    }
                    if (e is Events.Redemption redemption)
                    {
                        inv = new Cash(GetInvestmentIndex(), redemption);
                    }
                }
                else // undefined security
                {
                    inv = type.GetConstructor(new Type[] { typeof(long), typeof(Events.Inflow) }).Invoke(new object[] { GetInvestmentIndex(), e }) as Investment;
                }

                if (inv == null) throw new ArgumentException("Cannot define investment.");
            }
            catch (Exception ex)
            {
                Log.Report(Severity.Error, "Error creating a new investment.", e, ex);
                return null;
            }

            DateTime investmentRecognitionDate = inv.GetPurchaseDate().Value;

            // Add investment to investment list
            for (int i = 0; i < investments.Count; i++)
            {
                if (investmentRecognitionDate < investments[i].GetPurchaseDate().Value)
                {
                    investments.Insert(i, inv);
                    return inv;
                }
            }
            investments.Add(inv);
            return inv;
        }

        #endregion



        long globalOperationIndex = 0;
        public long GetOperationIndex()
        {
            return ++globalOperationIndex;
        }

        public void Enter(AccountingSchemeActivity s, DateTime date, long invIndex, long operIndex, long transIndex, string description, string portfolio, decimal amount, BookingProperties properties = new BookingProperties())
        {
            if (amount == 0) return;
            if (s == null)
            {
                Log.Report(Severity.Error, String.Format("No accounting scheme activity specified for operation: {0} at {1} ({2}).", description, date.ToShortDateString(), amount));
                return;
            }

            Dictionary<string, decimal> pb;
            if (String.IsNullOrEmpty(portfolio))
            {
                pb = DivideToPortfolios(date, amount);
            }
            else
            {
                pb = new Dictionary<string, decimal>() { { portfolio, amount } };
            }

            foreach (var b in pb)
            {
                if (s.DtAccount != null && s.CtAccount != null)
                {
                    decimal currentAmount =
                        s.DtAccount.Entries.Where(x => x.InvestmentIndex == invIndex && x.Portfolio == b.Key).Sum(x => x.Amount) +
                        s.CtAccount.Entries.Where(x => x.InvestmentIndex == invIndex && x.Portfolio == b.Key).Sum(x => x.Amount);

                    decimal derecognitionAmount = 0;
                    if (currentAmount > 0 && b.Value < 0) derecognitionAmount = Math.Max(b.Value, -currentAmount);
                    if (currentAmount < 0 && b.Value > 0) derecognitionAmount = Math.Min(b.Value, -currentAmount);

                    s.DtAccount.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, Math.Min(0, derecognitionAmount), properties);
                    s.DtAccount.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, Math.Max(0, b.Value - derecognitionAmount), properties);
                    s.CtAccount.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, Math.Max(0, derecognitionAmount), properties);
                    s.CtAccount.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, Math.Min(0, b.Value - derecognitionAmount), properties);
                }
                else if (s.DtAccount != null)
                {
                    s.DtAccount.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, b.Value, properties);
                }
                else if (s.CtAccount != null)
                {
                    s.CtAccount.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, b.Value, properties);
                }
                else
                {
                    Log.Report(Severity.Error, String.Format("No account specified for accounting scheme activity: {0} for operation: {1} at {2} ({3}).", s.Name, description, date.ToShortDateString(), amount));
                }
            }
        }

        public void Enter(Account a, DateTime date, long invIndex, long operIndex, long transIndex, string description, string portfolio, decimal amount, BookingProperties properties = new BookingProperties())
        {
            if (amount == 0) return;
            if (a == null)
            {
                Log.Report(Severity.Error, String.Format("No account specified for operation: {0} at {1} ({2}).", description, date.ToShortDateString(), amount));
                return;
            }

            Dictionary<string, decimal> pb;
            if (String.IsNullOrEmpty(portfolio))
            {
                pb = DivideToPortfolios(date, amount);
            }
            else
            {
                pb = new Dictionary<string, decimal>() { { portfolio, amount } };
            }

            foreach (var b in pb)
            {
                a.AddEntry(date, invIndex, operIndex, transIndex, description, b.Key, b.Value, properties);
            }
        }

        public AccountingSchemeActivity GetAccountingActivity(string name, string activity, Investment inv = null)
        {
            try
            {
                if (inv == null)
                {
                    return schemes[name].Activities.FirstOrDefault(x => x.Name == activity);
                }
                else
                {
                    return schemes[name].Activities.FirstOrDefault(x => x.Name == activity && x.MeetsConditions(inv));
                }
            }
            catch (Exception ex)
            {
                Log.Report(Severity.Error, String.Format("Cannot find scheme account referenced by name: {0} and activity: {1} in book: {2}.", name, activity, Name), this, ex);
                return null;
            }
        }

        public IEnumerable<Account> GetAccounts()
        {
            return Accounts.Select(x => x.Value);
        }

        public void Clear()
        {
            foreach (var a in Accounts)
            {
                a.Value.Clear();
            }
            IsProcessed = false;
        }

        public (DateTime date, decimal result)[] GetTaxResults()
        {
            List<(DateTime date, decimal result)> output = new List<(DateTime date, decimal result)>();
            DateTime end = Calendar.ReachEndOfYear(Common.StartDate, 0);
            while (end <= Common.EndDate)
            {
                output.Add((end, (TaxBook ?? this).GetResult(end, false)));
                end = Calendar.ReachEndOfYear(end, 1);
            }
            return output.ToArray();
        }

        public decimal GetResult(DateTime date, bool? tax = null)
        {
            AccountQuery aq = new AccountQuery() { Date = date, IsTaxCharge = tax };
            decimal balance = 0;
            foreach (var a in Accounts.Where(x => x.Value.IsResultAccount))
            {
                balance += -a.Value.Get(aq).Sum(x => x.Amount);
            }
            return balance;
        }

        private Dictionary<string, double> GetPortfolioWeights(DateTime start, DateTime end, IEnumerable<Investment> investments)
        {
            Dictionary<string, double> o = new Dictionary<string, double>();
            double sum = 0;

            foreach (var inv in investments)
            {
                if (!inv.IsActive(start, end)) continue;
                double val = ((double)inv.GetValue(start) + (double)inv.GetValue(end)) / 2.0;
                if (val <= 0) continue;
                if (o.ContainsKey(inv.Portfolio))
                {
                    o[inv.Portfolio] += val;
                }
                else
                {
                    o[inv.Portfolio] = val;
                }
                sum += val;
            }

            if (sum > 0)
            {
                for (int i = 0; i < o.Count; i++)
                {
                    o[o.ElementAt(i).Key] = o.ElementAt(i).Value / sum;
                }
            }

            return o;
        }

        public Dictionary<string, decimal> DivideToPortfolios(DateTime date, decimal amount)
        {
            Dictionary<string, decimal> o = new Dictionary<string, decimal>();

            Dictionary<string, double> pw;

            if (PortfolioWeights.ContainsKey(date))
                pw = PortfolioWeights[date];
            else
                pw = PortfolioWeights.LastOrDefault(x => x.Key < date).Value;

            decimal sum = 0;
            decimal portfolioAmount = 0;
            string portfolio = "";

            for (int i = 0; i < pw.Count - 1; i++)
            {
                portfolio = pw.ElementAt(i).Key;
                portfolioAmount = Core.Round((decimal)pw.ElementAt(i).Value * amount);
                portfolioAmount = Math.Min(amount - sum, portfolioAmount);
                sum += portfolioAmount;
                o[portfolio] = portfolioAmount;
            }

            // Last elements gets all
            portfolio = pw.Last().Key;
            portfolioAmount = Core.Round((decimal)pw.Last().Value * amount);
            portfolioAmount = amount - sum;
            o[portfolio] = portfolioAmount;

            return o;
        }

        public override string ToString()
        {
            return Name;
        }


    }
}
