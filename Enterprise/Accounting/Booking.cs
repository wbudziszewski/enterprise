﻿using System;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Contains various properties regarding a booking.
    /// </summary>
    public struct BookingProperties
    {
        public bool YearClosing { get; set; }

        public bool TaxCharge { get; set; }
    }

    /// <summary>
    /// Represents single entry in accounting books.
    /// </summary>
    public class Booking
    {
        /// <summary>
        /// Date of the event that is represented by the book entry.
        /// </summary>
        public DateTime Date { get; private set; }

        /// <summary>
        /// Unique indentifier of an investment that is the subject of this booking.
        /// </summary>
        public long InvestmentIndex { get; private set; }

        /// <summary>
        /// Unique indentifier of an operation that is the subject of this booking.
        /// </summary>
        public long OperationIndex { get; private set; }

        /// <summary>
        /// Unique indentifier of a transaction that is the subject of this booking.
        /// </summary>
        public long TransactionIndex { get; private set; }

        /// <summary>
        /// Description of the booking
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Describes to which portfolio the booking should be made.
        /// </summary>
        public string Portfolio { get; set; }

        /// <summary>
        /// More detailed properties.
        /// </summary>
        public BookingProperties Properties { get; set; }

        /// <summary>
        /// Amount in domestic currency. Debit bookings are given as positive values, credit bookings are given as negative values.
        /// </summary>
        public decimal Amount { get; private set; }

        /// <summary>
        /// Creates a new booking.
        /// </summary>
        /// <param name="date">Date of an event</param>
        /// <param name="invGuid">Unique indentifier of an investment </param>
        /// <param name="operGuid">Unique indentifier of an operation </param>
        /// <param name="transGuid">Unique indentifier of a transaction </param>
        /// <param name="description">Description of the booking</param>
        /// <param name="portfolio">Portfolio of the booking</param>
        /// <param name="amount">Amount in domestic currency</param>
        /// <param name="properties">Detailed properties</param>
        public Booking(DateTime date, long invIndex, long operIndex, long transIndex, string description, string portfolio, decimal amount, BookingProperties properties)
        {
            Date = date;
            InvestmentIndex = invIndex;
            OperationIndex = operIndex;
            TransactionIndex = transIndex;
            Description = description;
            Portfolio = portfolio;
            Amount = Core.Round(amount);
            Properties = properties;
        }

        public bool IsBooksClosing
        {
            get
            {
                return Properties.YearClosing;
            }
        }

        /// <summary>
        /// Returns text representation of the booking
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0:N2}: {1}", Amount, Description);
        }
    }
}
