using System;
using System.Linq;
namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Performs bookings for asset sale.
    /// </summary>
    public class AssetDerecognitionOnSale : AssetSpecificAccountingScheme
    {
        public AssetDerecognitionOnSale(Book book, Definitions.AccountingScheme def) : base(book, def)
        {
        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account from which the asset will be derecognized upon sale / liquidation
            /// </summary>
            var AssetDerecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "AssetDerecognition", inv);

            /// <summary>
            /// Account from which unrealized gains/losses from market valuation will be derecognized
            /// </summary>
            var UnrealizedResultDerecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "UnrealizedResultOnValuationDerecognition", inv);

            /// <summary>
            /// Receivables account for temporary recognition of receivable between trade date and settlement date
            /// </summary>
            var ReceivableRecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "ReceivableRecognition", inv);

            /// <summary>
            /// Cash account to which payment would be made
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("AssetDerecognitionOnSale", "CashSettlement", inv);

            /// <summary>
            /// Cost account for fee recognition
            /// </summary>
            var FeeRecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "FeeRecognition", inv);

            /// <summary>
            /// Reserves account which holds purchase fees for unsold assets - these may be book costs but not tax costs
            /// </summary>
            var UnrealizedFeeDerecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "UnrealizedFeeDerecognition", inv);

            /// <summary>
            /// Account where realized gains/losses from sale (market valuation) will be booked
            /// </summary>
            var RealizedResultRecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "RealizedResultOnValuationRecognition", inv);

            /// <summary>
            /// Accounts from where unrealized FX result will be derecognized.
            /// </summary>
            var UnrealizedResultOnFXDerecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "UnrealizedResultOnFXDerecognition", inv);

            /// <summary>
            /// Accounts where realized FX result will be recognized.
            /// </summary>
            var RealizedResultOnFXRecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "RealizedResultOnFXRecognition", inv);

            //if (AssetDerecognition == null) { Log.Report(Severity.Error, "Error setting accounts in AssetDerecognitionOnSale accounting scheme. AssetDerecognition account remains undefined.", this); return; }
            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in AssetDerecognitionOnSale accounting scheme. CashSettlement account remains undefined.", this); return; }

            if (AssetDerecognition == null || CashSettlement == null)
            {
                return;
            }


            decimal saleAmount = 0;
            decimal feeAmount = 0;
            decimal unrealizedFeeAmount = 0;

            // First look for sale events, applicable for tradeable securities      
            foreach (var e in inv.GetEvents<Events.Sale>(start, end))
            {
                long index = Book.GetOperationIndex();

                saleAmount = e.Amount;
                // Book sale fee, and optionally puchase fee that was not recognized as tax cost up to this point
                if (FeeRecognition != null)
                {
                    feeAmount = e.Fee;
                    Book.Enter(FeeRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (fee cost recognition)", inv.Portfolio, feeAmount);
                    Book.Enter(CashSettlement, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (fee payment)", inv.Portfolio, -feeAmount);
                    if (UnrealizedFeeDerecognition != null)
                    {
                        unrealizedFeeAmount = Core.Round(e.Count / inv.GetCount(Core.TimeArg.BeforeEvent(e)) * inv.GetUnrealizedPurchaseFee(Core.TimeArg.BeforeEvent(e), true, true));
                        Book.Enter(UnrealizedFeeDerecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (unrecognized purchase fee reserve derecognition)", inv.Portfolio, -unrealizedFeeAmount);
                        Book.Enter(FeeRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (unrecognized purchase fee cost recognition)", inv.Portfolio, unrealizedFeeAmount);
                    }
                }

                decimal fxDerecognition = 0;
                decimal fxRecognition = 0;
                decimal fxTotal = 0;
                // Calculate FX effects
                if (inv.Currency != Book.Currency)
                {
                    if (RealizedResultOnFXRecognition == null) { Log.Report(Severity.Error, "Error setting accounts in AssetDerecognitionOnSale accounting scheme. RealizedResultOnFXRecognition account remains undefined.", this); return; }
                    bool isResult = RealizedResultOnFXRecognition.InvolvesResultAccount;

                    if (Book.CalculateUnrealizedFXEffects)
                    {
                        if (UnrealizedResultOnFXDerecognition == null) { Log.Report(Severity.Error, "Error setting accounts in AssetDerecognitionOnSale accounting scheme. UnrealizedResultOnFXDerecognition account remains undefined.", this); return; }

                        fxDerecognition = inv.GetUnrealizedGainsLossesFromFX(isResult ? Core.TimeArg.AtDate(Calendar.ReachEndOfYear(e.settleDate, -1)) : null, Core.TimeArg.BeforeEvent(e), true, Book.ApplyTaxRules, isResult);
                        fxRecognition = inv.GetRealizedGainsLossesFromFX(e, true, Book.ApplyTaxRules, isResult);
                        fxTotal = inv.GetUnrealizedGainsLossesFromFX(null, Core.TimeArg.BeforeEvent(e), true, Book.ApplyTaxRules, isResult);

                        Book.Enter(UnrealizedResultOnFXDerecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (unrealized FX result derecognition)", inv.Portfolio, -fxDerecognition);
                        Book.Enter(RealizedResultOnFXRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (realized FX result recognition)", inv.Portfolio, -fxRecognition);
                    }
                    else
                    {
                        fxTotal = inv.GetUnrealizedGainsLossesFromFX(null, Core.TimeArg.BeforeEvent(e), true, Book.ApplyTaxRules, isResult);

                        Book.Enter(RealizedResultOnFXRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (realized FX result recognition)", inv.Portfolio, -fxTotal);
                    }
                }

                if (!Book.CalculateTimeValueOfMoney) // There is no valuation at all (e.g. tax book)
                {
                    decimal derecognitionAmount = Core.Round(inv.GetPurchaseAmount(Core.TimeArg.BeforeEvent(e), true, true, Book.ApplyTaxRules) * e.Count / inv.GetCount(Core.TimeArg.BeforeEvent(e)));
                    decimal result = saleAmount - derecognitionAmount - fxTotal;

                    Book.Enter(AssetDerecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (asset derecognition)", inv.Portfolio, -derecognitionAmount);
                    Book.Enter(RealizedResultRecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (result)", inv.Portfolio, -result);

                    if (e.evtDate != e.settleDate)
                    {
                        Book.Enter(ReceivableRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (receivable recognition)", inv.Portfolio, saleAmount);
                        Book.Enter(ReceivableRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (receivable derecognition)", inv.Portfolio, -saleAmount);
                    }
                    Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (sale amount payment)", inv.Portfolio, saleAmount);
                }
                else if (UnrealizedResultDerecognition == null) // No market valuation (e.g. HTM instrument)
                {
                    decimal derecognitionAmount = Core.Round(inv.GetAmortizedCostValue(Core.TimeArg.BeforeEvent(e), true, true, Book.ApplyTaxRules) * e.Count / inv.GetCount(Core.TimeArg.BeforeEvent(e)));
                    decimal result = saleAmount - derecognitionAmount - fxTotal;

                    Book.Enter(AssetDerecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (asset derecognition)", inv.Portfolio, -derecognitionAmount);
                    Book.Enter(RealizedResultRecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (result)", inv.Portfolio, -result);

                    if (e.evtDate != e.settleDate)
                    {
                        Book.Enter(ReceivableRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (receivable recognition)", inv.Portfolio, saleAmount);
                        Book.Enter(ReceivableRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (receivable derecognition)", inv.Portfolio, -saleAmount);
                    }
                    Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (sale amount payment)", inv.Portfolio, saleAmount);
                }
                else
                {
                    // First derecognize previous valuation
                    decimal previousMarketPremium = UnrealizedResultDerecognition.DtAccount.Get(new AccountQuery() { Date = start, Investment = inv }).Sum(x => x.Amount) + UnrealizedResultDerecognition.CtAccount.Get(new AccountQuery() { Date = start, Investment = inv }).Sum(x => x.Amount);
                    Book.Enter(UnrealizedResultDerecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (market valuation derecognition)", inv.Portfolio, -previousMarketPremium);
                    Book.Enter(AssetDerecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (market valuation derecognition)", inv.Portfolio, previousMarketPremium);

                    decimal currentMarketPremium = inv.GetRealizedGainsLossesFromValuation(e, true, Book.ApplyTaxRules, UnrealizedResultDerecognition.InvolvesResultAccount);
                    Book.Enter(RealizedResultRecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (realized result recognition)", inv.Portfolio, -currentMarketPremium);

                    decimal derecognitionAmount = Core.Round(inv.GetAmortizedCostValue(Core.TimeArg.BeforeEvent(e), true, true, Book.ApplyTaxRules) * e.Count / inv.GetCount(Core.TimeArg.BeforeEvent(e)));
                    Book.Enter(AssetDerecognition, inv.RecognitionOnTradeDate ? e.evtDate : e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (asset derecognition)", inv.Portfolio, -derecognitionAmount);

                    if (e.evtDate != e.settleDate)
                    {
                        Book.Enter(ReceivableRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset sale (receivable recognition)", inv.Portfolio, saleAmount);
                        Book.Enter(ReceivableRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (receivable derecognition)", inv.Portfolio, -saleAmount);
                    }
                    Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset sale (sale amount payment)", inv.Portfolio, saleAmount);
                }
            }
        }
    }
}