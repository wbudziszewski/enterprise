﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Calculates effects of market valuation of an asset at the end of each period.
    /// </summary>
    [Priority(-1)]
    public class MarketValuation : AssetSpecificAccountingScheme
    {
        public MarketValuation(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account where change in market valuation will be recognized
            /// </summary>
            var AssetValuation = Book.GetAccountingActivity("MarketValuation", "AssetValuation", inv);

            /// <summary>
            /// Account where unrealized gains/losses from market valuation will be recognized
            /// </summary>
            var UnrealizedResultRecognition = Book.GetAccountingActivity("MarketValuation", "UnrealizedResultOnValuationRecognition", inv);

            /// <summary>
            /// Account where realized gains/losses from sale (market valuation) will be booked
            /// </summary>
            var RealizedResultRecognition = Book.GetAccountingActivity("AssetDerecognitionOnSale", "RealizedResultOnValuationRecognition", inv);

            //if (AssetValuation == null) { Log.Report(Severity.Error, "Error setting accounts in MarketValuation accounting scheme. AssetValuation account remains undefined.", this); return; }
            //if (UnrealizedResultRecognition == null) { Log.Report(Severity.Error, "Error setting accounts in MarketValuation accounting scheme. UnrealizedResultRecognition account remains undefined.", this); return; }

            if (AssetValuation == null || UnrealizedResultRecognition == null)
            {
                return;
            }

            if (!inv.IsActive(Core.TimeArg.AtDate(end))) return;
            long index = Book.GetOperationIndex();

            decimal income = inv.GetUnrealizedGainsLossesFromValuation(Core.TimeArg.AtDate(start), Core.TimeArg.AtDate(end), true, Book.ApplyTaxRules, UnrealizedResultRecognition.InvolvesResultAccount, true);

            Book.Enter(AssetValuation, end, inv.Index, index, 0, "Market valuation (change of asset value)", inv.Portfolio, income);
            Book.Enter(UnrealizedResultRecognition, end, inv.Index, index, 0, "Market valuation (unrealized gains/losses recognition)", inv.Portfolio, -income);
        }
    }
}
