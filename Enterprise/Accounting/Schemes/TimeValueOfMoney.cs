﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Represents accounting effects of time flow in case debt instruments
    /// </summary>
    [Priority(-1)]
    public class TimeValueOfMoney : AssetSpecificAccountingScheme
    {
        public TimeValueOfMoney(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account where change in amortized cost valuation will be recognized
            /// </summary>
            var AssetValuation = Book.GetAccountingActivity("TimeValueOfMoney", "AssetValuation", inv);

            /// <summary>
            /// Account where ordinary income will be recognized
            /// </summary>
            var OrdinaryIncome = Book.GetAccountingActivity("TimeValueOfMoney", "OrdinaryIncome", inv);

            //if (AssetValuation == null) { Log.Report(Severity.Error, "Error setting accounts in TimeValueOfMoney accounting scheme. AssetValuation account remains undefined.", this); return; }
            //if (OrdinaryIncome == null) { Log.Report(Severity.Error, "Error setting accounts in TimeValueOfMoney accounting scheme. OrdinaryIncome account remains undefined.", this); return; }

            if (AssetValuation == null || OrdinaryIncome == null)
            {
                return;
            }

            long index = Book.GetOperationIndex();
            decimal income = inv.GetTimeValueOfMoneyIncome(Core.TimeArg.AtDate(start), Core.TimeArg.AtDate(end), true, Book.ApplyTaxRules);

            Book.Enter(AssetValuation, end, inv.Index, index, 0, "Time value of money (change of asset value)", inv.Portfolio, income);
            Book.Enter(OrdinaryIncome, end, inv.Index, index, 0, "Time value of money (ordinary income)", inv.Portfolio, -income);
        }
    }
}
