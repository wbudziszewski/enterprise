﻿using System;
using System.Linq;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Applies income tax calculation at the end of the period.
    /// </summary>
    [Priority(-4)]
    public class IncomeTax : GeneralLedgerAccountingScheme
    {
        public IncomeTax(Book book, Definitions.AccountingScheme def) : base(book, def)
        {
        }

        public override void Process(DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset where accrued expense resulting from tax calculated but not yet charged is booked.
            /// </summary>
            var AccruedExpense = Book.GetAccountingActivity("IncomeTax", "AccruedExpense");

            /// <summary>
            /// Account where liabilities resulting from tax charged is booked.
            /// </summary>
            var TaxLiabilitiesRecognition = Book.GetAccountingActivity("IncomeTax", "TaxLiabilitiesRecognition");

            /// <summary>
            /// Account where tax that will be deducted and paid from current year's result is booked.
            /// </summary>
            var IncomeTax = Book.GetAccountingActivity("IncomeTax", "IncomeTax");

            if (AccruedExpense == null || TaxLiabilitiesRecognition == null || IncomeTax == null)
            {
                return;
            }

            decimal taxCurrentResult = (Book.TaxBook ?? Book).GetResult(end, true);
            decimal taxCurrent = Core.Round(Core.Round(
                Math.Max(taxCurrentResult, 0), Common.TaxRules.IncomePreTaxRounding) * Common.TaxRules.IncomeTaxRate, Common.TaxRules.IncomePostTaxRounding);

            decimal taxStartResult = Calendar.IsEndOfYear(start) ? 0 : Book.TaxBook.GetResult(start, true);
            decimal taxStart = Core.Round(Core.Round(
                Math.Max(taxStartResult, 0), Common.TaxRules.IncomePreTaxRounding) * Common.TaxRules.IncomeTaxRate, Common.TaxRules.IncomePostTaxRounding);

            decimal taxChange = taxCurrent - taxStart;

            BookingProperties bp = new BookingProperties() { TaxCharge = true };

            long index = Book.GetOperationIndex();
            if (Calendar.IsEndOfYear(end))
            {
                Book.Enter(IncomeTax, end, 0, index, 0, "Tax payment (mid-year assessment derecognition)", null, -taxStart, bp);
                Book.Enter(AccruedExpense, end, 0, index, 0, "Tax payment (mid-year assessment derecognition)", null, taxStart);
                Book.Enter(IncomeTax, end, 0, index, 0, "Tax payment", null, taxCurrent, bp);
                Book.Enter(TaxLiabilitiesRecognition, end, 0, index, 0, "Tax payment", null, -taxCurrent);
            }
            else
            {
                Book.Enter(IncomeTax, end, 0, index, 0, "Tax payment (mid-year assessment)", null, taxChange, bp);
                Book.Enter(AccruedExpense, end, 0, index, 0, "Tax payment (mid-year assessment)", null, -taxChange);
            }
        }

    }
}
