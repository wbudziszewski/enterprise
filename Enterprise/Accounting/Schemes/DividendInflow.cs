﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Represents inflow of dividend from some equity-type investment.
    /// </summary>
    public class DividendInflow : AssetSpecificAccountingScheme
    {
        public DividendInflow(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account for cash settlement of dividend (inflow)
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("DividendInflow", "CashSettlement", inv);

            /// <summary>
            /// Result account for recognition of ordinary income.
            /// </summary>
            var OrdinaryIncome = Book.GetAccountingActivity("DividendInflow", "OrdinaryIncome", inv);

            /// <summary>
            /// Result account for recognition of dividend receivables.
            /// </summary>
            var ReceivablesRecognition = Book.GetAccountingActivity("DividendInflow", "ReceivablesRecognition", inv);

            /// <summary>
            /// Tax account for recognition of income tax on dividend.
            /// </summary>
            var TaxRecognition = Book.GetAccountingActivity("DividendInflow", "TaxRecognition", inv);

            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in DividendInflow accounting scheme. CashSettlement account remains undefined.", this); return; }
            //if (OrdinaryIncome == null) { Log.Report(Severity.Error, "Error setting accounts in DividendInflow accounting scheme. OrdinaryIncome account remains undefined.", this); return; }

            if (OrdinaryIncome == null || CashSettlement == null)
            {
                return;
            }

            //Dividend recognition

            foreach (var e in inv.GetDividends(start, end, Investments.Investment.DividendDate.RecordDate))
            {
                decimal amount = Core.Round((decimal)e.Rate * inv.GetCount(Core.TimeArg.AtDate(e.RecordDate)));
                decimal taxCharge = 0;
                long index = Book.GetOperationIndex();

                if (Book.ApplyTaxRules)
                {
                    if (TaxRecognition != null)
                    {
                        taxCharge = Core.Round(Core.Round(amount, Common.TaxRules.DividendPreTaxRounding) * Common.TaxRules.DividendTaxRate, Common.TaxRules.DividendPostTaxRounding);
                        BookingProperties bp = new BookingProperties() { TaxCharge = true };
                        Book.Enter(TaxRecognition, e.settleDate, inv.Index, index, 0, "Dividend (tax recognition)", inv.Portfolio, taxCharge, bp);
                    }
                    Book.Enter(CashSettlement, e.settleDate, inv.Index, index, 0, "Dividend (cash inflow)", inv.Portfolio, amount - taxCharge);
                    Book.Enter(OrdinaryIncome, e.settleDate, inv.Index, index, 0, "Dividend (ordinary income)", inv.Portfolio, -amount);
                }
                else
                {
                    if (TaxRecognition != null)
                    {
                        taxCharge = Core.Round(Core.Round(amount, Common.TaxRules.DividendPreTaxRounding) * Common.TaxRules.DividendTaxRate, Common.TaxRules.DividendPostTaxRounding);
                        BookingProperties bp = new BookingProperties() { TaxCharge = true };
                        Book.Enter(TaxRecognition, e.settleDate, inv.Index, index, 0, "Dividend (tax recognition)", inv.Portfolio, taxCharge, bp);
                    }
                    Book.Enter(ReceivablesRecognition, e.RecordDate, inv.Index, index, 0, "Dividend (receivable)", inv.Portfolio, amount - taxCharge);
                    Book.Enter(CashSettlement, e.settleDate, inv.Index, index, 0, "Dividend (cash inflow)", inv.Portfolio, amount - taxCharge);
                    Book.Enter(ReceivablesRecognition, e.RecordDate, inv.Index, index, 0, "Dividend (receivable)", inv.Portfolio, -(amount - taxCharge));
                    Book.Enter(OrdinaryIncome, e.RecordDate, inv.Index, index, 0, "Dividend (ordinary income)", inv.Portfolio, -amount);
                }
            }
        }
    }
}
