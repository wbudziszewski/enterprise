﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Books inflows from debt instruments (i.e. coupons, redemptions, etc.)
    /// </summary>
    public class RedemptionInflow : AssetSpecificAccountingScheme
    {
        public RedemptionInflow(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account for cash settlement of redemption (inflow)
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("RedemptionInflow", "CashSettlement", inv);

            /// <summary>
            /// Result account for recognition of ordinary income.
            /// </summary>
            var OrdinaryIncome = Book.GetAccountingActivity("RedemptionInflow", "OrdinaryIncome", inv);

            /// <summary>
            /// Tax account for recognition of income tax on redemption.
            /// </summary>
            var TaxRecognition = Book.GetAccountingActivity("RedemptionInflow", "TaxRecognition", inv);

            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in RedemptionInflow accounting scheme. CashSettlement account remains undefined.", this); return; }
            //if (OrdinaryIncome == null) { Log.Report(Severity.Error, "Error setting accounts in RedemptionInflow accounting scheme. OrdinaryIncome account remains undefined.", this); return; }

            if (OrdinaryIncome == null || CashSettlement == null)
            {
                return;
            }

            foreach (var e in inv.GetEvents<Events.Redemption>(start, end))
            {
                decimal amount = Core.Round((decimal)e.Rate * inv.GetNominalAmount(Core.TimeArg.BeforeEvent(e), true, Book.ApplyTaxRules));
                decimal taxCharge = 0;

                long index = Book.GetOperationIndex();
                // TODO: redemption taxation!
                if (TaxRecognition != null)
                {
                    taxCharge = Core.Round(Core.Round(amount, Common.TaxRules.CouponPreTaxRounding) * Common.TaxRules.CouponTaxRate, Common.TaxRules.CouponPostTaxRounding);
                    BookingProperties bp = new BookingProperties() { TaxCharge = true };
                    Book.Enter(TaxRecognition, e.settleDate, inv.Index, index, 0, "Redemption (tax recognition)", inv.Portfolio, taxCharge, bp);
                }
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, 0, "Redemption (cash inflow)", inv.Portfolio, amount - taxCharge);
                Book.Enter(OrdinaryIncome, e.settleDate, inv.Index, index, 0, "Redemption (ordinary income)", inv.Portfolio, -amount);
            }
        }
    }
}
