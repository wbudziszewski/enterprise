﻿using System;
using System.Linq;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Processes book closing, i.e. booking all result accounts balances onto 'net result' account and then on the first day of the next year, booking this result onto 'net result of previous year(s)' account.
    /// </summary>
    [Priority(-10)]
    public class BookClosing : GeneralLedgerAccountingScheme
    {
        public BookClosing(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset where deferred tax is booked.
            /// </summary>
            var CurrentPeriodResult = Book.GetAccountingActivity("BookClosing", "CurrentPeriodResult");

            /// <summary>
            /// Account where tax that will be deducted and paid from current year's result is booked.
            /// </summary>
            var PriorPeriodResult = Book.GetAccountingActivity("BookClosing", "PriorPeriodResult");

            //if (CurrentPeriodResult == null) { Log.Report(Severity.Error, "Error setting accounts in BookClosing accounting scheme. CurrentPeriodResult account remains undefined.", this); return; }
            //if (PriorPeriodResult == null) { Log.Report(Severity.Error, "Error setting accounts in BookClosing accounting scheme. PriorPeriodResult account remains undefined.", this); return; }

            if (CurrentPeriodResult == null || PriorPeriodResult == null)
            {
                return;
            }

            decimal result = 0;

            BookingProperties bp = new BookingProperties() { YearClosing = true };

            long index = Book.GetOperationIndex();
            result = Book.GetResult(end, false) - (Calendar.IsEndOfYear(start) ? 0 : Book.GetResult(start, false));
            Book.Enter(CurrentPeriodResult, end, 0, index, 0, "Result of reporting period", null, -result);

            // End of year
            if (Calendar.IsEndOfYear(end))
            {
                result = 0;

                foreach (var a in Book.Accounts.Values.Where(x => x.IsResultAccount))
                {
                    decimal amount = a.Get(new AccountQuery() { End = end }).Sum(x => x.Amount);
                    Book.Enter(a, end, 0, index, 0, "Result of reporting period", null, -amount, bp);
                    result += amount;
                }

                Book.Enter(PriorPeriodResult, end, 0, index, 0, "Result of closed period", null, result, bp);
                Book.Enter(CurrentPeriodResult, end, 0, index, 0, "Result of closed period", null, -result, bp);
            }
        }
    }
}
