﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Represents actual payment of tax liability (cash outflow)
    /// </summary>
    public class TaxPayment : AssetSpecificAccountingScheme
    {
        public TaxPayment(Book book, Definitions.AccountingScheme def) : base(book, def)
        {
        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Cash account from which actual payment is deducted.
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("TaxPayment", "CashSettlement");

            /// <summary>
            /// Tax liabilities which are derecognized upon payment.
            /// </summary>
            var TaxLiabilitiesDerecognition = Book.GetAccountingActivity("TaxPayment", "TaxLiabilitiesDerecognition");

            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in TaxPayment accounting scheme. CashSettlement account remains undefined.", this); return; }
            //if (TaxLiabilitiesDerecognition == null) { Log.Report(Severity.Error, "Error setting accounts in TaxPayment accounting scheme. TaxLiabilitiesDerecognition account remains undefined.", this); return; }

            if (TaxLiabilitiesDerecognition == null || CashSettlement == null)
            {
                return;
            }

            long index = Book.GetOperationIndex();
            foreach (var e in inv.GetEvents<Events.Outflow>(start, end).Where(x => x.TaxPayment))
            {
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Tax payment: " + inv, inv.Portfolio, -e.Amount);
                Book.Enter(TaxLiabilitiesDerecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Tax payment: " + inv, inv.Portfolio, e.Amount);
            }
        }
    }
}
