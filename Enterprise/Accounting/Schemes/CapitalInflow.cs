﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Represents capital inflow (cash transfer) outside of investment activity.
    /// </summary>
    public class CapitalInflow : AssetSpecificAccountingScheme
    {
        public CapitalInflow(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account for cash inflow
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("CapitalInflow", "CashSettlement", inv);

            /// <summary>
            /// Result account for recognition of equity (capital) increase
            /// </summary>
            var EquityIncrease = Book.GetAccountingActivity("CapitalInflow", "EquityIncrease", inv);

            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in CapitalInflow accounting scheme. CashSettlement account remains undefined.", this); return; }
            //if (EquityIncrease == null) { Log.Report(Severity.Error, "Error setting accounts in CapitalInflow accounting scheme. EquityIncrease account remains undefined.", this); return; }

            if (CashSettlement == null || EquityIncrease == null)
            {
                return;
            }


            foreach (var e in inv.GetEvents<Events.Inflow>(start, end).Where(x => x.ExternalInflow))
            {
                long index = Book.GetOperationIndex();
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Capital inflow (cash inflow)", inv.Portfolio, e.Amount);
                Book.Enter(EquityIncrease, e.settleDate, inv.Index, index, e.TransactionIndex, "Capital inflow (equity increase) ", inv.Portfolio, -e.Amount);
            }
        }
    }
}
