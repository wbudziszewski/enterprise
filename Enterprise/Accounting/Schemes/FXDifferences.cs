﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Calculates effects of market valuation of an asset at the end of each period.
    /// </summary>
    [Priority(-2)]
    public class FXDifferences : AssetSpecificAccountingScheme
    {
        public FXDifferences(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account where change in market valuation will be recognized
            /// </summary>
            var AssetValuation = Book.GetAccountingActivity("FXDifferences", "AssetValuation", inv);

            /// <summary>
            /// Account where unrealized gains/losses from market valuation will be recognized
            /// </summary>
            var UnrealizedFXResultRecognition = Book.GetAccountingActivity("FXDifferences", "UnrealizedFXResultRecognition", inv);

            //if (AssetValuation == null) { Log.Report(Severity.Error, "Error setting accounts in FXDifferences accounting scheme. AssetValuation account remains undefined.", this); return; }
            //if (UnrealizedFXResultRecognition == null) { Log.Report(Severity.Error, "Error setting accounts in FXDifferences accounting scheme. UnrealizedFXResultRecognition account remains undefined.", this); return; }

            if (AssetValuation == null || UnrealizedFXResultRecognition == null)
            {
                return;
            }

            long index = Book.GetOperationIndex();
            decimal income = inv.GetUnrealizedGainsLossesFromFX(Core.TimeArg.AtDate(start), Core.TimeArg.AtDate(end), true, Book.ApplyTaxRules, UnrealizedFXResultRecognition.InvolvesResultAccount);

            Book.Enter(AssetValuation, end, inv.Index, index, 0, "FX differences (change of asset value)", inv.Portfolio, income);
            Book.Enter(UnrealizedFXResultRecognition, end, inv.Index, index, 0, "FX differences (unrealized gains/losses recognition)", inv.Portfolio, -income);
        }
    }
}
