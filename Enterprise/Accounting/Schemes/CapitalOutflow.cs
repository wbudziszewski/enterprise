﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Represents capital outflow (cash transfer) out from investment activity.
    /// </summary>
    public class CapitalOutflow : AssetSpecificAccountingScheme
    {
        public CapitalOutflow(Book book, Definitions.AccountingScheme def) : base(book, def)
        {

        }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Asset account for cash outflow
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("CapitalOutflow", "CashSettlement", inv);

            /// <summary>
            /// Result account for recognition of equity (capital) decrease
            /// </summary>
            var EquityDecrease = Book.GetAccountingActivity("CapitalOutflow", "EquityDecrease", inv);

            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in CapitalOutflow accounting scheme. CashSettlement account remains undefined.", this); return; }
            //if (EquityDecrease == null) { Log.Report(Severity.Error, "Error setting accounts in CapitalOutflow accounting scheme. EquityDecrease account remains undefined.", this); return; }

            if (CashSettlement == null || EquityDecrease == null)
            {
                return;
            }

            foreach (var e in inv.GetEvents<Events.Outflow>(start, end).Where(x => x.ExternalOutflow))
            {
                long index = Book.GetOperationIndex();
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Capital outflow (cash outflow)", inv.Portfolio, e.Amount);
                Book.Enter(EquityDecrease, e.settleDate, inv.Index, index, e.TransactionIndex, "Capital outflow (equity decrease) ", inv.Portfolio, -e.Amount);
            }
        }
    }
}
