﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Performs bookings for asset purchase.
    /// </summary>
    public class AssetRecognition : AssetSpecificAccountingScheme
    {
        public AssetRecognition(Book book, Definitions.AccountingScheme def) : base(book, def)
        { }

        public override void Process(Investments.Investment inv, DateTime start, DateTime end)
        {
            /// <summary>
            /// Assets account for recognition of purchased (created) asset
            /// </summary>
            var AssetRecognition = Book.GetAccountingActivity("AssetRecognition", "AssetRecognition", inv);

            /// <summary>
            /// Payables account for temporary recognition of payable between trade date and settlement date
            /// </summary>
            var PayableRecognition = Book.GetAccountingActivity("AssetRecognition", "PayableRecognition", inv);

            /// <summary>
            /// Cash account from which payments would be made
            /// </summary>
            var CashSettlement = Book.GetAccountingActivity("AssetRecognition", "CashSettlement", inv);

            /// <summary>
            /// Cost account for fee recognition
            /// </summary>
            var FeeRecognition = Book.GetAccountingActivity("AssetRecognition", "FeeRecognition", inv);

            //if (AssetRecognition == null) { Log.Report(Severity.Error, "Error setting accounts in AssetRecognition accounting scheme. AssetRecognition account remains undefined.", this); return; }
            //if (CashSettlement == null) { Log.Report(Severity.Error, "Error setting accounts in AssetRecognition accounting scheme. CashSettlement account remains undefined.", this); return; }

            if (AssetRecognition == null || CashSettlement == null)
            {
                return;
            }


            decimal purchaseAmount = 0;
            decimal feeAmount = 0;

            // First look for purchase events, applicable for tradeable securities            
            foreach (var e in inv.GetEvents<Events.Purchase>(start, end))
            {
                purchaseAmount = e.Amount;
                if (FeeRecognition != null)
                {
                    feeAmount = e.Fee;
                }
                if (e.evtDate != e.settleDate && PayableRecognition == null)
                {
                    Log.Report(Severity.Error, "Error setting accounts in AssetPurchase accounting scheme. PayableRecognition account remains undefined.", this);
                    return;
                }

                long index = Book.GetOperationIndex();

                Book.Enter(AssetRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset purchase (asset recognition)", inv.Portfolio, purchaseAmount);
                if (e.evtDate != e.settleDate)
                {
                    Book.Enter(PayableRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset purchase (payable recognition)", inv.Portfolio, -purchaseAmount);
                    Book.Enter(PayableRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset purchase (payable derecognition)", inv.Portfolio, purchaseAmount);
                }
                Book.Enter(CashSettlement, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset purchase (fee payment)", inv.Portfolio, -feeAmount);
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset purchase (purchase amount payment)", inv.Portfolio, -purchaseAmount);
                if (FeeRecognition != null)
                {
                    Book.Enter(FeeRecognition, e.evtDate, inv.Index, index, e.TransactionIndex, "Asset purchase (fee cost recognition)", inv.Portfolio, feeAmount);
                }
            }

            // Then look for inflow events, applicable for non-tradeable investments, e.g. cash
            foreach (var e in inv.GetEvents<Events.Inflow>(start, end))
            {
                long index = Book.GetOperationIndex();

                Book.Enter(AssetRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset recognition", inv.Portfolio, purchaseAmount);
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset recognition (fee payment)", inv.Portfolio, -feeAmount);
                Book.Enter(CashSettlement, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset recognition (amount payment)", inv.Portfolio, -purchaseAmount);
                if (FeeRecognition != null)
                {
                    Book.Enter(FeeRecognition, e.settleDate, inv.Index, index, e.TransactionIndex, "Asset recognition (fee cost recognition)", inv.Portfolio, feeAmount);
                }
            }
        }
    }
}
