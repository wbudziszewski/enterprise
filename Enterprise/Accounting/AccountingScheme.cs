﻿using System;
using System.Collections.Generic;

namespace Budziszewski.Enterprise.Accounting
{
    //public enum SchemeCondition { Unspecified, AssetClass, ValuationClass }

    /// <summary>
    /// Represents set of activities that need to be done to accomplish some accounting process.
    /// </summary>
    public abstract class AccountingScheme
    {
        protected Book Book { get; private set; }

        public string Name { get; protected set; }

        public List<AccountingSchemeActivity> Activities { get; private set; } = new List<AccountingSchemeActivity>();

        public AccountingScheme(Book book, Definitions.AccountingScheme def)
        {
            Book = book;
            Name = def.Name;
            //Guid = System.Guid.NewGuid().ToString();
        }

        public void AddActivity(Definitions.AccountingScheme def)
        {
            Account dtAccount = null;
            Account ctAccount = null;

            if (!String.IsNullOrEmpty(def.DtAccount))
            {
                if (!Book.Accounts.ContainsKey(def.DtAccount))
                {
                    Log.Report(Severity.Error, String.Format("Unknown account: {0} (book: {1}) referenced in an accounting scheme: {2}.", def.DtAccount, Book.Name, def.Name), this, null);
                    dtAccount = null;
                }
                else
                {
                    dtAccount = Book.Accounts[def.DtAccount];
                }
            }
            if (!String.IsNullOrEmpty(def.CtAccount))
            {
                if (!Book.Accounts.ContainsKey(def.CtAccount))
                {
                    Log.Report(Severity.Error, String.Format("Unknown account: {0} (book: {1}) referenced in an accounting scheme: {2}.", def.CtAccount, Book.Name, def.Name), this, null);
                    ctAccount = null;
                }
                else
                {
                    ctAccount = Book.Accounts[def.CtAccount];
                }
            }

            Activities.Add(new AccountingSchemeActivity(def.Name, def.Activity, dtAccount, ctAccount, def.Conditions));
        }
    }

    /// <summary>
    /// Represents set of activities that need to be done to accomplish some accounting process where the scheme applies to specific asset (will be called for every applicable one).
    /// </summary>
    public abstract class AssetSpecificAccountingScheme : AccountingScheme
    {
        public AssetSpecificAccountingScheme(Book book, Definitions.AccountingScheme def) : base(book, def)
        { }

        public abstract void Process(Investments.Investment inv, DateTime start, DateTime end);
    }

    /// <summary>
    /// Represents set of activities that need to be done to accomplish some accounting process where the scheme does not apply to any specific asset (will be called once per whole ledger).
    /// </summary>
    public abstract class GeneralLedgerAccountingScheme : AccountingScheme
    {
        public GeneralLedgerAccountingScheme(Book book, Definitions.AccountingScheme def) : base(book, def)
        { }

        public abstract void Process(DateTime start, DateTime end);
    }

    public class AccountingSchemeActivity
    {
        public string Scheme { get; private set; }
        public string Name { get; private set; }
        public Account DtAccount { get; private set; }
        public Account CtAccount { get; private set; }
        public bool InvolvesResultAccount
        {
            get
            {
                return (DtAccount != null && DtAccount.IsResultAccount) || (CtAccount != null && CtAccount.IsResultAccount);
            }
        }

        private ConditionEngine conditions;

        public AccountingSchemeActivity(string scheme, string name, Account dtAccount, Account ctAccount, string cond)
        {
            Scheme = scheme;
            Name = name;
            DtAccount = dtAccount;
            CtAccount = ctAccount;
            conditions = new ConditionEngine(cond);
        }

        public bool MeetsConditions(Investments.Investment inv)
        {
            return conditions.Solve(inv);
        }

        public override string ToString()
        {
            return Scheme + ": " + Name + " (" + conditions + ")";
        }
    }
}
