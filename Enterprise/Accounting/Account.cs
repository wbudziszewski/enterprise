﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Budziszewski.Enterprise.Accounting
{
    /// <summary>
    /// Represents a book account.
    /// </summary>
    public class Account
    {
        public string Number { get; private set; }

        public string Name { get; private set; }

        public bool IsResultAccount { get; private set; }

        private List<Booking> entries;

        public ReadOnlyCollection<Booking> Entries { get { return entries.AsReadOnly(); } }

        public Account()
        {
            entries = new List<Booking>();
        }

        public Account(Definitions.BookAccount definition) : this()
        {
            Number = definition.Number;
            Name = definition.Name;
            IsResultAccount = definition.IsResultAccount;
        }

        public void AddEntry(DateTime date, long invIndex, long operIndex, long transIndex, string description, string portfolio, decimal amount, BookingProperties properties)
        {
            if (amount == 0)
            {
                return;
            }
            Booking b = new Booking(date, invIndex, operIndex, transIndex, description, portfolio, amount, properties);

            // Add a booking
            for (int i = 0; i < Entries.Count; i++)
            {
                if (b.Date < entries[i].Date)
                {
                    entries.Insert(i, b);
                    return;
                }
            }
            entries.Add(b);
        }

        public IEnumerable<Booking> Get(AccountQuery q)
        {
            DateTime end = q.End ?? Common.CurrentDate;
            DateTime start = q.Start ?? (IsResultAccount ? Calendar.ReachEndOfYear(end, -1) : DateTime.MinValue);

            for (int i = 0; i < Entries.Count; i++)
            {
                if (q.Investment != null && Entries[i].InvestmentIndex != q.Investment.Index) continue;
                if (q.Transaction != null && Entries[i].TransactionIndex != q.Transaction.Index) continue;

                // If tax charge is not null
                if (q.IsTaxCharge == false && Entries[i].Properties.TaxCharge) continue;
                if (q.IsTaxCharge == true && !Entries[i].Properties.TaxCharge) continue;

                switch (q.BooksClosing)
                {
                    // omit year closing bookings
                    case BooksClosingQuery.None:
                        if (Entries[i].Properties.YearClosing == true) continue;
                        break;
                    // omit only closing booking on the final date
                    case BooksClosingQuery.IncludePreviousYears:
                        if (Entries[i].Properties.YearClosing == true && Entries[i].Date == end) continue;
                        break;
                    // do not omit any bookings
                    case BooksClosingQuery.IncludeAll:
                        break;
                    // list only year closing bookings
                    case BooksClosingQuery.IncludeOnly:
                        if (Entries[i].Properties.YearClosing == false) continue;
                        break;
                    default:
                        break;
                }

                if (Entries[i].Date <= start) continue;
                if (Entries[i].Date > end)
                {
                    break;
                }
                yield return Entries[i];
            }
        }

        public void Clear()
        {
            entries.Clear();
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", Number, Name);
        }
    }

    public class AccountQuery
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public Investments.Investment Investment { get; set; }
        public Definitions.Transaction Transaction { get; set; }
        public BooksClosingQuery BooksClosing = BooksClosingQuery.IncludePreviousYears;
        public bool? IsTaxCharge = null;

        public DateTime? Date
        {
            get { return End; }
            set { Start = null; End = value; }
        }
    }
}
