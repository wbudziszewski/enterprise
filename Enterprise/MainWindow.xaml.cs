﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Budziszewski.Enterprise
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UserControl CurrentTransaction;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            Database.Purge();
            Common.Initialize(this);

            Log.Reported += Log_Reported;
        }

        /// <summary>
        /// Reacts to any additional entry into the log.
        /// </summary>
        /// <param name="e">Event arguments</param>
        private void Log_Reported(object sender, Log.ReporterEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                // Report errors and critical errors in status bar text.
                if (e.UnreadCriticalErrors > 0 || e.UnreadErrors > 0)
                {
                    LabelReporter.Visibility = Visibility.Visible;
                    IconStatusWarning.Visibility = Visibility.Collapsed;
                    IconStatusError.Visibility = Visibility.Visible;
                    TextReporter.Text = String.Format("Unread log messages: {0}{1}{2}{3}{4}",
                        e.UnreadCriticalErrors > 0 ? e.UnreadCriticalErrors + " critical errors" : "",
                        e.UnreadCriticalErrors > 0 && (e.UnreadErrors > 0 || e.UnreadWarnings > 0) ? ", " : "",
                        e.UnreadErrors > 0 ? e.UnreadErrors + " errors" : "",
                        e.UnreadErrors > 0 && e.UnreadWarnings > 0 ? ", " : "",
                        e.UnreadWarnings > 0 ? e.UnreadWarnings + " warnings" : "");
                }
                // Report warnings in status bar text.
                else if (e.UnreadWarnings > 0)
                {
                    LabelReporter.Visibility = Visibility.Visible;
                    IconStatusWarning.Visibility = Visibility.Visible;
                    IconStatusError.Visibility = Visibility.Collapsed;
                    TextReporter.Text = String.Format("Unread log messages: {0} warnings", e.UnreadWarnings);
                }
                // Hide status bar text if no errors encountered.
                else
                {
                    LabelReporter.Visibility = Visibility.Collapsed;
                    IconStatusWarning.Visibility = Visibility.Collapsed;
                    IconStatusError.Visibility = Visibility.Collapsed;
                    TextReporter.Text = "";
                }
            });
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Log.Report(Severity.Information, "Log initialized.");
        }

        private void Main_Closed(object sender, EventArgs e)
        {
            Common.Close();
        }

        private void ModuleMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainPanel.Children.Clear();
            UserControl control;
            switch ((sender as MenuItem).Tag.ToString())
            {
                case "ValueEditor": control = new Modules.ValueEditor(); break;
                case "LogViewer": control = new Modules.LogViewer(); break;
                default: Log.Report(Severity.Error, "Unknown module identifier.", sender); return;
            }
            CurrentTransaction = control;
            MainPanel.Children.Add(control);
        }
    }
}
