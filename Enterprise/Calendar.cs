﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Budziszewski.Enterprise
{
    public enum Direction { Backwards, Forward }

    public static class Calendar
    {
        private readonly static int endOfFinancialYear = 12;

        public static bool IsEndOfYear(DateTime date)
        {
            if (date.Month == endOfFinancialYear) return date.Day == DateTime.DaysInMonth(date.Year, date.Month);
            return false;
        }

        public static bool IsEndOfMonth(DateTime date)
        {
            return date.Date == new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        public static DateTime ReachEndOfYear(DateTime date, int step)
        {
            DateTime org = date;
            while (step != 0 || !IsEndOfYear(date))
            {
                if (step > 0)
                {
                    date = ReachEndOfMonth(date, 0);
                    while (date == org || !IsEndOfYear(date))
                    {
                        date = ReachEndOfMonth(date, 1);
                    }
                    step--;
                }
                else if (step == 0)
                {
                    date = ReachEndOfMonth(date, 0);
                    while (!IsEndOfYear(date))
                    {
                        date = ReachEndOfMonth(date, 1);
                    }
                }
                else if (step < 0)
                {
                    while (date == org || !IsEndOfYear(date))
                    {
                        date = ReachEndOfMonth(date, -1);
                    }
                    step++;
                }
            }

            return date;
        }

        public static DateTime ReachEndOfMonth(DateTime date, int step)
        {
            if (step != 0)
            {
                date = date.AddMonths(step);
            }
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        public static IEnumerable<DateTime> GetReportingDates(DateTime? start, DateTime? end)
        {
            if (start == null || end == null) yield break;
            DateTime date = ReachEndOfYear(start.Value, 0);
            while (date <= end)
            {
                yield return date;
                date = ReachEndOfYear(date, 1);
            }
        }
    }
}
