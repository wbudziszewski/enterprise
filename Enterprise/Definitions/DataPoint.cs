﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Budziszewski.Enterprise.Modules;
using System.Collections.ObjectModel;

namespace Budziszewski.Enterprise
{
    [Serializable]
    public abstract class DataPoint : IComparable<DataPoint>, IEquatable<DataPoint>
    {
        [HideProperty]
        public abstract string ID { get; }

        public void ImportFromCSV(string[] data, PropertyInfo[] props)
        {
            for (int i = 0; i < Math.Min(data.Length, props.Length); i++)
            {
                if (props[i] == null) continue;
                if (props[i].PropertyType.IsEnum)
                {
                    try
                    {
                        props[i].SetValue(this, Enum.Parse(props[i].PropertyType, data[i], true));
                    }
                    catch (Exception ex)
                    {
                        Log.Report(Severity.Error, "Cannot set enum property.", this, exception: ex);
                    }
                }
                else if (props[i].PropertyType == typeof(string)) props[i].SetValue(this, data[i]);
                else if (props[i].PropertyType == typeof(bool)) props[i].SetValue(this, Conversion.ToBoolean(data[i]));
                else if (props[i].PropertyType == typeof(DateTime?)) props[i].SetValue(this, Conversion.ToDateTimeOrNull(data[i]));
                else if (props[i].PropertyType == typeof(DateTime)) props[i].SetValue(this, Conversion.ToDateTime(data[i]));
                else if (props[i].PropertyType == typeof(float)) props[i].SetValue(this, Conversion.ToSingle(data[i]));
                else if (props[i].PropertyType == typeof(double)) props[i].SetValue(this, Conversion.ToDouble(data[i]));
                else if (props[i].PropertyType == typeof(Int64)) props[i].SetValue(this, Conversion.ToInt64(data[i]));
                else if (props[i].PropertyType == typeof(Int32)) props[i].SetValue(this, Conversion.ToInt32(data[i]));
                else if (props[i].PropertyType == typeof(decimal)) props[i].SetValue(this, Conversion.ToDecimal(data[i]));
                else throw new Exception("Unknown property type.");
            }
        }

        /// <summary>
        /// Perform a deep copy of the object.
        /// </summary>
        /// <returns>The copied object.</returns>
        public object Clone()
        {
            if (!this.GetType().IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(this, null))
            {
                return null;
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                return formatter.Deserialize(stream);
            }
        }

        public override string ToString()
        {
            return ID;
        }

        public virtual int CompareTo(DataPoint other)
        {
            return String.CompareOrdinal(ID, other.ID);
        }

        public virtual bool Equals(DataPoint other)
        {
            return String.CompareOrdinal(ID, other.ID) == 0;
        }

        public override bool Equals(object other)
        {
            if (!(other is DataPoint)) return false;
            return Equals((DataPoint)other);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }
    }

    public class ValueComparer : IEqualityComparer<DataPoint>
    {

        public bool Equals(DataPoint x, DataPoint y)
        {
            if (Object.ReferenceEquals(x, y)) return true;
            return x.Equals(y);
        }

        public int GetHashCode(DataPoint obj)
        {
            return obj.GetHashCode();
        }
    }
}
