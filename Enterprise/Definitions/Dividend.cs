﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    public class Dividend : DataPoint
    {
        public override string ID
        {
            get { return InvestmentID + "_" + Conversion.ToString(paymentDate); }
        }

        string investmentID;
        [External]
        [Headers("isin")]
        public string InvestmentID
        {
            get { return investmentID; }
            set { investmentID = value; }
        }

        DateTime recordDate;
        [External]
        [Headers("recordDate")]
        public DateTime RecordDate
        {
            get { return recordDate; }
            set { recordDate = value; }
        }

        DateTime exDate;
        [External]
        [Headers("exDate")]
        public DateTime ExDate
        {
            get { return exDate; }
            set { exDate = value; }
        }

        DateTime paymentDate;
        [External]
        [Headers("paymentDate")]
        public DateTime PaymentDate
        {
            get { return paymentDate; }
            set { paymentDate = value; }
        }

        string currency;
        [External]
        [Headers("currency")]
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        double paymentPerShare;
        [External]
        [Headers("paymentPerShare")]
        public double PaymentPerShare
        {
            get { return paymentPerShare; }
            set { paymentPerShare = value; }
        }
    }
}
