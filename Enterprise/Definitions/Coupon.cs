﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    public class Coupon : DataPoint
    {
        public override string ID
        {
            get { return InvestmentID + "_" + Conversion.ToString(couponDate); }
        }

        string investmentID;
        [External]
        [Headers("isin")]
        public string InvestmentID
        {
            get { return investmentID; }
            set { investmentID = value; }
        }

        DateTime couponDate;
        [External]
        [Headers("couponDate")]
        public DateTime CouponDate
        {
            get { return couponDate; }
            set { couponDate = value; }
        }

        double couponRate;
        [External]
        [Headers("couponRate")]
        [PercentageProperty]
        [PresentationPrecision(3)]
        public double CouponRate
        {
            get { return couponRate; }
            set { couponRate = value; }
        }
    }
}
