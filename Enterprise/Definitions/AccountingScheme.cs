﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    /// <summary>
    /// Represents an account that takes part in a given accounting scheme.
    /// </summary>
    public class AccountingScheme : DataPoint
    {
        public override string ID
        {
            get { return String.Format("{0}_{1}_{2}_{3}", BookID, Name, Activity, Conditions); }
        }

        string bookID;
        [External]
        [Headers("book")]
        public string BookID
        {
            get { return bookID; }
            set { bookID = value; }
        }

        string name;
        [External]
        [Headers("accountingScheme", "name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string activity;
        [External]
        [Headers("activity")]
        public string Activity
        {
            get { return activity; }
            set { activity = value; }
        }

        string dtAccount;
        [External]
        [Headers("dtAccount", "dtAccountNo")]
        public string DtAccount
        {
            get { return dtAccount; }
            set { dtAccount = value; }
        }

        string ctAccount;
        [External]
        [Headers("ctAccount", "ctAccountNo")]
        public string CtAccount
        {
            get { return ctAccount; }
            set { ctAccount = value; }
        }

        string conditions;
        [External]
        [Headers("conditions")]
        public string Conditions
        {
            get { return conditions; }
            set { conditions = value; }
        }
    }
}
