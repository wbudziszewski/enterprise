﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    public class BookAccount : DataPoint
    {
        public override string ID
        {
            get { return number; }
        }

        string number;
        [External]
        [Headers("number", "id")]
        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        string name;
        [External]
        [Headers("name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        bool isResultAccount;
        [External]
        [Headers("isResultAccount")]
        public bool IsResultAccount
        {
            get { return isResultAccount; }
            set { isResultAccount = value; }
        }
    }
}
