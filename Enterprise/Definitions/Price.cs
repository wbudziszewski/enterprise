﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    [Serializable]
    public class Price : DataPoint, Financial.Calendar.IDate
    {
        public override string ID
        {
            get
            {
                return InstrumentID + "_" + date.ToString("yyyyMMdd");
            }
        }

        string instrumentID;
        [External]
        [Headers("instrument", "<INSTRUMENT>")]
        public string InstrumentID
        {
            get
            {
                return instrumentID;
            }
            set
            {
                instrumentID = value;
            }
        }

        DateTime date;
        [External]
        [Headers("date", "<DTYYYYMMDD>")]
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        double value;
        [External]
        [Headers("close", "<CLOSE>")]
        public double Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        public bool IsFXRate { get; set; }

        public Price() : base()
        {

        }

        public Price(string instrument, DateTime date, double price) : base()
        {
            this.InstrumentID = instrument;
            this.Date = date;
            this.Value = price;
        }

        public override int CompareTo(DataPoint other)
        {
            if (other is Price)
            {
                return String.CompareOrdinal(this.ID, ((Price)other).ID);
            }
            else
            {
                return base.CompareTo(other);
            }
        }

        public void Divide(double divisor)
        {
            if (divisor <= 0) return;
            if (IsFXRate) return;
            Value /= divisor;
        }

        public void Multiply(double divisor)
        {
            if (divisor <= 0) return;
            if (IsFXRate) return;
            Value *= divisor;
        }
    }
}
