﻿using System;

namespace Budziszewski.Enterprise.Definitions
{
    [Serializable]
    public class Transaction : DataPoint
    {
        private long index;
        public long Index
        {
            get { return index; }
            set { index = value; }
        }

        public override string ID
        {
            get
            {
                return Index.ToString();
            }
        }

        private TransactionType type;
        [External]
        [Headers("type")]
        public TransactionType Type
        {
            get { return type; }
            set { type = value; }
        }

        private string instrumentID;
        [External]
        [Headers("instrument", "instrumentid")]
        public string InstrumentID
        {
            get { return instrumentID; }
            set { instrumentID = value; }
        }

        private DateTime tradeDate;
        [External]
        [Headers("tradedate")]
        public DateTime TradeDate
        {
            get { return tradeDate; }
            set { tradeDate = value; }
        }

        private DateTime settlementDate;
        [External]
        [Headers("settlementdate")]
        public DateTime SettlementDate
        {
            get { return settlementDate; }
            set { settlementDate = value; }
        }

        private decimal count;
        [External]
        [Headers("count")]
        public decimal Count
        {
            get { return count; }
            set { count = value; }
        }

        private string currency;
        [External]
        [Headers("currency")]
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        private decimal nominalAmount;
        [External]
        [Headers("nominalamount")]
        public decimal NominalAmount
        {
            get { return nominalAmount; }
            set { nominalAmount = value; }
        }

        private double price;
        [External]
        [Headers("price")]
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        private decimal fee;
        [External]
        [Headers("fee")]
        public decimal Fee
        {
            get { return fee; }
            set { fee = value; }
        }

        private string accountSrc;
        [External]
        [Headers("AccountSrc")]
        public string AccountSrc
        {
            get { return accountSrc; }
            set { accountSrc = value; }
        }

        private string accountDst;
        [External]
        [Headers("AccountDst")]
        public string AccountDst
        {
            get { return accountDst; }
            set { accountDst = value; }
        }

        private string portfolioSrc;
        [External]
        [Headers("portfolioSrc")]
        public string PortfolioSrc
        {
            get { return portfolioSrc; }
            set { portfolioSrc = value; }
        }

        private string portfolioDst;
        [External]
        [Headers("portfolioDst")]
        public string PortfolioDst
        {
            get { return portfolioDst; }
            set { portfolioDst = value; }
        }

        private string valuationClass;
        [External]
        [Headers("valuationClass")]
        public string ValuationClass
        {
            get { return valuationClass; }
            set { valuationClass = value; }
        }

        private double fxRate;
        [External]
        [Headers("fxRate")]
        public double FXRate
        {
            get { return fxRate; }
            set { fxRate = value == 0 ? 1 : value; }
        }

        private bool taxPayment;
        [External]
        [Headers("TaxPayment")]
        public bool TaxPayment
        {
            get { return taxPayment; }
            set { taxPayment = value; }
        }

        private string equityAccount;
        [External]
        [Headers("EquityAccount")]
        public string EquityAccount
        {
            get { return equityAccount; }
            set { equityAccount = value; }
        }

        public Transaction(): base()
        {

        }

        public Transaction(long index) : base()
        {
            Index = index;
        }

        public decimal GetSettlementAmount(bool fee)
        {
            Instrument instrument;
            switch (Type)
            {
                case TransactionType.Buy:
                    instrument = Definitions.Instrument.GetInstrument(InstrumentID);
                    if (instrument == null)
                    {
                        Log.Report(Severity.Error, "Instrument of the given ID: {" + InstrumentID + "} could not be found. Check instrument definitions if they contain the given entry.", this);
                        return 0;
                    }
                    if (instrument.InstrumentType == "Bond")
                    {
                        double interest = 0;
                        try
                        {
                            interest = Financial.FixedIncome.Interest(SettlementDate, instrument.Maturity.Value, instrument.CouponRate, instrument.CouponFreq, Financial.DayCountConvention.Actual_Actual_Excel);
                        }
                        catch (Exception ex)
                        {
                            Log.Report(Severity.Error, "Error calculating interest in settlement amount.", this, ex);
                        }
                        return Core.Round((NominalAmount * (decimal)(Price + interest) / 100) + (fee ? Fee : 0));
                    }
                    else
                    {
                        return Core.Round(Count * (decimal)Price + (fee ? Fee : 0));
                    }

                case TransactionType.Sell:
                    instrument = Definitions.Instrument.GetInstrument(InstrumentID);
                    if (instrument == null)
                    {
                        Log.Report(Severity.Error, "Instrument of the given ID: {" + InstrumentID + "} could not be found. Check instrument definitions if they contain the given entry.", this);
                        return 0;
                    }

                    if (instrument.InstrumentType == "Bond")
                    {
                        double interest = 0;
                        try
                        {
                            interest = Financial.FixedIncome.Interest(SettlementDate, instrument.Maturity.Value, instrument.CouponRate, instrument.CouponFreq, Financial.DayCountConvention.Actual_Actual_Excel);
                        }
                        catch (Exception ex)
                        {
                            Log.Report(Severity.Error, "Error calculating interest in settlement amount.", this, ex);
                        }
                        return Core.Round((NominalAmount * (decimal)(Price + interest) / 100) + (fee ? Fee : 0));
                    }
                    else
                    {
                        return Core.Round(Count * (decimal)Price - (fee ? Fee : 0));
                    }

                case TransactionType.Cash:
                    return NominalAmount;

                default:
                    return 0;
            }
        }

        public override int CompareTo(DataPoint other)
        {
            if (other is Transaction t)
            {
                return Index.CompareTo(t.Index);
            }
            else
            {
                return base.CompareTo(other);
            }
        }

        public override string ToString()
        {
            return String.Format("{0}.{1} @{2} {3}", Index, Type, SettlementDate.ToString("yyyy-MM-dd"), InstrumentID);
        }
    }

    public enum TransactionType { Buy, Sell, Cash }
}
