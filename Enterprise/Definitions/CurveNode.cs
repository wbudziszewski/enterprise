﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    public class CurveNode : DataPoint
    {
        public override string ID
        {
            get
            {
                if (String.IsNullOrEmpty(Label))
                {
                    return String.Format("{0}_{1}", Currency, date.ToString("yyyyMMdd"));
                }
                else
                {
                    return String.Format("{0}_{1}_{2}", Label, Currency, date.ToString("yyyyMMdd"));
                }
            }
        }

        string label;
        [External]
        [Headers("label")]
        public string Label
        {
            get
            {
                return label;
            }
            set
            {
                label = value;
            }
        }

        string currency;
        [External]
        [Headers("currency")]
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        DateTime date;
        [External]
        [Headers("date")]
        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        double maturity;
        [External]
        [Headers("maturity", "x")]
        public double Maturity
        {
            get
            {
                return maturity;
            }
            set
            {
                maturity = value;
            }
        }

        double value;
        [External]
        [Headers("value", "y")]
        public double Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        public CurveNode() : base()
        {

        }

        public override int CompareTo(DataPoint other)
        {
            if (other is CurveNode c)
            {
                return this.ID.CompareTo(c.ID);
            }
            else
            {
                return base.CompareTo(other);
            }
        }
    }
}
