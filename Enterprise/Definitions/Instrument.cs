﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Definitions
{
    public class Instrument : DataPoint
    {
        public override string ID
        {
            get { return uniqueId; }
        }

        string uniqueId;
        [External]
        [Headers("uniqueid", "id", "isin")]
        public string UniqueID
        {
            get { return uniqueId; }
            set { uniqueId = value; }
        }

        string ticker;
        [External]
        [Headers("ticker")]
        public string Ticker
        {
            get { return ticker; }
            set { ticker = value; }
        }

        string longTicker;
        [External]
        [Headers("longTicker")]
        public string LongTicker
        {
            get { return longTicker; }
            set { longTicker = value; }
        }

        string instrumenttype;
        [External]
        [Headers("instrumenttype")]
        public string InstrumentType
        {
            get { return instrumenttype; }
            set { instrumenttype = value; }
        }

        string name;
        [External]
        [Headers("name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string issuer;
        [External]
        [Headers("issuer")]
        public string Issuer
        {
            get { return issuer; }
            set { issuer = value; }
        }

        DateTime? maturity;
        [External]
        [Headers("maturity")]
        public DateTime? Maturity
        {
            get { return maturity; }
            set { maturity = value; }
        }

        string couponType;
        [External]
        [Headers("coupontype")]
        public string CouponType
        {
            get { return couponType; }
            set { couponType = value; }
        }

        double couponRate;
        [External]
        [Headers("couponrate")]
        [PercentageProperty]
        public double CouponRate
        {
            get { return couponRate; }
            set { couponRate = value; }
        }

        int couponFreq;
        [External]
        [Headers("couponfreq")]
        public int CouponFreq
        {
            get { return couponFreq; }
            set { couponFreq = value; }
        }

        decimal unitPrice;
        [External]
        [Headers("unitprice")]
        public decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        string currency;
        [External]
        [Headers("currency")]
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        string dayCountConvention;
        [External]
        [Headers("daycountconvention")]
        public string DayCountConvention
        {
            get { return dayCountConvention; }
            set { dayCountConvention = value; }
        }

        string endOfMonthConvention;
        [External]
        [Headers("endofmonthconvention")]
        public string EndOfMonthConvention
        {
            get { return endOfMonthConvention; }
            set { endOfMonthConvention = value; }
        }

        public static Definitions.Instrument GetInstrument(string uniqueID)
        {
            try
            {
                if (uniqueID == null) return null;
                Definitions.Instrument output = null;
                output = Database.Instruments.SingleOrDefault(x => x.ID == uniqueID);
                if (output == null) output = Database.Instruments.SingleOrDefault(x => x.UniqueID == uniqueID);
                if (output == null) throw new Exception();
                return output;
            }
            catch
            {
                Log.Report(Severity.Error, "Error finding instrument of the given ID: {" + uniqueID + "}. It does not exist or exists more than once.");
                return null;
            }
        }

        public static Type GetInstrumentType(string type)
        {
            switch (type)
            {
                case "Bond": return typeof(Investments.Bond);
                case "Equity": return typeof(Investments.Equity);
                case "Fund": return typeof(Investments.Fund);
                case "ETF": return typeof(Investments.ETF);
                default: return typeof(object);
            }
        }
    }
}
