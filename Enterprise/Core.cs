﻿using System;

namespace Budziszewski.Enterprise
{
    public static class Core
    {
        /// <summary>
        /// Rounds given amount to nearest decimal, with specified precision.
        /// </summary>
        /// <param name="amount">Amount to be rounded</param>
        /// <param name="decimals">Number of decimals to which the amount will be rounded, default is 2</param>
        /// <returns>Rounded amount</returns>
        public static decimal Round(decimal amount, int decimals = 2)
        {
            return Math.Round(amount, decimals);
        }

        /// <summary>
        /// Rounds given amount to nearest decimal, with specified precision.
        /// </summary>
        /// <param name="amount">Amount to be rounded</param>
        /// <param name="decimals">Number of decimals to which the amount will be rounded, default is 2</param>
        /// <returns>Rounded amount</returns>
        public static decimal Round(double amount, int decimals = 2)
        {
            return Round(amount, decimals);
        }

        /// <summary>
        /// Rounds given amount using specified rounding rules.
        /// </summary>
        /// <param name="amount">Amount to be rounded</param>
        /// <param name="rules">Rounding rules</param>
        /// <returns>Rounded amount</returns>
        public static decimal Round(decimal amount, RoundingRules rules)
        {
            switch (rules)
            {
                case RoundingRules.Unrounded: return amount;
                case RoundingRules.Monetary: return Math.Round(amount, 2);
                case RoundingRules.Integer: return Math.Round(amount);
                case RoundingRules.Floor: return Math.Floor(amount);
                case RoundingRules.Ceiling: return Math.Ceiling(amount);
                default: return amount;
            }
        }

        /// <summary>
        /// Concatenates given set of strings into one string where they are delimited by a given string.
        /// </summary>
        /// <param name="delimiter">Delimiter</param>
        /// <param name="input">Strings to be concatenated</param>
        /// <returns></returns>
        public static string Concat(string delimiter, params string[] input)
        {
            string output = "";
            foreach (var item in input)
            {
                if (String.IsNullOrEmpty(item)) continue;
                if (String.IsNullOrEmpty(output))
                {
                    output = item;
                }
                else
                {
                    output += delimiter;
                    output = item;
                }
            }
            return output.Trim();
        }

        public enum RoundingRules
        {
            /// <summary>
            /// No rounding will be performed.
            /// </summary>
            Unrounded,
            /// <summary>
            /// Monetary rounding (standard rounding to two decimal places) will be performed.
            /// </summary>
            Monetary,
            /// <summary>
            /// Rounding to nearest whole integer will be performed.
            /// </summary>
            Integer,
            /// <summary>
            /// Rounding down to nearest whole integer will be performed.
            /// </summary>
            Floor,
            /// <summary>
            /// Rounding up to nearest whole integer will be performed.
            /// </summary>
            Ceiling
        }

        /// <summary>
        /// Return FX rate at the given date.
        /// </summary>
        /// <param name="currency">Foreign currency</param>
        /// <param name="date">Date</param>
        /// <returns>FX rate</returns>
        public static double GetFXRate(string currency, DateTime date)
        {
            return Database.GetFXRate(currency, date);
        }

        /// <summary>
        /// Return FX rate at the point of time.
        /// </summary>
        /// <param name="currency">Foreign currency</param>
        /// <param name="date">Point of time (date will be used)</param>
        /// <returns>FX rate</returns>
        public static double GetFXRate(string currency, TimeArg time)
        {
            return GetFXRate(currency, time.Date);
        }

        /// <summary>
        /// Defines point of time, used in specifying date and, if necessary, order of activities.
        /// </summary>
        public abstract class TimeArg
        {
            public DateTime Date
            {
                get
                {
                    if (this is AtDateBoundary atdb) return atdb.Arg;
                    if (this is AfterDateBoundary adb) return adb.Arg;
                    if (this is AfterEventBoundary aeb) return aeb.Arg.settleDate;
                    if (this is BeforeDateBoundary bdb) return bdb.Arg;
                    if (this is BeforeEventBoundary beb) return beb.Arg.settleDate;
                    throw new ArgumentException("Unknown type derived from TimeArg.");
                }
            }

            public static TimeArg AtDate(DateTime date)
            {
                return new AtDateBoundary(date);
            }

            public static TimeArg AfterDate(DateTime date)
            {
                return new AfterDateBoundary(date);
            }

            public static TimeArg AfterEvent(Events.Event e)
            {
                return new AfterEventBoundary(e);
            }

            public static TimeArg BeforeDate(DateTime date)
            {
                return new BeforeDateBoundary(date);
            }

            public static TimeArg BeforeEvent(Events.Event e)
            {
                return new BeforeEventBoundary(e);
            }
        }

        public class AtDateBoundary : TimeArg
        {
            public DateTime Arg { get; set; }

            public AtDateBoundary(DateTime arg)
            {
                Arg = arg;
            }
        }

        public class AfterDateBoundary : TimeArg
        {
            public DateTime Arg { get; set; }

            public AfterDateBoundary(DateTime arg)
            {
                Arg = arg;
            }
        }

        public class AfterEventBoundary : TimeArg
        {
            public Events.Event Arg { get; set; }

            public AfterEventBoundary(Events.Event arg)
            {
                Arg = arg;
            }
        }

        public class BeforeDateBoundary : TimeArg
        {
            public DateTime Arg { get; set; }

            public BeforeDateBoundary(DateTime arg)
            {
                Arg = arg;
            }
        }

        public class BeforeEventBoundary : TimeArg
        {
            public Events.Event Arg { get; set; }

            public BeforeEventBoundary(Events.Event arg)
            {
                Arg = arg;
            }
        }
    }
}
