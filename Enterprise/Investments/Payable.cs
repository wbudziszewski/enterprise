﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Investments
{
    public class Payable : Accounts
    {
        public Payable(long index, Events.Inflow e) : base(index, e)
        {
            RegisterAddition(e);
        }

        public override decimal GetRevaluationGainsLosses(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            return 0;
        }
    }
}
