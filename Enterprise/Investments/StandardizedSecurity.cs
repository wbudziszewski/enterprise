﻿using System;

namespace Budziszewski.Enterprise.Investments
{
    public abstract class StandardizedSecurity : Investment
    {
        protected Definitions.Instrument instrument;

        public StandardizedSecurity(long index, InvestmentSetup setup) : base(index, setup)
        { }

        public override void RegisterAddition(Events.Purchase e)
        {
            AddEvent(e);
            GenerateEvents();
        }

        public override decimal RegisterDeduction(Events.Sale e)
        {
            decimal count = Math.Min(e.Count, GetCount(Core.TimeArg.BeforeEvent(e)));
            if (count != 0)
            {
                e.Count = count;
                AddEvent(e);
            }
            return count;
        }

        public override decimal GetCount(Core.TimeArg time)
        {
            decimal count = 0;
            foreach (var e in Events.InBoundsUntil(time))
            {
                if (e is Events.Purchase) count += (e as Events.Purchase).Count;
                if (e is Events.Sale) count -= (e as Events.Sale).Count;
                if (e is Events.Redemption && (e as Events.Redemption).Maturity) count = 0;
            }
            return count;
        }

        public override double GetPurchasePrice(Core.TimeArg time, bool dirty)
        {
            if (!IsActive(time)) return 0;

            double value = 0;
            double count = 0;
            double price = 0;

            foreach (Events.Event e in Events.InBoundsUntil(time))
            {
                if (e is Events.Purchase purchase)
                {
                    price = purchase.Price;
                    if (!dirty) { price -= GetAccruedInterest(purchase.settleDate); }

                    value += price * (double)purchase.Count;
                    count += (double)purchase.Count;
                }
                if (e is Events.Redemption redemption)
                {
                    if (redemption.Maturity) count = 0;
                }
            }

            if (count == 0) return 0;

            return value / count;
        }

        public override decimal GetUnrealizedPurchaseFee(Core.TimeArg time, bool local, bool tax)
        {
            decimal count = 0;
            decimal fee = 0;
            decimal current = 0;

            foreach (Events.Event e in Events.InBoundsUntil(time))
            {
                if (e is Events.Purchase purchase)
                {
                    count = purchase.Count;
                    fee += purchase.Fee;
                }
                if (e is Events.Sale sale)
                {
                    current = Core.Round(sale.Count / count * fee);
                    count -= sale.Count;
                    fee -= current;
                }
            }
            return fee;
        }

        public override decimal GetRealizedGainsLossesFromValuation(Events.Event e, bool local, bool tax, bool yearly)
        {
            decimal result = 0;
            decimal factor = 0;

            if (!(e is Events.Sale))
            {
                Log.Report(Severity.Error, "GetRealizedGainsLossesFromValuation called for different event type than sale.");
                return 0;
            }

            Events.Sale sale = e as Events.Sale;
            factor = sale.Count / GetCount(Core.TimeArg.BeforeEvent(sale));

            if (yearly)
            {
                result += factor * GetUnrealizedGainsLossesFromValuation(Core.TimeArg.AfterDate(Calendar.ReachEndOfYear(sale.settleDate, -1)), Core.TimeArg.AfterEvent(sale), local, tax, yearly, false);
            }
            else
            {
                result += factor * GetUnrealizedGainsLossesFromValuation(Core.TimeArg.BeforeEvent(FirstEvent), Core.TimeArg.AfterEvent(sale), local, tax, yearly, false);
            }

            return result;
        }

        public override decimal GetRealizedGainsLossesFromFX(Events.Event e, bool local, bool tax, bool yearly)
        {
            if (!local) return 0;

            decimal result = 0;
            decimal factor = 0;

            if (!(e is Events.Sale) && !(e is Events.Redemption))
            {
                Log.Report(Severity.Error, "GetRealizedGainsLossesFromFX called for different event type than sale or redemption.");
                return 0;
            }

            if (e is Events.Sale sale)
            {
                factor = sale.Count / GetCount(Core.TimeArg.BeforeEvent(sale));

                if (yearly)
                {
                    result += factor * GetUnrealizedGainsLossesFromFX(Core.TimeArg.AfterDate(Calendar.ReachEndOfYear(sale.settleDate, -1)), Core.TimeArg.BeforeEvent(sale), local, tax, yearly);
                }
                else
                {
                    result += factor * GetUnrealizedGainsLossesFromFX(Core.TimeArg.BeforeEvent(FirstEvent), Core.TimeArg.AfterEvent(sale), local, tax, yearly);
                }
            }
            if (e is Events.Redemption redemption)
            {
                if (!redemption.Maturity) return 0;
                if (yearly)
                {
                    result += GetUnrealizedGainsLossesFromFX(Core.TimeArg.AfterDate(Calendar.ReachEndOfYear(redemption.settleDate, -1)), Core.TimeArg.BeforeEvent(redemption), local, tax, yearly);
                }
                else
                {
                    result += GetUnrealizedGainsLossesFromFX(null, Core.TimeArg.BeforeEvent(redemption), local, tax, yearly);
                }
            }

            return result;
        }
    }
}
