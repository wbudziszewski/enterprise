﻿using System.Collections.Generic;
using System.Linq;

namespace Budziszewski.Enterprise.Investments
{
    public static class EventBoundsExtensions
    {
        public static IEnumerable<T> InBounds<T>(this IEnumerable<T> events, Core.TimeArg start, Core.TimeArg end) where T : Events.Event
        {
            bool ret = false;
            if (start == null) ret = true;

            foreach (var evt in events)
            {
                if (start != null)
                {
                    if (start is Core.BeforeDateBoundary bdb) if (evt.evtDate >= bdb.Arg) ret = true;
                    if (start is Core.BeforeEventBoundary beb) if (evt >= beb.Arg) ret = true;
                    if (start is Core.AfterDateBoundary adb) if (evt.evtDate > adb.Arg) ret = true;
                    if (start is Core.AfterEventBoundary aeb) if (evt > aeb.Arg) ret = true;

                    if (start is Core.AtDateBoundary atdb) if (evt.evtDate > atdb.Arg) ret = true;
                }

                if (end != null)
                {
                    if (end is Core.BeforeDateBoundary bdb) if (evt.evtDate >= bdb.Arg) yield break;
                    if (end is Core.BeforeEventBoundary beb) if (evt >= beb.Arg) yield break;
                    if (end is Core.AfterDateBoundary adb) if (evt.evtDate > adb.Arg) yield break;
                    if (end is Core.AfterEventBoundary aeb) if (evt > aeb.Arg) yield break;

                    if (end is Core.AtDateBoundary atdb) if (evt.evtDate > atdb.Arg) yield break;
                }

                if (ret) yield return evt;
            }

        }

        public static IEnumerable<T> InBoundsSince<T>(this IEnumerable<T> events, Core.TimeArg start) where T : Events.Event
        {
            return InBounds<T>(events, start, null);
        }

        public static IEnumerable<T> InBoundsUntil<T>(this IEnumerable<T> events, Core.TimeArg end) where T : Events.Event
        {
            return InBounds<T>(events, null, end);
        }
    }
}
