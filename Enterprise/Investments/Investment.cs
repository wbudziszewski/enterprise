﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Budziszewski.Enterprise.Investments
{
    /// <summary>
    /// Describes valuation class.
    /// Trading class means that the investment is intended for constant trading and market valuation effects are recognized in profit and loss.
    /// Available for sale class means that the investment is intended for keeping until maturity but with possible sale beforehand; market valuation effects are recognized in other comprehensive income (capital)
    /// Held to maturity class means that the investment will always be held until maturity and no sale is permitted; no market valuation is attempted.
    /// </summary>
    public enum ValuationClass { Unspecified, Trading, AvailableForSale, HeldToMaturity }

    /// <summary>
    /// Represents the most general idea of an investment, including all securities, other assets (deposits, cash) and their equivalents (receivables) as well as liabilities.
    /// </summary>
    public abstract class Investment
    {
        protected List<Events.Event> Events;

        protected Events.Event FirstEvent
        {
            get
            {
                return Events?.FirstOrDefault();
            }
        }

        protected Events.Event LastEvent
        {
            get
            {
                return Events?.LastOrDefault();
            }
        }

        private long index;
        /// <summary>
        /// Unique identifier of the investment. No other investments of this guid will appear in the books, even if its characteristics (type, date, currency, amounts) are the same.
        /// </summary>
        public long Index
        {
            get
            {
                return index;
            }
        }

        protected string id;
        /// <summary>
        /// Identifier of the investment. Investments of the same type are identified by this field. It is:
        /// - ISIN number where available;
        /// - other unique short name where there is no ISIN.
        /// Non-standardized investments like deposits, cash, receivables, liabilities get no ID.
        /// </summary>
        public string ID { get; private set; }

        /// <summary>
        /// Name of the investment (presentable to the end user). It is:
        /// - short name in case of fixed income (e.g. WS0428, WS0447);
        /// - full name of a company in case of equity investments;
        /// - full name of an investment fund in case of fund units (certificates).
        /// In case of deposits the name includes creation and maturity date, currency, bank and 'deposit' caption.
        /// In case of receivables, liabilities the name includes creation and maturity date, entity, currency and respective caption.
        /// In case of cash the name includes currency, bank and 'cash' caption.
        /// </summary>
        public string Name { get; private set; }

        public string Ticker { get; protected set; }

        public string LongTicker { get; protected set; }

        /// <summary>
        /// Returns type of an investment derived from an object type.
        /// </summary>
        public string Type { get { return GetType().Name; } }

        /// <summary>
        /// Portfolio which the investment belongs to.
        /// </summary>
        public string Portfolio { get; private set; }

        /// <summary>
        /// In case of securities, custody account where the instrument is kept; otherwise null.
        /// </summary>
        public string CustodyAccount { get; private set; }

        /// <summary>
        /// Cash account which receives potential flows from the investment; by default cash account from which the purchase was made.
        /// In case of cash instruments (e.g. deposit, cash, receivable), bank account where the cash resides or where it would flow at the maturity.
        /// </summary>
        public string AssociatedCashAccount { get; private set; }

        /// <summary>
        /// Currency in which the investment is denominated.
        /// </summary>
        public string Currency { get; private set; }

        /// <summary>
        /// Valuation class, if applicable.
        /// </summary>
        public ValuationClass ValuationClass { get; private set; }

        /// <summary>
        /// Acronym of valuation class, if applicable.
        /// </summary>
        public string ValClass
        {
            get
            {
                switch (ValuationClass)
                {
                    case ValuationClass.Trading: return "TRD";
                    case ValuationClass.AvailableForSale: return "AFS";
                    case ValuationClass.HeldToMaturity: return "HTM";
                    default: return "";
                }
            }
        }

        /// <summary>
        /// Day count convention which applies to the investment.
        /// </summary>
        public Financial.DayCountConvention DayCountConvention { get; private set; }

        /// <summary>
        /// If true, asset is recognized or derecognized on trade date, otherwise on settlement date.
        /// </summary>
        public bool RecognitionOnTradeDate { get; set; }

        /// <summary>
        /// Creates a new instance of an investment.
        /// </summary>
        /// <param name="setup">Contains basic definition of this investment instance, applies to fields set only once for the whole life period of this instance.</param>
        public Investment(long index, InvestmentSetup setup)
        {
            this.index = index;
            Events = new List<Events.Event>();

            ID = String.IsNullOrEmpty(setup.ID) ? (setup.Name + ": " + index) : setup.ID;
            Name = setup.Name;
            Portfolio = setup.Portfolio;
            CustodyAccount = setup.CustodyAccount;
            AssociatedCashAccount = setup.AssociatedCashAccount;
            Currency = setup.Currency;
            ValuationClass = setup.ValuationClass;
            DayCountConvention = setup.DayCountConvention;
        }

        public virtual void RegisterAddition(Events.Inflow e)
        {
            AddEvent(e);
            GenerateEvents();
        }
        public virtual void RegisterAddition(Events.Purchase e)
        {
            AddEvent(e);
            GenerateEvents();
        }


        public virtual decimal RegisterDeduction(Events.Outflow e)
        {
            AddEvent(e);
            return e.Amount;
        }
        public virtual decimal RegisterDeduction(Events.Sale e)
        {
            AddEvent(e);
            return e.Amount;
        }

        /// <summary>
        /// Generates events resulting from cashflow occurences, i.e. redemptions, coupons, dividends, etc. as well as valuation (prices, FX rates).
        /// </summary>
        protected abstract void GenerateEvents();

        /// <summary>
        /// Adds new event to the events list.
        /// </summary>
        /// <param name="e">Event to be added</param>
        /// <param name="recalculateBoundaries">If true, time bounds in which investment is active will be checked and changed accordingly</param>
        public void AddEvent(Events.Event e)
        {
            int index = Events.FindIndex(x => x.evtDate > e.evtDate);
            if (index > 0)
            {
                Events.Insert(index, e);
            }
            else
            {
                Events.Add(e);
            }
        }

        /// <summary>
        /// Returns cash flow events of the investment, i.e. redemptions, coupons, dividends, etc.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Events.Flow> GetFlowEvents()
        {
            var events = Events.OfType<Events.Flow>();
            foreach (var e in events)
            {
                if (GetCount(Core.TimeArg.BeforeEvent(e)) != 0) yield return e;
            }
        }

        /// <summary>
        /// Returns purchase transaction date. In case of multiple purchases, it returns the date referring to the first transactions.
        /// </summary>
        /// <returns>Purchase transaction date</returns>
        public DateTime? GetPurchaseDate()
        {
            return RecognitionOnTradeDate ? (FirstEvent?.evtDate) : (FirstEvent?.settleDate);
        }

        /// <summary>
        /// Returns maturity date of the investment.
        /// </summary>
        /// <returns>Maturity date of the investment or null if there is no maturity date (e.g. in case of equity instruments)</returns>
        public virtual DateTime? GetMaturityDate()
        {
            DateTime? output = null;
            foreach (Events.Event e in Events)
            {
                if (e is Events.Redemption && (e as Events.Redemption).Maturity) output = e.settleDate;
            }
            return output;
        }

        /// <summary>
        /// Return date of the next coupon of the investment.
        /// </summary>
        /// <param name="date">The date starting from which the next coupon is counted.</param>
        /// <returns>Date of the next coupon or null if there is no next coupon date (e.g. in case of equity instruments)</returns>
        public DateTime? GetNextCouponDate(DateTime date)
        {
            DateTime? output = null;
            foreach (Events.Event e in Events)
            {
                if (e.settleDate <= date) continue;
                if (e is Events.Redemption) return e.settleDate;
            }
            return output;
        }

        /// <summary>
        /// Gets count (amount of units) of the investment at the specified time.
        /// </summary>
        /// <param name="time">Time at which count is given.</param>
        /// <returns>Count (amount of units) of the investment</returns>
        public abstract decimal GetCount(Core.TimeArg time);

        /// <summary>
        /// Gets coupon rate of the coupon that falls on the specified date or the next nearest coupon.
        /// </summary>
        /// <param name="date">The date at which coupon rate is given, if no coupon falls on the given date, then the next coupon is given</param>
        /// <returns>Coupon rate</returns>
        public abstract double GetCouponRate(DateTime date);

        #region Price

        /// <summary>
        /// Gets purchase price of the investment. In case of single purchase, it is the price of this transaction. In case of multiple purchases (average cost expense method) it is an average price of purchase transactions, taking into account possible disinvestments.
        /// </summary>
        /// <param name="time">Time at which price is given (it matters for average cost expense method where there could be multiple purchases)</param>
        /// <param name="dirty">If true, dirty price (including interest) will be given</param>
        /// <returns>Purchase price of the investment</returns>
        public abstract double GetPurchasePrice(Core.TimeArg time, bool dirty);

        /// <summary>
        /// Gets market price of the investment, i.e. price taken from an active market or equivalent. If no market price is available at the given date:
        /// - if there is any earlier market valuation for the investment, it is taken;
        /// - if there is no market price but there is sufficient cash flow information, market price is estimated using current market rates;
        /// - otherwise, amortized cost price is taken.
        /// In case of HTM instruments amortized cost price is always returned.
        /// </summary>
        /// <param name="time">Time at which price is given</param>
        /// <param name="dirty">If true, dirty price (including interest) will be given</param>
        /// <returns>Market price of the investment</returns>
        public abstract double GetMarketPrice(Core.TimeArg time, bool dirty);

        /// <summary>
        /// Gets amortized cost price of the investment, i.e. purchase price modified if applicable by change of valuation resulting from passing of time, calculated using internal rate of return (effective rate).
        /// </summary>
        /// <param name="time">Time at which price is given</param>
        /// <param name="dirty">If true, dirty price (including interest) will be given</param>
        /// <returns>Amortized cost price of the investment</returns>
        public abstract double GetAmortizedCostPrice(Core.TimeArg time, bool dirty);

        /// <summary>
        /// Gets amount of accrued interest of the investment (for fixed income, interest component of dirty price, per 100), if applicable.
        /// </summary>
        /// <param name="date">Date at which interest is given</param>
        /// <returns>Amount of accrued interest.</returns>
        public abstract double GetAccruedInterest(DateTime date);

        #endregion

        #region Asset amount

        /// <summary>
        /// Gets nominal amount of the investment. It is:
        /// - notional (face) amount of fixed income securities;
        /// - purchase amount in case of equity instruments;
        /// - initial amount in case of deposits;
        /// - actual amount in case of other assets and liabilities.
        /// </summary>
        /// <param name="time">Time at which amount is given</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <returns>Nominal amount of the investment</returns>
        public abstract decimal GetNominalAmount(Core.TimeArg time, bool local, bool tax);

        /// <summary>
        /// Gets amount of accrued interest of the investment.
        /// </summary>
        /// <param name="time">Time at which amount is given</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <returns>Interest amount of the investment</returns>
        public abstract decimal GetInterestAmount(Core.TimeArg time, bool local, bool tax);

        /// <summary>
        /// Gets purchase amount of the investment.
        /// </summary>
        /// <param name="time">Time at which amount is given</param>
        /// <param name="dirty">If true, dirty price (including interest) will be given</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <returns>Purchase amount of the investment</returns>
        public abstract decimal GetPurchaseAmount(Core.TimeArg time, bool dirty, bool local, bool tax);

        /// <summary>
        /// Gets market value of the investment.
        /// </summary>
        /// <param name="time">Time at which amount is given</param>
        /// <param name="dirty">If true, dirty price (including interest) will be given</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <returns>Market value of the investment</returns>
        public abstract decimal GetMarketValue(Core.TimeArg time, bool dirty, bool local, bool tax);

        /// <summary>
        /// Gets amortized cost value of the investment.
        /// </summary>
        /// <param name="time">Time at which amount is given</param>
        /// <param name="dirty">If true, dirty price (including interest) will be given</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <returns>Amortized cost value of the investment</returns>
        public abstract decimal GetAmortizedCostValue(Core.TimeArg time, bool dirty, bool local, bool tax);

        /// <summary>
        /// Gets generic value of the investment at the given time, in local currency. This gives indicative value, in most cases equal to book value of the asset, which is especially useful for weight-averaging and quick assessments. For investments classified as Trading or Available for sale, this is market value (dirty). For investments Held to maturity, this is amortized cost value (dirty).
        /// </summary>
        /// <param name="date">Date at which value is given</param>
        /// <returns>Value of the investment</returns>
        public decimal GetValue(DateTime date)
        {
            if (ValuationClass == ValuationClass.HeldToMaturity)
            {
                return GetAmortizedCostValue(Core.TimeArg.AtDate(date), true, true, false);
            }
            else
            {
                return GetMarketValue(Core.TimeArg.AtDate(date), true, true, false);
            }
        }

        #endregion

        #region Parameters

        /// <summary>
        /// Gets tenor, i.e. amount of years (taking into account day count convention) that remains until maturity.
        /// </summary>
        /// <param name="date">Date at which tenor is given</param>
        /// <returns>Returns tenor of the investment or 0 if there is no tenor (no specific maturity date).</returns>
        public abstract double GetTenor(DateTime date);

        /// <summary>
        /// Gets modified duration, i.e. sensitivity of value to change in market rates for fixed income instruments.
        /// </summary>
        /// <param name="date">Date at which modified duration is given</param>
        /// <returns>Returns tenor of the investment or 0 if there is no modified duration (instrument not applicable).</returns>
        public abstract double GetModifiedDuration(DateTime date);

        /// <summary>
        /// Gets yield to maturity of the investment, i.e. income rate per annum it would generate until maturity.
        /// </summary>
        /// <param name="date">Date at which yield to maturity is given</param>
        /// <param name="price">Current clean price of the investment at the given date, acquired e.g. by specific function to get market value or amortized cost value. </param>
        /// <returns>Returns yield to maturity of the investment or 0 if there is no modified duration (instrument not applicable).</returns>
        public abstract double GetYieldToMaturity(DateTime date, double price);

        #endregion

        /// <summary>
        /// Gets amount of purchase fees for the amount of investment that still has not been sold (redeemed) - for tax calculations.
        /// </summary>
        /// <param name="time">Time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed (usually needed in case of this measure)</param>
        /// <returns>Purchase fee accountable to unsold amount</returns>
        public abstract decimal GetUnrealizedPurchaseFee(Core.TimeArg time, bool local, bool tax);

        #region Income

        /// <summary>
        /// Gets income resulting from passing of time and resulting revaluation of cashflows (change of amortized cost value).
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <returns>Time value of money income</returns>
        public abstract decimal GetTimeValueOfMoneyIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax);

        /// <summary>
        /// Gets income resulting from incoming cashflows, e.g. redemptions, coupons, dividends.
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <returns>Cash flow income</returns>
        public abstract decimal GetCashflowIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax);

        /// <summary>
        /// Gets income resulting from sale of instrument and recognition of previous market valuation of the investment, i.e. difference between market valuation (market prices) and amortized cost valuation, as realized gain or loss.
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <param name="yearly">If true, unrealized gains/losses from the current year will be derecognized, otherwise from the point of initial recognition</param>
        /// <returns>Market valuation gain or loss</returns>
        public abstract decimal GetRealizedGainsLossesFromValuation(Events.Event e, bool local, bool tax, bool yearly);

        /// <summary>
        /// Gets income resulting from market valuation of the investment, i.e. difference between market valuation (market prices) and amortized cost valuation. Resulting difference may be booked, depending on accounting environment, in profit and loss statement or as other comprehensive income (capital).
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <param name="yearly">If true, unrealized gains/losses from the current year will be derecognized, otherwise from the point of initial recognition</param>
        /// <param name="net">If true, the amount of unrealized gains/losses subtracted due to disinvestments (and recognition as realized gains/losses) would be deducted, otherwise only gross increase would be shown.</param>
        /// <returns>Market valuation gain or loss</returns>
        public abstract decimal GetUnrealizedGainsLossesFromValuation(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly, bool net);

        /// <summary>
        /// Gets income resulting from sale of instrument and recognition of previous FX effects of the investment, as realized FX differences.
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <param name="yearly">If true, unrealized gains/losses from the current year will be derecognized, otherwise from the point of initial recognition</param>
        /// <returns>FX gain or loss</returns>
        public abstract decimal GetRealizedGainsLossesFromFX(Events.Event e, bool local, bool tax, bool yearly);

        /// <summary>
        /// Gets income resulting from FX effects of the investment.
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <param name="yearly">If true, unrealized gains/losses from the current year will be derecognized, otherwise from the point of initial recognition</param>
        /// <returns>FX gain or loss</returns>
        public abstract decimal GetUnrealizedGainsLossesFromFX(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly);

        #region Revaluation gains/losses (from impairment)

        /// <summary>
        /// Gets result from impairment (revaluation) effects.
        /// </summary>
        /// <param name="start">Starting time of the calculation (exclusive)</param>
        /// <param name="end">Ending time of the calculation (inclusive)</param>
        /// <param name="local">If true, amount will be calculated in the local currency</param>
        /// <param name="tax">If true, rules regarding tax calculation will be applied, if needed</param>
        /// <returns>Revaluation gain or loss</returns>
        public abstract decimal GetRevaluationGainsLosses(Core.TimeArg start, Core.TimeArg end, bool local, bool tax);

        #endregion

        #endregion

        /// <summary>
        /// Gets events of the specified type between given dates. Events will be included on settlement (cash flow, i.e. payment) basis.
        /// </summary>
        /// <typeparam name="T">Type of events</typeparam>
        /// <param name="start">Starting date (exclusive)</param>
        /// <param name="end">Ending date (inclusive)</param>
        /// <returns>Events that specify given criteria</returns>
        public IEnumerable<T> GetEvents<T>(DateTime start, DateTime end) where T : Events.Event
        {
            return Events.OfType<T>().Where(x => x.evtDate > start && x.evtDate <= end);
        }

        public IEnumerable<Events.Dividend> GetDividends(DateTime start, DateTime end, DividendDate date)
        {
            switch (date)
            {
                case DividendDate.RecordDate:
                    return Events.OfType<Events.Dividend>().Where(x => x.RecordDate > start && x.RecordDate <= end);
                case DividendDate.ExDate:
                    return Events.OfType<Events.Dividend>().Where(x => x.ExDate > start && x.ExDate <= end);
                case DividendDate.PaymentDate:
                    return Events.OfType<Events.Dividend>().Where(x => x.PaymentDate > start && x.PaymentDate <= end);
                default:
                    return Events.OfType<Events.Dividend>().Where(x => x.evtDate > start && x.evtDate <= end);
            }
        }

        public enum DividendDate { RecordDate, ExDate, PaymentDate }

        /// <summary>
        /// Returns whether the investment is active (is present in the books, has any value) at any point within the given period of time.
        /// </summary>
        public bool IsActive(DateTime start, DateTime end)
        {
            if (Events.Count == 0) return false;
            if (FirstEvent == null)
            {
                Log.Report(Severity.Error, "No first event found on investment.", this);
                return false;
            }

            if (LastEvent == null)
            {
                return end >= (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate);
            }
            else
            {
                return (start < (RecognitionOnTradeDate ? LastEvent.evtDate : LastEvent.settleDate)) && (end >= (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate));
            }
        }

        /// <summary>
        /// Returns whether the investment is active (is present in the books, has any value) at specific point of time.
        /// </summary>
        public bool IsActive(Core.TimeArg arg)
        {
            if (Events.Count == 0) return false;
            if (FirstEvent == null)
            {
                Log.Report(Severity.Error, "No first event found on investment.", this);
                return false;
            }

            if (LastEvent == null)
            {
                if (arg is Core.AtDateBoundary atdb) return atdb.Arg >= (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate);
                if (arg is Core.AfterDateBoundary adb) return adb.Arg >= (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate);
                if (arg is Core.AfterEventBoundary aeb) return aeb.Arg >= FirstEvent;
                if (arg is Core.BeforeDateBoundary bdb) return bdb.Arg > (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate);
                if (arg is Core.BeforeEventBoundary beb) return beb.Arg > FirstEvent;
                throw new Exception("Unknown type derived from TimeArg.");
            }
            else
            {
                if (arg is Core.AtDateBoundary atdb) return (atdb.Arg >= (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate)) && (atdb.Arg < (RecognitionOnTradeDate ? LastEvent.evtDate : LastEvent.settleDate));
                if (arg is Core.AfterDateBoundary adb) return (adb.Arg >= (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate)) && (adb.Arg < (RecognitionOnTradeDate ? LastEvent.evtDate : LastEvent.settleDate));
                if (arg is Core.AfterEventBoundary aeb) return aeb.Arg >= FirstEvent && aeb.Arg < LastEvent;
                if (arg is Core.BeforeDateBoundary bdb) return (bdb.Arg > (RecognitionOnTradeDate ? FirstEvent.evtDate : FirstEvent.settleDate)) && (bdb.Arg <= (RecognitionOnTradeDate ? LastEvent.evtDate : LastEvent.settleDate));
                if (arg is Core.BeforeEventBoundary beb) return beb.Arg > FirstEvent && beb.Arg <= LastEvent;
                throw new Exception("Unknown type derived from TimeArg.");
            }
        }

        public override string ToString()
        {
            return String.Format("{0}: {1} ({2}, {3}) @{4} #{5}", GetType().Name, Name, Currency, Portfolio, String.IsNullOrEmpty(CustodyAccount) ? AssociatedCashAccount : CustodyAccount, GetPurchaseDate().Value.ToString("yyyy-MM-dd"));
        }
    }
}
