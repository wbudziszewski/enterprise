﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Budziszewski.Enterprise.Events;

namespace Budziszewski.Enterprise.Investments
{
    public struct InvestmentSetup
    {
        public string ID;
        public string Name;
        public string Portfolio;
        public string CustodyAccount;
        public string AssociatedCashAccount;
        public string Currency;
        public ValuationClass ValuationClass;
        public Financial.DayCountConvention DayCountConvention;

        public InvestmentSetup(Definitions.Instrument def, Purchase e)
        {
            ID = def.ID;
            Name = def.Name;
            Portfolio = e.Portfolio;
            CustodyAccount = e.CustodyAccount;
            AssociatedCashAccount = e.CashAccount;
            Currency = e.Currency;
            ValuationClass = e.ValuationClass;
            
            switch (def?.DayCountConvention?.ToLower())
            {
                case "act/360": DayCountConvention = Financial.DayCountConvention.Actual_360; break;
                case "act/365": DayCountConvention = Financial.DayCountConvention.Actual_365; break;
                case "act/act": DayCountConvention = Financial.DayCountConvention.Actual_Actual_Excel; break;
                case "eur/360/30": DayCountConvention = Financial.DayCountConvention.European_30_360; break;
                case "us/360/30": DayCountConvention = Financial.DayCountConvention.US_30_360; break;
                default:
                    DayCountConvention = Financial.DayCountConvention.Actual_Actual_Excel;
                    if (def.InstrumentType == "Bond")
                    {
                        Log.Report(Severity.Warning, "Unspecified day count convention for a fixed income instrument.", def);
                    }
                    break;
            }
        }

        public InvestmentSetup(Inflow e)
        {
            ID = "";
            Name = "Cash (" + e.Currency + ")";
            Portfolio = e.Portfolio;
            CustodyAccount = null;
            AssociatedCashAccount = e.CashAccount;
            Currency = e.Currency;
            ValuationClass = ValuationClass.Unspecified;
            DayCountConvention = Financial.DayCountConvention.Actual_Actual_Excel; // TODO
        }
    }
}
