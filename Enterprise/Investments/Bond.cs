﻿using System;
using System.Linq;

namespace Budziszewski.Enterprise.Investments
{
    /// <summary>
    /// Represents vanilla fixed-income instrument (fixed rate or floating rate) with payment of notional at maturity and possible periodical payment of coupons. All instruments are deemed tradeable and identifiable by ISIN.
    /// </summary>
    public class Bond : StandardizedSecurity
    {
        public decimal UnitPrice { get; private set; }

        public double CouponRate { get; private set; }

        public int CouponFreq { get; private set; }

        public string CouponType { get; private set; }
        
        public Bond(long index, Definitions.Instrument def, Events.Purchase e) : base(index, new InvestmentSetup(def, e))
        {
            instrument = def;
            Ticker = def.Ticker;
            LongTicker = def.LongTicker;
            UnitPrice = def.UnitPrice;
            CouponRate = def.CouponRate;
            CouponFreq = def.CouponFreq;
            CouponType = def.CouponType;
            RecognitionOnTradeDate = false;

            RegisterAddition(e);
        }

        protected override void GenerateEvents()
        {
            DateTime? start = FirstEvent?.settleDate;
            DateTime? end = instrument?.Maturity;

            if (start == null || end == null)
            {
                Log.Report(Severity.Warning, String.Format("Error generating cash flow events for an investment: {0}.", this));
                return;
            }

            int monthStep;
            DateTime date;

            if (CouponType == "fixed")
            {
                monthStep = (int)(12.0 / CouponFreq);

                date = end.Value;
                if (date >= start)
                {
                    AddEvent(new Events.Redemption(date, 1, instrument.Currency, Database.GetFXRate(instrument.Currency, date), true));
                }
                if (CouponRate > 0)
                {
                    while (date >= start)
                    {
                        AddEvent(new Events.Redemption(date, CouponRate / CouponFreq, instrument.Currency, Database.GetFXRate(instrument.Currency, date), false));
                        date = date.AddMonths(-monthStep);
                    }
                }
            }
            else
            {
                var coupons = Database.Coupons.Where(x => x.InvestmentID == ID);
                monthStep = (int)(12.0 / CouponFreq);

                date = end.Value;
                if (date >= start)
                {
                    AddEvent(new Events.Redemption(date, 1, instrument.Currency, Database.GetFXRate(instrument.Currency, date), true));
                }
                while (date >= start)
                {
                    AddEvent(new Events.Redemption(date, (coupons.SingleOrDefault(x => x.CouponDate == date)?.CouponRate / CouponFreq) ?? 0, instrument.Currency, Database.GetFXRate(instrument.Currency, date), false));
                    date = date.AddMonths(-monthStep);
                }
            }
        }

        public override double GetCouponRate(DateTime date)
        {
            if (!IsActive(Core.TimeArg.AtDate(date))) return 0;

            double output = 0;
            foreach (Events.Event e in Events)
            {
                if (e.settleDate < date) continue;
                if (e is Events.Redemption && !(e as Events.Redemption).Maturity)
                {
                    output = (e as Events.Redemption).Rate * CouponFreq; // recreate coupon rate from actual payment
                    return output;
                }
            }
            return output;
        }

        public override double GetMarketPrice(Core.TimeArg time, bool dirty)
        {
            if (!IsActive(time)) return 0;

            double output = 0;
            Definitions.Price price = Database.GetPrice(this, time.Date);
            if (price == null)
            {
                Log.Report(Severity.Warning, "No price for: " + Name + " (" + ID + ") at date: " + time.Date.ToString("yyyy-MM-dd") + ".");

                Financial.Curve curve = Database.GetCurve(time.Date, Currency);
                if (curve == null)
                {
                    output = GetAmortizedCostPrice(time, dirty);
                }
                else
                {
                    output = Financial.FixedIncome.DirtyPrice(time.Date, GetMaturityDate().Value, CouponRate, curve, 100, CouponFreq, DayCountConvention);
                }
            }
            else
            {
                output = price.Value;
            }

            if (dirty) { output += GetAccruedInterest(time.Date); }
            return output;
        }

        public override double GetAmortizedCostPrice(Core.TimeArg time, bool dirty)
        {
            if (!IsActive(time)) return 0;

            double value = 0;
            double count = 0;
            double yield = 0;
            double price = 0;

            foreach (Events.Event e in Events.InBoundsUntil(time))
            {
                if (e is Events.Purchase purchase)
                {
                    price = purchase.Price - GetAccruedInterest(purchase.settleDate);
                    yield = GetYieldToMaturity(purchase.settleDate, price);

                    value += yield * (double)purchase.Count;
                    count += (double)purchase.Count;
                }
                if (e is Events.Impairment impairment)
                {
                    // Impairment replaces earlier calculations and gives new YTM
                    switch (impairment.ImpairmentType)
                    {
                        case Enterprise.Events.ImpairmentType.ToMarketValue:
                            yield = GetYieldToMaturity(impairment.evtDate, GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), false));
                            break;
                        case Enterprise.Events.ImpairmentType.ToSpecifiedPrice:
                            yield = GetYieldToMaturity(impairment.evtDate, (double)impairment.Argument);
                            break;
                        case Enterprise.Events.ImpairmentType.MarketValuePercentage:
                            yield = GetYieldToMaturity(impairment.evtDate, GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), false) * (double)impairment.Argument);
                            break;
                        case Enterprise.Events.ImpairmentType.Amount:
                            price = (double)((GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, true, false) - impairment.Argument) / GetNominalAmount(Core.TimeArg.BeforeEvent(impairment), true, false) * 100m);
                            yield = GetYieldToMaturity(impairment.evtDate, price);
                            break;
                        default: throw new Exception("Unknown impairment type.");
                    }

                    value = yield * (double)GetCount(Core.TimeArg.BeforeEvent(impairment));
                    count = (double)GetCount(Core.TimeArg.BeforeEvent(impairment));
                }
            }

            if (count == 0) return 0;

            if (dirty)
            {
                return Budziszewski.Financial.FixedIncome.DirtyPrice(time.Date, GetMaturityDate().Value, GetCouponRate(time.Date), value / count, 100, CouponFreq, DayCountConvention);
            }
            else
            {
                return Budziszewski.Financial.FixedIncome.Price(time.Date, GetMaturityDate().Value, GetCouponRate(time.Date), value / count, 100, CouponFreq, DayCountConvention);
            }
        }

        public override double GetAccruedInterest(DateTime date)
        {
            if (!IsActive(Core.TimeArg.AtDate(date))) return 0;
            return Budziszewski.Financial.FixedIncome.Interest(date, GetMaturityDate().Value, GetCouponRate(date), CouponFreq, DayCountConvention);
        }

        public override decimal GetNominalAmount(Core.TimeArg time, bool local, bool tax)
        {
            return Core.Round(GetCount(time) * UnitPrice * (local ? (decimal)Core.GetFXRate(Currency, time) : 1));
        }

        public override decimal GetInterestAmount(Core.TimeArg time, bool local, bool tax)
        {
            return Core.Round((decimal)GetAccruedInterest(time.Date) / 100m * GetNominalAmount(time, local, tax));
        }

        public override decimal GetPurchaseAmount(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetPurchasePrice(time, dirty) / 100m * GetNominalAmount(time, local, tax));
        }

        public override decimal GetMarketValue(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetMarketPrice(time, dirty) / 100m * GetNominalAmount(time, local, tax));
        }

        public override decimal GetAmortizedCostValue(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetAmortizedCostPrice(time, dirty) / 100m * GetNominalAmount(time, local, tax));
        }

        public override double GetTenor(DateTime date)
        {
            if (!IsActive(Core.TimeArg.AtDate(date))) return 0;
            return Financial.DayCount.GetTenor(date, GetMaturityDate().Value, CouponFreq, DayCountConvention);
        }

        public override double GetModifiedDuration(DateTime date)
        {
            if (!IsActive(Core.TimeArg.AtDate(date))) return 0;
            return Financial.FixedIncome.MDuration(date, GetMaturityDate().Value, GetCouponRate(date), GetYieldToMaturity(date, GetMarketPrice(Core.TimeArg.AtDate(date), false)), 100, CouponFreq, DayCountConvention);
        }

        public override double GetYieldToMaturity(DateTime date, double price)
        {
            if (!IsActive(Core.TimeArg.AtDate(date))) return 0;
            return Financial.FixedIncome.Yield(date, GetMaturityDate().Value, GetCouponRate(date), price, 100, CouponFreq, DayCountConvention);
        }

        public override decimal GetTimeValueOfMoneyIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            decimal income = 0;
            decimal fx = 1;
            (Core.TimeArg time, decimal count) previous = (start, GetCount(start));
            (Core.TimeArg time, decimal count) current = previous;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Purchase purchase)
                {
                    if (local) { fx = (decimal)purchase.FXRate; }
                    current = (Core.TimeArg.AfterEvent(purchase), previous.count + purchase.Count);

                    income +=
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(previous.time, true) / 100m * UnitPrice * fx);
                    previous = current;
                }
                if (e is Events.Redemption redemption)
                {
                    if (local) { fx = (decimal)redemption.FXRate; }
                    current = (Core.TimeArg.AfterEvent(redemption), previous.count);

                    if (redemption.Maturity)
                    {
                        income +=
                            Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx) -
                            Core.Round(previous.count * (decimal)GetAmortizedCostPrice(previous.time, true) / 100m * UnitPrice * fx);

                        return income;
                    }
                }
                if (e is Events.Sale sale)
                {
                    if (local) { fx = (decimal)sale.FXRate; }
                    current = (Core.TimeArg.AfterEvent(sale), previous.count - sale.Count);

                    income +=
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(previous.time, true) / 100m * UnitPrice * fx);
                    previous = current;
                }
            }

            // End of period
            if (local) { fx = (decimal)Core.GetFXRate(Currency, end); }
            current = (end, previous.count);

            income +=
                Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx) -
                Core.Round(previous.count * (decimal)GetAmortizedCostPrice(previous.time, true) / 100m * UnitPrice * fx);

            return income;
        }

        public override decimal GetCashflowIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            decimal income = 0;
            decimal fx = 1;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Redemption redemption)
                {
                    if (redemption.Maturity)
                    {
                        return income;
                    }
                    else
                    {
                        if (local) { fx = (decimal)redemption.FXRate; }

                        income += Core.Round((decimal)redemption.Rate * UnitPrice * GetCount(Core.TimeArg.BeforeEvent(redemption)) * fx);
                    }
                }
            }

            return income;
        }

        public override decimal GetUnrealizedGainsLossesFromValuation(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly, bool net)
        {
            decimal result = 0;
            decimal fx = 1;
            decimal previousReserve = 0;
            decimal currentReserve = 0;
            (Core.TimeArg time, decimal count) previous = (start, GetCount(start));
            (Core.TimeArg time, decimal count) current = previous;

            if (local) { fx = (decimal)Core.GetFXRate(Currency, start); }
            previous = (start, GetCount(start));
            previousReserve =
                Core.Round(previous.count * (decimal)GetMarketPrice(previous.time, true) / 100m * UnitPrice * fx) -
                Core.Round(previous.count * (decimal)GetAmortizedCostPrice(previous.time, true) / 100m * UnitPrice * fx);

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Purchase purchase)
                {
                    if (local) { fx = (decimal)purchase.FXRate; }
                    current = (Core.TimeArg.AfterEvent(purchase), previous.count + purchase.Count);
                    currentReserve =
                        Core.Round(previous.count * (decimal)purchase.Price / 100m * UnitPrice * fx) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx);

                    result += currentReserve - previousReserve;

                    previousReserve = currentReserve;
                    previous = current;
                }
                if (e is Events.Redemption redemption)
                {
                    if (redemption.Maturity)
                    {
                        currentReserve = 0;

                        result += currentReserve - previousReserve;

                        if (net) return 0;

                        return result;
                    }
                }
                if (e is Events.Sale sale)
                {
                    if (local) { fx = (decimal)sale.FXRate; }
                    currentReserve =
                        Core.Round(previous.count * (decimal)sale.Price / 100m * UnitPrice * fx) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(Core.TimeArg.BeforeEvent(sale), true) / 100m * UnitPrice * fx);

                    result += currentReserve - previousReserve;

                    current = (Core.TimeArg.AfterEvent(sale), previous.count - sale.Count);
                    currentReserve =
                        Core.Round(previous.count * (decimal)sale.Price / 100m * UnitPrice * fx) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx);

                    previousReserve = currentReserve;
                    previous = current;

                    if (net) result -= GetRealizedGainsLossesFromValuation(e, local, tax, yearly);
                }
            }

            // End of period
            if (local) { fx = (decimal)Core.GetFXRate(Currency, end); }
            current = (end, previous.count);
            currentReserve =
                Core.Round(previous.count * (decimal)GetMarketPrice(current.time, true) / 100m * UnitPrice * fx) -
                Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) / 100m * UnitPrice * fx);

            result += currentReserve - previousReserve;

            result -= GetRevaluationGainsLosses(start, end, local, tax);

            return result;
        }

        public override decimal GetUnrealizedGainsLossesFromFX(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly)
        {
            if (!local) return 0;

            decimal result = 0;
            decimal price = (decimal)GetAmortizedCostPrice(start, true);
            decimal previousFx = (decimal)Core.GetFXRate(Currency, start);
            decimal currentFx = previousFx;
            (Core.TimeArg time, decimal count) previous = (start, GetCount(start));

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Purchase purchase)
                {
                    currentFx = (decimal)purchase.FXRate;

                    result +=
                        Core.Round(previous.count * price / 100m * UnitPrice * currentFx) -
                        Core.Round(previous.count * price / 100m * UnitPrice * previousFx);
                    previous = (Core.TimeArg.AfterEvent(purchase), previous.count + purchase.Count);
                    previousFx = currentFx;
                }
                if (e is Events.Redemption redemption)
                {
                    if (!redemption.Maturity) continue;

                    currentFx = (decimal)redemption.FXRate;

                    result +=
                        Core.Round(previous.count * price / 100m * UnitPrice * currentFx) -
                        Core.Round(previous.count * price / 100m * UnitPrice * previousFx);
                    
                    return result;
                }
                if (e is Events.Sale sale)
                {
                    currentFx = (decimal)sale.FXRate;

                    result +=
                        Core.Round(previous.count * price / 100m * UnitPrice * currentFx) -
                        Core.Round(previous.count * price / 100m * UnitPrice * previousFx);
                    previous = (Core.TimeArg.AfterEvent(sale), previous.count - sale.Count);
                    previousFx = currentFx;
                }
            }

            // End of period
            currentFx = (decimal)Core.GetFXRate(Currency, end);

            result +=
                Core.Round(previous.count * price / 100m * UnitPrice * currentFx) -
                Core.Round(previous.count * price / 100m * UnitPrice * previousFx);

            return result;
        }

        public override decimal GetRevaluationGainsLosses(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            decimal result = 0;
            decimal amount = 0;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Impairment impairment)
                {
                    switch (impairment.ImpairmentType)
                    {
                        case Enterprise.Events.ImpairmentType.ToMarketValue:
                            amount = GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax) - GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax);
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.ToSpecifiedPrice:
                            amount = Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(impairment), local, tax) * ((decimal)GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), true) - impairment.Argument) / 100m);
                            amount = Math.Min(amount, Math.Max(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax), GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax)));
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.MarketValuePercentage:
                            amount = Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(impairment), local, tax) * (decimal)GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), true) * impairment.Argument / 100m);
                            amount = Math.Min(amount, Math.Max(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax), GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax)));
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.Amount:
                            result -= Math.Min(impairment.Argument, Math.Max(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax), GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax)));
                            break;
                        default: throw new Exception("Unknown impairment type.");
                    }
                }
            }

            return result;
        }
    }
}
