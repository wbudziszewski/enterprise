﻿using System;

namespace Budziszewski.Enterprise.Investments
{
    public class Cash : Investment
    {
        public Cash(long index, Events.Inflow e) : base(index, new InvestmentSetup(e))
        {
            RecognitionOnTradeDate = false;

            RegisterAddition(e);
        }

        protected override void GenerateEvents()
        { }

        public override decimal GetCount(Core.TimeArg time)
        {
            decimal amount = 0;
            foreach (var e in Events.InBoundsUntil(time))
            {
                if (e is Events.Inflow) amount += (e as Events.Inflow).Amount;
                if (e is Events.Outflow) amount -= (e as Events.Outflow).Amount;
            }
            return amount > 0 ? 1 : 0;
        }

        public override double GetCouponRate(DateTime date)
        {
            return 0;
        }

        public override double GetPurchasePrice(Core.TimeArg time, bool dirty)
        {
            return IsActive(time) ? 1 : 0;
        }

        public override double GetMarketPrice(Core.TimeArg time, bool dirty)
        {
            return IsActive(time) ? 1 : 0;
        }

        public override double GetAmortizedCostPrice(Core.TimeArg time, bool dirty)
        {
            return IsActive(time) ? 1 : 0;
        }

        public override double GetAccruedInterest(DateTime date)
        {
            return 0;
        }

        public override decimal GetNominalAmount(Core.TimeArg time, bool local, bool tax)
        {
            decimal output = 0;
            foreach (Events.Event e in Events.InBoundsUntil(time))
            {
                if (e is Events.Inflow inflow) output += inflow.Amount;
                if (e is Events.Outflow outflow) output -= outflow.Amount;
            }
            return Core.Round(output * (local ? (decimal)Core.GetFXRate(Currency, time) : 1));
        }

        public override decimal GetInterestAmount(Core.TimeArg time, bool local, bool tax)
        {
            return 0;
        }

        public override decimal GetPurchaseAmount(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return GetNominalAmount(time, local, tax);
        }

        public override decimal GetMarketValue(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetMarketPrice(time, dirty) * GetNominalAmount(time, local, tax));
        }

        public override decimal GetAmortizedCostValue(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetAmortizedCostPrice(time, dirty) * GetNominalAmount(time, local, tax));
        }

        public override double GetTenor(DateTime date)
        {
            return 0;
        }

        public override double GetModifiedDuration(DateTime date)
        {
            return 0;
        }

        public override double GetYieldToMaturity(DateTime date, double price)
        {
            return 0;
        }

        public override decimal GetTimeValueOfMoneyIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            return 0;
        }

        public override decimal GetCashflowIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            return 0;
        }

        public override decimal GetRealizedGainsLossesFromValuation(Events.Event e, bool local, bool tax, bool yearly)
        {
            return 0;
        }

        public override decimal GetUnrealizedGainsLossesFromValuation(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly, bool net)
        {
            return 0;
        }

        public override decimal GetRealizedGainsLossesFromFX(Events.Event e, bool local, bool tax, bool yearly)
        {
            if (!local) return 0;

            decimal result = 0;
            decimal factor = 0;

            if (!(e is Events.Outflow))
            {
                Log.Report(Severity.Error, "GetRealizedGainsLossesFromFX called for different event type than outflow.");
                return 0;
            }

            if (e is Events.Outflow outflow)
            {
                factor = outflow.Amount / GetNominalAmount(Core.TimeArg.BeforeEvent(outflow), local, tax);

                if (yearly)
                {
                    result += GetUnrealizedGainsLossesFromFX(Core.TimeArg.AfterDate(Calendar.ReachEndOfYear(outflow.settleDate, -1)), Core.TimeArg.BeforeEvent(outflow), local, tax, yearly);
                }
                else
                {
                    result += GetUnrealizedGainsLossesFromFX(null, Core.TimeArg.BeforeEvent(outflow), local, tax, yearly);
                }
            }

            return result;
        }

        public override decimal GetUnrealizedGainsLossesFromFX(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly)
        {
            if (!local) return 0;

            decimal result = 0;
            decimal previousFx = (decimal)Core.GetFXRate(Currency, start);
            decimal currentFx = previousFx;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Inflow inflow)
                {
                    currentFx = (decimal)inflow.FXRate;

                    result +=
                        Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(e), false, tax) * currentFx) -
                        Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(e), false, tax) * previousFx);

                    previousFx = currentFx;
                }
                if (e is Events.Outflow outflow)
                {
                    currentFx = (decimal)outflow.FXRate;

                    result +=
                        Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(outflow), false, tax) * currentFx) -
                        Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(outflow), false, tax) * previousFx);

                    previousFx = currentFx;
                }
            }

            // End of period
            currentFx = (decimal)Core.GetFXRate(Currency, end);

            result +=
                Core.Round(GetNominalAmount(end, false, tax) * currentFx) -
                Core.Round(GetNominalAmount(end, false, tax) * previousFx);

            return result;
        }

        public override decimal GetRevaluationGainsLosses(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            decimal result = 0;
            decimal amount = 0;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Impairment impairment)
                {
                    switch (impairment.ImpairmentType)
                    {
                        case Enterprise.Events.ImpairmentType.ToMarketValue:
                            Log.Report(Severity.Error, "Cannot apply 'ToMarketValue' impairment type to deposits.", this);
                            break;
                        case Enterprise.Events.ImpairmentType.ToSpecifiedPrice:
                            Log.Report(Severity.Error, "Cannot apply 'ToSpecifiedPrice' impairment type to deposits.", this);
                            break;
                        case Enterprise.Events.ImpairmentType.MarketValuePercentage:
                            amount = Core.Round(GetNominalAmount(Core.TimeArg.BeforeEvent(impairment), local, tax) * impairment.Argument);
                            amount = Math.Min(amount, GetNominalAmount(Core.TimeArg.BeforeEvent(impairment), local, tax));
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.Amount:
                            result -= Math.Min(impairment.Argument, GetNominalAmount(Core.TimeArg.BeforeEvent(impairment), local, tax));
                            break;
                        default: throw new Exception("Unknown impairment type.");
                    }
                }
            }

            return result;
        }

        public override decimal GetUnrealizedPurchaseFee(Core.TimeArg time, bool local, bool tax)
        {
            return 0;
        }
    }
}
