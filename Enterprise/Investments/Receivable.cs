﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Investments
{
    public class Receivable : Accounts
    {
        public Receivable(long index, Events.Inflow e) : base(index, e)
        {
            RegisterAddition(e);
        }        
    }
}
