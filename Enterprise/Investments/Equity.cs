﻿using System;

namespace Budziszewski.Enterprise.Investments
{
    /// <summary>
    /// Represents vanilla equity instrument with possible payment of dividends. All instruments are deemed tradeable and identifiable by ISIN.
    /// </summary>
    public class Equity : StandardizedSecurity
    {
        public Equity(long index, Definitions.Instrument def, Events.Purchase e) : base(index, new InvestmentSetup(def, e))
        {
            instrument = def;
            Ticker = def.Ticker;
            LongTicker = def.LongTicker;
            RecognitionOnTradeDate = true;

            RegisterAddition(e);
        }

        protected override void GenerateEvents()
        {
            foreach (var d in Database.Dividends)
            {
                if (ID == d.InvestmentID && d.RecordDate >= (FirstEvent?.settleDate ?? DateTime.MinValue))
                {
                    AddEvent(new Events.Dividend(d, this));
                }
            }
        }

        public override double GetCouponRate(DateTime date)
        {
            return 0;
        }

        public override double GetMarketPrice(Core.TimeArg time, bool dirty)
        {
            if (!IsActive(time)) return 0;

            double output = 0;
            Definitions.Price price = Database.GetPrice(this, time.Date);
            if (price == null)
            {
                Log.Report(Severity.Warning, "No price for: " + Name + " (" + ID + ") at date: " + time.Date.ToString("yyyy-MM-dd") + ".");
                output = GetAmortizedCostPrice(time, dirty);
            }
            else
            {
                output = price.Value;
            }
            return output;
        }

        public override double GetAmortizedCostPrice(Core.TimeArg time, bool dirty)
        {
            if (!IsActive(time)) return 0;

            double value = 0;
            double count = 0;
            double price = 0;

            foreach (Events.Event e in Events.InBoundsUntil(time))
            {
                if (e is Events.Purchase purchase)
                {
                    value += purchase.Price * (double)purchase.Count;
                    count += (double)purchase.Count;
                }
                if (e is Events.Impairment impairment)
                {
                    // Impairment replaces earlier calculations and gives new price
                    switch (impairment.ImpairmentType)
                    {
                        case Enterprise.Events.ImpairmentType.ToMarketValue:
                            price = GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), false);
                            break;
                        case Enterprise.Events.ImpairmentType.ToSpecifiedPrice:
                            price = (double)impairment.Argument;
                            break;
                        case Enterprise.Events.ImpairmentType.MarketValuePercentage:
                            price = (double)Core.Round(GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), false) * (double)impairment.Argument);
                            break;
                        case Enterprise.Events.ImpairmentType.Amount:
                            price = (double)Core.Round(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, true, false) - impairment.Argument);
                            break;
                        default: throw new Exception("Unknown impairment type.");
                    }

                    value = price * (double)GetCount(Core.TimeArg.BeforeEvent(impairment));
                    count = (double)GetCount(Core.TimeArg.BeforeEvent(impairment));
                }
            }

            if (count == 0) return 0;

            return value / count;
        }

        public override double GetAccruedInterest(DateTime date)
        {
            return 0;
        }

        public override decimal GetNominalAmount(Core.TimeArg time, bool local, bool tax)
        {
            return Core.Round(GetPurchaseAmount(time, false, local, tax) * (local ? (decimal)Core.GetFXRate(Currency, time) : 1));
        }

        public override decimal GetInterestAmount(Core.TimeArg time, bool local, bool tax)
        {
            return 0;
        }

        public override decimal GetPurchaseAmount(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetPurchasePrice(time, dirty) * GetCount(time) * (local ? (decimal)Core.GetFXRate(Currency, time) : 1));
        }

        public override decimal GetMarketValue(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetMarketPrice(time, dirty) * GetCount(time) * (local ? (decimal)Core.GetFXRate(Currency, time) : 1));
        }

        public override decimal GetAmortizedCostValue(Core.TimeArg time, bool dirty, bool local, bool tax)
        {
            return Core.Round((decimal)GetAmortizedCostPrice(time, dirty) * GetCount(time) * (local ? (decimal)Core.GetFXRate(Currency, time) : 1));
        }

        public override double GetTenor(DateTime date)
        {
            return 0;
        }

        public override double GetModifiedDuration(DateTime date)
        {
            return 0;
        }

        public override double GetYieldToMaturity(DateTime date, double price)
        {
            return 0;
        }

        public override decimal GetTimeValueOfMoneyIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            return 0;
        }

        public override decimal GetCashflowIncome(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            decimal income = 0;
            decimal fx = 1;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Dividend dividend)
                {
                    if (local) { fx = (decimal)dividend.FXRate; }

                    income += Core.Round((decimal)dividend.Rate * GetCount(Core.TimeArg.BeforeEvent(dividend)) * fx);
                }
            }

            return income;
        }

        public override decimal GetUnrealizedGainsLossesFromValuation(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly, bool net)
        {
            decimal result = 0;
            decimal fx = 1;
            decimal previousReserve = 0;
            decimal currentReserve = 0;
            (Core.TimeArg time, decimal count) previous = (start, GetCount(start));
            (Core.TimeArg time, decimal count) current = previous;

            if (local) { fx = (decimal)Core.GetFXRate(Currency, start); }
            previous = (start, GetCount(start));
            previousReserve =
                Core.Round(previous.count * (decimal)GetMarketPrice(previous.time, true) * fx) -
                Core.Round(previous.count * (decimal)GetAmortizedCostPrice(previous.time, true) * fx);

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Purchase purchase)
                {
                    if (local) { fx = (decimal)purchase.FXRate; }
                    current = (Core.TimeArg.AfterEvent(purchase), previous.count + purchase.Count);
                    currentReserve =
                        Core.Round(previous.count * (decimal)purchase.Price * fx, 2) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) * fx);

                    result += currentReserve - previousReserve;

                    previousReserve = currentReserve;
                    previous = current;
                }
                if (e is Events.Sale sale)
                {
                    if (local) { fx = (decimal)sale.FXRate; }
                    currentReserve =
                        Core.Round(previous.count * (decimal)sale.Price * fx) -
                        Core.Round(previous.count * (decimal)GetAmortizedCostPrice(Core.TimeArg.BeforeEvent(sale), true) * fx);

                    result += currentReserve - previousReserve;

                    current = (Core.TimeArg.AfterEvent(sale), previous.count - sale.Count);
                    currentReserve =
                        Core.Round(current.count * (decimal)sale.Price * fx) -
                        Core.Round(current.count * (decimal)GetAmortizedCostPrice(current.time, true) * fx);

                    previousReserve = currentReserve;
                    previous = current;

                    if (net) result -= GetRealizedGainsLossesFromValuation(e, local, tax, yearly);
                }
            }

            // End of period
            if (local) { fx = (decimal)Core.GetFXRate(Currency, end); }
            current = (end, previous.count);
            currentReserve =
                Core.Round(previous.count * (decimal)GetMarketPrice(current.time, true) * fx) -
                Core.Round(previous.count * (decimal)GetAmortizedCostPrice(current.time, true) * fx);

            result += currentReserve - previousReserve;

            result -= GetRevaluationGainsLosses(start, end, local, tax);

            return result;
        }

        public override decimal GetUnrealizedGainsLossesFromFX(Core.TimeArg start, Core.TimeArg end, bool local, bool tax, bool yearly)
        {
            if (!local) return 0;

            decimal result = 0;
            decimal price = (decimal)GetAmortizedCostPrice(start, true);
            decimal previousFx = (decimal)Core.GetFXRate(Currency, start);
            decimal currentFx = previousFx;
            (Core.TimeArg time, decimal count) previous = (start, GetCount(start));

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Purchase purchase)
                {
                    currentFx = (decimal)purchase.FXRate;

                    result +=
                        Core.Round(previous.count * price * currentFx) -
                        Core.Round(previous.count * price * previousFx);
                    previous = (Core.TimeArg.AfterEvent(purchase), previous.count + purchase.Count);
                    previousFx = currentFx;
                }
                if (e is Events.Sale sale)
                {
                    currentFx = (decimal)sale.FXRate;

                    result +=
                        Core.Round(previous.count * price * currentFx) -
                        Core.Round(previous.count * price * previousFx);
                    previous = (Core.TimeArg.AfterEvent(sale), previous.count - sale.Count);
                    previousFx = currentFx;
                }
            }

            // End of period
            currentFx = (decimal)Core.GetFXRate(Currency, end);

            result +=
                Core.Round(previous.count * price * currentFx) -
                Core.Round(previous.count * price * previousFx);

            return result;
        }

        public override decimal GetRevaluationGainsLosses(Core.TimeArg start, Core.TimeArg end, bool local, bool tax)
        {
            decimal result = 0;
            decimal amount = 0;

            foreach (var e in Events.InBounds(start, end))
            {
                if (e is Events.Impairment impairment)
                {
                    switch (impairment.ImpairmentType)
                    {
                        case Enterprise.Events.ImpairmentType.ToMarketValue:
                            amount = GetMarketValue(Core.TimeArg.BeforeEvent(impairment), false, local, tax) - GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), false, local, tax);
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.ToSpecifiedPrice:
                            amount = Core.Round(GetCount(Core.TimeArg.BeforeEvent(impairment)) * ((decimal)GetMarketPrice(Core.TimeArg.BeforeEvent(impairment), true) - impairment.Argument));
                            amount = Math.Min(amount, Math.Max(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax), GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax)));
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.MarketValuePercentage:
                            amount = Core.Round(GetCount(Core.TimeArg.BeforeEvent(impairment)) * GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax) * impairment.Argument);
                            amount = Math.Min(amount, Math.Max(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax), GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax)));
                            result -= amount;
                            break;
                        case Enterprise.Events.ImpairmentType.Amount:
                            result -= Math.Min(impairment.Argument, Math.Max(GetMarketValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax), GetAmortizedCostValue(Core.TimeArg.BeforeEvent(impairment), true, local, tax)));
                            break;
                        default: throw new Exception("Unknown impairment type.");
                    }
                }
            }

            return result;
        }

        //public override decimal GetUnrealizedFee(DateTime recognitionDate, int beforeTransactionIndex = -1)
        //{
        //    decimal count = 0;
        //    decimal fee = 0;
        //    decimal currentFee = 0;

        //    var events = Events.Where(x => x.RecognitionDate <= recognitionDate && (x.TransactionNo < beforeTransactionIndex || beforeTransactionIndex == -1));
        //    foreach (var e in events)
        //    {
        //        if (e is Events.Purchase)
        //        {
        //            count = (e as Events.Purchase).Count;
        //            fee = (e as Events.Purchase).Fee;
        //        }
        //        if (e is Events.Sale)
        //        {
        //            currentFee = Math.Round(((e as Events.Sale).Count / count) * fee, 2);
        //            count -= (e as Events.Sale).Count;
        //            fee -= currentFee;
        //        }
        //    }
        //    return fee;
        //}
    }
}
