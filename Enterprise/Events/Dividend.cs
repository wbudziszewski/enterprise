﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public class Dividend: Flow
    {
        public Dividend(Definitions.Dividend def, Investments.Investment inv)
        {
            this.RecordDate = def.RecordDate;
            this.ExDate = def.ExDate;
            this.PaymentDate = def.PaymentDate;
            this.Rate = def.PaymentPerShare;
            this.Currency = def.Currency;

            this.evtDate = def.PaymentDate;

            //TransactionNo = def.TransactionNo;
            Amount = Core.Round(inv.GetCount(Core.TimeArg.AtDate(def.RecordDate)) * (decimal)Rate);
            Fee = 0;
            Tax = Core.Round(Core.Round(Amount, Common.TaxRules.DepositPreTaxRounding) * Common.TaxRules.DepositTaxRate, Common.TaxRules.DepositPostTaxRounding);
            Currency = def.Currency;
            Portfolio = inv.Portfolio;
            CashAccount = inv.AssociatedCashAccount;

            Amount -= Tax;
        }

        public override string ToString()
        {
            return String.Format("Dividend @{0} / {1}: {2}", evtDate.ToString("yyyy-MM-dd"), TransactionIndex, Rate);
        }
    }
}
