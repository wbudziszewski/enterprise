﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public class Redemption : Flow
    {
        public bool Maturity { get; set; }

        public Redemption(DateTime date, double rate, string currency, double fxRate, bool maturity)
        {
            this.evtDate = date;
            this.RecordDate = Financial.Calendar.WorkingDays(date, -2);
            this.ExDate = Financial.Calendar.WorkingDays(date, -1);
            this.PaymentDate = date;
            this.Maturity = maturity;
            this.Rate = rate;
            this.Currency = currency;
            this.FXRate = fxRate;
        }

        public override string ToString()
        {
            return String.Format(Maturity ? "Redemption (nominal) @{0} / {1}: {2}" : "Redemption (coupon) @{0} / {1}: {2}", evtDate.ToString("yyyy-MM-dd"), TransactionIndex, Rate);
        }
    }
}
