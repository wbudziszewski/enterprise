﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public enum ImpairmentType { ToMarketValue, ToSpecifiedPrice, MarketValuePercentage, Amount }

    public class Impairment : Event
    {
        public ImpairmentType ImpairmentType { get; protected set; }

        public decimal Argument { get; set; }

        public Impairment()
        {
        }

        public override string ToString()
        {
            return String.Format("Impairment @{0} / {1}: {2}", evtDate.ToString("yyyy-MM-dd"), ImpairmentType, Argument);
        }
    }
}
