﻿using Budziszewski.Enterprise.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public abstract class Event: IComparable<Event>
    {
        public DateTime evtDate { get; set; }

        public DateTime settleDate { get; set; }

        public long TransactionIndex { get; set; }

        public string Currency { get; set; }

        private double? fxRate = null;

        public double FXRate
        {
            get
            {
                if (fxRate.HasValue) return fxRate.Value;
                return Core.GetFXRate(Currency, settleDate);
            }
            set
            {
                fxRate = value;
            }
        }

        protected static Investments.ValuationClass InterpretValuationClass(string valuationClass)
        {
            switch (valuationClass.ToUpper())
            {
                case "TRD": return Investments.ValuationClass.Trading;
                case "AFS": return Investments.ValuationClass.AvailableForSale;
                case "HTM": return Investments.ValuationClass.HeldToMaturity;
                default: return Investments.ValuationClass.Unspecified;
            }
        }

        int IComparable<Event>.CompareTo(Event other)
        {
            if (evtDate != other.evtDate) return evtDate.CompareTo(other.evtDate);
            return TransactionIndex.CompareTo(other.TransactionIndex);
        }


        public static bool operator < (Event a, Event b)
        {
            if (a.evtDate != b.evtDate) return a.evtDate <= b.evtDate;
            return a.TransactionIndex < b.TransactionIndex;
        }

        public static bool operator <= (Event a, Event b)
        {
            if (a.evtDate != b.evtDate) return a.evtDate <= b.evtDate;
            return a.TransactionIndex <= b.TransactionIndex;
        }

        public static bool operator >= (Event a, Event b)
        {
            if (a.evtDate != b.evtDate) return a.evtDate >= b.evtDate;
            return a.TransactionIndex >= b.TransactionIndex;
        }

        public static bool operator > (Event a, Event b)
        {
            if (a.evtDate != b.evtDate) return a.evtDate > b.evtDate;
            return a.TransactionIndex > b.TransactionIndex;
        }
    }
}
