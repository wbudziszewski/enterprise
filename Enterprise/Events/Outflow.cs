﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public class Outflow : Event
    {
        public string CashAccount { get; set; }

        public bool ExternalOutflow { get; set; } = false;

        public bool TaxPayment { get; set; } = false;

        public string EquityAccount { get; set; } = "";

        public decimal Amount { get; set; }

        public decimal Fee { get; protected set; }

        public decimal Tax { get; protected set; }

        public string Portfolio { get; set; }

        public Outflow(Definitions.Transaction t, bool atSettlement)
        {
            evtDate = t.TradeDate;
            settleDate = atSettlement ? t.SettlementDate : t.TradeDate;
            TransactionIndex = t.Index;
            Amount = t.GetSettlementAmount(true);
            Fee = 0;
            Tax = 0;
            Currency = t.Currency;
            FXRate = t.FXRate;
            Portfolio = t.Type == Definitions.TransactionType.Buy ? t.PortfolioDst : t.PortfolioSrc;
            CashAccount = t.Type == Definitions.TransactionType.Sell ? t.AccountDst : t.AccountSrc;
            ExternalOutflow = t.Type == Definitions.TransactionType.Cash && string.IsNullOrEmpty(t.AccountDst);
            if (t.Type == Definitions.TransactionType.Cash) { EquityAccount = t.EquityAccount; }
        }

        protected Outflow()
        {
        }

        public override string ToString()
        {
            return String.Format("Outflow @{0} / {1}: {2}", evtDate.ToString("yyyy-MM-dd"), TransactionIndex, Amount);
        }
    }
}
