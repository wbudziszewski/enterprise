﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public abstract class Flow : Inflow
    {
        public DateTime RecordDate { get; protected set; }

        public DateTime ExDate { get; protected set; }

        public DateTime PaymentDate { get; protected set; }

        public Flow() : base()
        {

        }
    }
}
