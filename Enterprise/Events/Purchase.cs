﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public class Purchase : Event
    {
        public string Instrument { get; set; }

        public decimal NominalAmount { get; set; }

        public decimal Count { get; set; }

        public double Price { get; set; } // dirty price for fixed income instruments!

        public string CustodyAccount { get; set; }

        public string CashAccount { get; set; }

        public Investments.ValuationClass ValuationClass { get; set; }

        public decimal Amount { get; protected set; } // less taxes/fees

        public decimal Fee { get; protected set; }

        public decimal Tax { get; protected set; }

        public string Portfolio { get; set; }

        public Purchase(Definitions.Transaction t)
        {
            if (t.Type != Definitions.TransactionType.Buy)
            {
                Log.Report(Severity.Error, "Purchase event must be created from 'buy' transaction.", t);
                return;
            }

            evtDate = t.TradeDate;
            settleDate = t.SettlementDate;
            TransactionIndex = t.Index;
            Instrument = t.InstrumentID;
            NominalAmount = t.NominalAmount;
            Currency = t.Currency;
            FXRate = t.FXRate;
            Count = t.Count;
            Price = t.Price;
            Portfolio = t.PortfolioDst;
            CustodyAccount = t.AccountDst;
            CashAccount = t.AccountSrc;
            ValuationClass = InterpretValuationClass(t.ValuationClass);
            Amount = t.GetSettlementAmount(false);
            Fee = t.Fee;
            Tax = 0;
        }

        public override string ToString()
        {
            return String.Format("Purchase @{0} / {1}: {2} {3}", evtDate.ToString("yyyy-MM-dd"), TransactionIndex, Instrument, Count);
        }
    }
}
