﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public class Inflow : Event
    {
        public string CashAccount { get; set; }

        public double Rate { get; set; } = 0;

        public bool ExternalInflow { get; set; } = false;

        public string EquityAccount { get; set; } = "";

        public decimal Amount { get; protected set; } // less taxes/fees

        public decimal Fee { get; protected set; }

        public decimal Tax { get; protected set; }

        public string Portfolio { get; set; }

        public Inflow(Definitions.Transaction t, bool atSettlement)
        {
            evtDate = t.TradeDate;
            settleDate = atSettlement ? t.SettlementDate : t.TradeDate;
            TransactionIndex = t.Index;
            Amount = t.GetSettlementAmount(true);
            Fee = 0;
            Tax = 0;
            Currency = t.Currency;
            FXRate = t.FXRate;
            Portfolio = t.Type == Definitions.TransactionType.Sell ? t.PortfolioSrc : t.PortfolioDst;
            CashAccount = t.Type == Definitions.TransactionType.Buy ? t.AccountSrc : t.AccountDst;
            ExternalInflow = t.Type == Definitions.TransactionType.Cash && string.IsNullOrEmpty(t.AccountSrc);
            if (t.Type == Definitions.TransactionType.Cash) { EquityAccount = t.EquityAccount; } 
        }

        protected Inflow()
        {

        }

        public override string ToString()
        {
            return String.Format("Inflow @{0} / {1}: {2}", evtDate.ToString("yyyy-MM-dd"), TransactionIndex, Amount);
        }
    }
}
