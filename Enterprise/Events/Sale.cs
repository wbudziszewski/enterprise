﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Events
{
    public class Sale : Event
    {
        public string Instrument { get; set; }

        public decimal NominalAmount { get; set; }

        public decimal Count { get; set; }

        public double Price { get; set; }

        public string CustodyAccount { get; set; }

        public string CashAccount { get; set; }

        public Investments.ValuationClass ValuationClass { get; set; }

        public decimal Amount { get; set; }

        public decimal Fee { get; protected set; }

        public decimal Tax { get; protected set; }

        public string Portfolio { get; set; }

        protected Sale()
        {
        }

        public Sale(Definitions.Transaction t)
        {
            if (t.Type != Definitions.TransactionType.Sell)
            {
                Log.Report(Severity.Error, "Sale event must be created from 'sell' transaction.", t);
                return;
            }

            evtDate = t.TradeDate;
            settleDate = t.SettlementDate;
            TransactionIndex = t.Index;
            Instrument = t.InstrumentID;
            NominalAmount = t.NominalAmount;
            Currency = t.Currency;
            FXRate = t.FXRate;
            Count = t.Count;
            Price = t.Price;
            Portfolio = t.PortfolioSrc;
            CustodyAccount = t.AccountSrc;
            CashAccount = t.AccountDst;
            ValuationClass = InterpretValuationClass(t.ValuationClass);
            Amount = t.GetSettlementAmount(false);
            Fee = t.Fee;
            Tax = 0;
        }

        public override string ToString()
        {
            return String.Format("Sale @{0} / {1}: {2} {3}", evtDate.ToString("yyyy-MM-dd"), TransactionIndex, Instrument, Count);
        }
    }
}
