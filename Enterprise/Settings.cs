﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Budziszewski.Enterprise
{
    /// <summary>
    /// Handles reading and saving settings of the program.
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Gets value of a setting, given its identifier.
        /// </summary>
        /// <param name="name">Unique identifier of a setting</param>
        /// <returns>Value of a setting</returns>
        public static object Get(string name)
        {
            try
            {
                return Properties.Settings.Default[name];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Sets value of a setting under its identifier
        /// </summary>
        /// <param name="name">Unique identifier of a setting</param>
        /// <param name="value">Value of a setting</param>
        public static void Set(string name, object value)
        {
            Properties.Settings.Default[name] = value;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Clears all settings.
        /// </summary>
        public static void Clear()
        {
            Properties.Settings.Default.Reset();
        }

    }
}

