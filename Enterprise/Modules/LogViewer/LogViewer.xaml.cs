﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Budziszewski.Enterprise.Modules
{
    /// <summary>
    /// Interaction logic for LogViewer.xaml 
    /// </summary>
    public partial class LogViewer : UserControl
    {
        public LogViewer()
        {
            InitializeComponent();

            Log.ResetUnread();
        }

        protected void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Dialogs.SFD.ShowDialog() == true)
            {
                var csv = new CSV(Dialogs.SFD.FileName);
                csv.Prepare<Log.LogEntry>(Log.Entries);
                csv.Write();
            }
        }

        protected void CopyBtn_Click(object sender, RoutedEventArgs e)
        {
            var csv = new CSV();
            csv.Prepare<Log.LogEntry>(Log.Entries);
            csv.CopyToClipboard();
        }

        private void DummyBtn_Click(object sender, RoutedEventArgs e)
        {
            Log.Report(Severity.Information, "Dummy log entry.");
        }
    }

}
