﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Budziszewski.Enterprise.Modules
{
    /// <summary>
    /// Interaction logic for BookingReview.xaml 
    /// </summary>
    public partial class BookingReview : UserControl
    {
        public BookingReview() : base()
        {
            InitializeComponent();

            TextIndex.Text = "";
            ComboBook.ItemsSource = Common.Books;
            if (ComboBook.Items.Count > 0) ComboBook.SelectedIndex = 0;
        }

        //public IEnumerable<ReportEntry> Process()
        //{
        //    // Generate snapshot entries for the given date.
        //    var result = new List<AccountingReportEntry>();
        //    foreach (var a in (ComboBook.SelectedItem as Accounting.Book).Accounts.Values)
        //    {
        //        foreach (var b in a.Entries)
        //        {
        //            result.Add(new AccountingReportEntry(a.Number, b));
        //        }
        //    }
        //    result = result.Where(x => x.OperationIndex == Convert.ToInt64(TextIndex.Text)).OrderBy(x => x.Date).ThenBy(x => x.AccountNumber).ToList();

        //    // Display sum
        //    result.Add(AccountingReportEntry.CreateSum(result));

        //    return result;
        //}

        //public void Present(List<ReportEntry> content)
        //{
        //    BookingReport.ColorBindingField = "IsSum";
        //    BookingReport.ColorConverter = new BooleanToBackgroundColorConverter();
        //    BookingReport.ItemsSource = content;
        //}
    }

}
