﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Modules
{
    public class FinancialEventEntry
    {
        public DateTime? EventDate { get; private set; }

        public string Portfolio { get; private set; }

        public string Account { get; private set; }

        public string Currency { get; private set; }

        public string Instrument { get; private set; }

        public string Type { get; private set; }

        public decimal Count { get; private set; }

        public decimal GrossAmount { get; private set; }

        public decimal Tax { get; private set; }

        public decimal NetAmount { get; private set; }

        // Presentation
        [HideProperty]
        public bool IsSum { get; set; }

        public FinancialEventEntry(Investments.Investment inv, Events.Event e)
        {
            if (e is Events.Dividend dividend)
            {
                decimal count = inv.GetCount(Core.TimeArg.BeforeEvent(dividend));
                decimal amount = Core.Round(count * (decimal)dividend.Rate);
                decimal tax = Core.Round(Core.Round(amount, Common.TaxRules.DividendPreTaxRounding) * Common.TaxRules.DividendTaxRate, Common.TaxRules.DividendPostTaxRounding);
                EventDate = dividend.PaymentDate;
                Portfolio = inv.Portfolio;
                Account = inv.AssociatedCashAccount;
                Currency = dividend.Currency;
                Instrument = inv.Name;
                Type = "Dividend";
                Count = count;
                GrossAmount = amount;
                Tax = tax;
                NetAmount = amount - tax;
            }
            if (e is Events.Redemption redemption)
            {
                decimal count = inv.GetCount(Core.TimeArg.BeforeEvent(redemption));
                decimal amount = Core.Round(inv.GetNominalAmount(Core.TimeArg.BeforeEvent(redemption), true, false) * (decimal)redemption.Rate, 2);
                decimal tax = Core.Round(Core.Round(amount, Common.TaxRules.CouponPreTaxRounding) * Common.TaxRules.CouponTaxRate, Common.TaxRules.CouponPostTaxRounding);
                EventDate = redemption.evtDate;
                Portfolio = inv.Portfolio;
                Account = inv.AssociatedCashAccount;
                Currency = redemption.Currency;
                Instrument = inv.Name;
                Type = redemption.Maturity ? "Maturity" : "Coupon";
                Count = inv.GetCount(Core.TimeArg.BeforeEvent(redemption));
                GrossAmount = amount;
                Tax = tax;
                NetAmount = amount - tax;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}: {2:N2}", Instrument, Type, GrossAmount);
        }
    }
}
