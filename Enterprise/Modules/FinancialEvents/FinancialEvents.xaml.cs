﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Budziszewski.Enterprise.Modules
{
    /// <summary>
    /// Interaction logic for FinancialEvents.xaml 
    /// </summary>
    public partial class FinancialEvents : UserControl
    {
        public FinancialEvents() : base()
        {
            InitializeComponent();

            StartDate.Text = Common.StartDate.ToString("yyyy-MM-dd");
            EndDate.Text = Common.CurrentDate.ToString("yyyy-MM-dd");
        }

        public IEnumerable<FinancialEventEntry> Process()
        {
            DateTime startDate = Conversion.ToDateTimeOrNull(StartDate.Text) ?? Common.StartDate;
            DateTime endDate = Conversion.ToDateTimeOrNull(EndDate.Text) ?? Common.CurrentDate;

            // Generate snapshot entries for the given date.
            var result = new List<FinancialEventEntry>();

            //var inv = Investments.Investments.Generate(Common.AbsoluteStartDate, date);
            List<Events.Event> evts = new List<Events.Event>();
            foreach (var i in Common.Books[0].Investments)
            {
                var newEvents = i.GetFlowEvents();
                foreach (var evt in newEvents)
                {
                    var newEntry = new FinancialEventEntry(i, evt);
                    if (newEntry.EventDate <= startDate) continue;
                    if (newEntry.EventDate > endDate) continue;
                    result.Add(newEntry);
                }
            }
            result = result.OrderBy(x => x.EventDate).ToList();

            return result;
        }

        //public void Present(List<ReportEntry> content)
        //{
        //    Report.ItemsSource = content;
        //}

        private void StartDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            var date = Conversion.ToDateTimeOrNull((sender as TextBox).Text);
            if (date != null) Common.StartDate = date.Value;
        }

        private void EndDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            var date = Conversion.ToDateTimeOrNull((sender as TextBox).Text);
            if (date != null) Common.CurrentDate = date.Value;
        }

        private void StartDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GenerateBtn_Click(sender, new RoutedEventArgs());
        }

        private void GenerateBtn_Click(object sender, RoutedEventArgs e)
        {
            Process();
        }
    }

}
