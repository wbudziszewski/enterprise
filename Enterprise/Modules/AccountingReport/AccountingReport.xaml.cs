﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Budziszewski.Enterprise.Modules
{
    /// <summary>
    /// Interaction logic for AccountingReport.xaml 
    /// </summary>
    public partial class AccountingReport : UserControl
    {
        public AccountingReport(Type type) : base()
        {
            InitializeComponent();

            StartDate.Text = Common.StartDate.ToString("yyyy-MM-dd");
            EndDate.Text = Common.CurrentDate.ToString("yyyy-MM-dd");

            ComboBook.ItemsSource = Common.Books;
            ComboBook.SelectedIndex = 0;

            //StartDate.Text = Conversion.ToString(settings.StartDate);
            //EndDate.Text = Conversion.ToString(settings.EndDate);
            //ComboBook.SelectedItem = Common.Books.FirstOrDefault(x => x.Name == settings.Book);
            //TextAccountFilter.Text = settings.GetValue<string>("Filter");
        }

        private void StartDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            var date = Conversion.ToDateTimeOrNull((sender as TextBox).Text);
            if (date != null) Common.StartDate = date.Value;
        }

        private void EndDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            var date = Conversion.ToDateTimeOrNull((sender as TextBox).Text);
            if (date != null) Common.CurrentDate = date.Value;
        }

        private void StartDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GenerateBtn_Click(sender, new RoutedEventArgs());
        }

        private void ComboBook_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBook.SelectedItem as Accounting.Book == null)
            {
                MessageBox.Show("No book selected.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            List<Accounting.Account> accounts = (ComboBook.SelectedItem as Accounting.Book).GetAccounts().ToList();
            accounts.Insert(0, new Accounting.Account());
            ComboAccount.ItemsSource = accounts;
            if (accounts.Count > 0) ComboAccount.SelectedIndex = 0;
        }

        public IEnumerable<AccountingReportEntry> Process()
        {
            Accounting.Book book = Common.Books.Find(x => x.Name == (ComboBook.SelectedItem as Accounting.Book).Name);
            DateTime? startDate = Conversion.ToDateTimeOrNull(StartDate.Text);
            DateTime? endDate = Conversion.ToDateTimeOrNull(EndDate.Text);

            // Generate snapshot entries for the given date.
            var result = new List<AccountingReportEntry>();
            var aq = new Accounting.AccountQuery
            {
                Start = startDate,
                End = endDate,
            };
            var a = book.Accounts.Values.First(x => x.Number == ComboBook.SelectedItem.ToString().Substring(0, 6));
            foreach (var b in a.Get(aq))
            {
                result.Add(new AccountingReportEntry(a.Number, b));
            }

            result = result.OrderBy(x => x.Date).ThenBy(x => x.AccountNumber).ToList();

            // Display sum
            result.Add(AccountingReportEntry.CreateSum(result));

            return result;
        }

        //public void Present(List<AccountingReportEntry> content)
        //{
        //    Report.ColorBindingField = "IsSum";
        //    Report.ColorConverter = new BooleanToBackgroundColorConverter();
        //    Report.ItemsSource = content;
        //}

        private void GenerateBtn_Click(object sender, RoutedEventArgs e)
        {
            Process();
        }
    }

    public class AccountingReportSettings
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Accounting.Book Book { get; set; }

        public string Filter { get; set; }
    }
}
