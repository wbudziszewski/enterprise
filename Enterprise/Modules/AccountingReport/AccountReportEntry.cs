﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Budziszewski.Enterprise.Modules
{
    public class AccountingReportEntry
    {
        public long? InvestmentIndex { get; private set; }
        public long? OperationIndex { get; private set; }
        public long? TransactionIndex { get; private set; }

        public DateTime? Date { get; private set; }

        public string AccountNumber { get; private set; }

        public string Name { get; private set; }

        public decimal? Amount { get; private set; }

        public string Flags { get; private set; }

        [HideProperty]
        public bool IsSum { get; set; }

        private AccountingReportEntry()
        {
            // Sum
            InvestmentIndex = null;
            OperationIndex = null;
            TransactionIndex = null;
            Date = null;
            AccountNumber = null;
            Name = "";
            Amount = 0;
            IsSum = false;
            Flags = "";
        }

        public AccountingReportEntry(string account, Accounting.Booking b)
        {
            InvestmentIndex = b.InvestmentIndex;
            OperationIndex = b.OperationIndex;
            TransactionIndex = b.TransactionIndex;
            Date = b.Date;
            AccountNumber = account;
            Name = b.Description;
            Amount = b.Amount;
            Flags = Core.Concat("; ",
                b.Properties.YearClosing ? "BooksClosing" : "",
                b.Properties.TaxCharge ? "TaxCharge" : "");
        }

        public static AccountingReportEntry CreateSum(List<AccountingReportEntry> re)
        {
            var e = new AccountingReportEntry()
            {
                Name = "TOTAL",
                Amount = re.Sum(x => x.Amount),
                IsSum = true
            };
            return e;
        }

        public override string ToString()
        {
            return String.Format("{0} ({1}): {2:N2}", AccountNumber, Name, Amount);
        }
    }
}
