﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Budziszewski.Enterprise.Modules
{
    /// <summary>
    /// Interaction logic for ValueEditor.xaml 
    /// </summary>
    public partial class ValueEditor : UserControl
    {
        public ObservableCollection<DataPoint> Data { get; set; }

        public bool Unsaved
        {
            get { return (bool)GetValue(UnsavedProperty); }
            set { SetValue(UnsavedProperty, value); }
        }
        public static readonly DependencyProperty UnsavedProperty = DependencyProperty.Register("Unsaved", typeof(bool), typeof(ValueEditor), new PropertyMetadata(false));

        public ValueEditor() : base()
        {
            InitializeComponent();
            DataContext = this;

            Data = new ObservableCollection<DataPoint>();
            Report.ItemsSource = Data;

            ValueTypeCombo.Items.Add("Transactions");
            ValueTypeCombo.Items.Add("Accounting schemes");
            ValueTypeCombo.Items.Add("Book accounts");
            ValueTypeCombo.Items.Add("Coupons");
            ValueTypeCombo.Items.Add("Dividends");
            ValueTypeCombo.Items.Add("Instruments");
            ValueTypeCombo.Items.Add("Prices");
            ValueTypeCombo.Items.Add("Special events");
            ValueTypeCombo.SelectedIndex = 0;

            Unsaved = false;
        }

        protected void ImportBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Dialogs.OFD.ShowDialog() == true)
            {
                CSV csv = new CSV(Dialogs.OFD.FileName);
                csv.Read();

                Data.Clear();

                switch (ValueTypeCombo.SelectedItem.ToString())
                {
                    case "Transactions": Populate<Definitions.Transaction>(Data, csv.Interpret<Definitions.Transaction>(true)); break;
                    case "Accounting schemes": Populate<Definitions.AccountingScheme>(Data, csv.Interpret<Definitions.AccountingScheme>()); break;
                    case "Book accounts": Populate<Definitions.BookAccount>(Data, csv.Interpret<Definitions.BookAccount>()); break;
                    case "Coupons": Populate<Definitions.Coupon>(Data, csv.Interpret<Definitions.Coupon>()); break;
                    case "Dividends": Populate<Definitions.Dividend>(Data, csv.Interpret<Definitions.Dividend>()); break;
                    case "Instruments": Populate<Definitions.Instrument>(Data, csv.Interpret<Definitions.Instrument>()); break;
                    case "Prices": Populate<Definitions.Price>(Data, csv.Interpret<Definitions.Price>()); break;
                    case "Special events": Populate<Definitions.Price>(Data, csv.Interpret<Definitions.Price>()); break;
                    default:
                        Log.Report(Severity.Error, "Unknown definitions type in ValueEditor.ImportBtn_Click method.", ValueTypeCombo.SelectedItem);
                        break;
                }

                Unsaved = true;
            }
        }

        private void Populate<T>(ObservableCollection<DataPoint> collectionToPopulate, IEnumerable<T> newItems) where T : DataPoint
        {
            foreach (T item in newItems)
            {
                collectionToPopulate.Add(item);
            }
        }

        private void ApplyBtn_Click(object sender, RoutedEventArgs e)
        {
            switch (ValueTypeCombo.SelectedItem.ToString())
            {
                case "Transactions": Database.Set<Definitions.Transaction>(Data); break;
                case "Accounting schemes": Database.Set<Definitions.AccountingScheme>(Data); break;
                case "Book accounts": Database.Set<Definitions.BookAccount>(Data); ; break;
                case "Coupons": Database.Set<Definitions.Coupon>(Data); break;
                case "Dividends": Database.Set<Definitions.Dividend>(Data); break;
                case "Instruments": Database.Set<Definitions.Instrument>(Data); break;
                case "Prices": Database.Set<Definitions.Price>(Data); break;
                //case "Special events": Database.Set<Definitions.Price>(Data); break;
                default:
                    Log.Report(Severity.Error, "Unknown definitions type in ValueEditor.ApplyBtn_Click method.", ValueTypeCombo.SelectedItem);
                    break;
            }

            Unsaved = false;
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            Data.Clear();

            Unsaved = true;
        }

        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Dialogs.SFD.ShowDialog() == true)
            {
                var csv = new CSV(Dialogs.SFD.FileName);
                switch (ValueTypeCombo.SelectedItem.ToString())
                {
                    case "Transactions": csv.Prepare<Definitions.Transaction>(Data.Cast<Definitions.Transaction>()); break;
                    case "Accounting schemes": csv.Prepare<Definitions.AccountingScheme>(Data.Cast<Definitions.AccountingScheme>()); break;
                    case "Book accounts": csv.Prepare<Definitions.BookAccount>(Data.Cast<Definitions.BookAccount>()); break;
                    case "Coupons": csv.Prepare<Definitions.Coupon>(Data.Cast<Definitions.Coupon>()); break;
                    case "Dividends": csv.Prepare<Definitions.Dividend>(Data.Cast<Definitions.Dividend>()); break;
                    case "Instruments": csv.Prepare<Definitions.Instrument>(Data.Cast<Definitions.Instrument>()); break;
                    case "Prices": csv.Prepare<Definitions.Price>(Data.Cast<Definitions.Price>()); break;
                    //case "Special events": Database.Set<Definitions.Price>(Data); break;
                    default:
                        Log.Report(Severity.Error, "Unknown definitions type in ValueEditor.ExportBtn_Click method.", ValueTypeCombo.SelectedItem);
                        break;
                }
                csv.Write();
            }
        }

        private void CopyBtn_Click(object sender, RoutedEventArgs e)
        {
            var csv = new CSV();
            switch (ValueTypeCombo.SelectedItem.ToString())
            {
                case "Transactions": csv.Prepare<Definitions.Transaction>(Data.Cast<Definitions.Transaction>()); break;
                case "Accounting schemes": csv.Prepare<Definitions.AccountingScheme>(Data.Cast<Definitions.AccountingScheme>()); break;
                case "Book accounts": csv.Prepare<Definitions.BookAccount>(Data.Cast<Definitions.BookAccount>()); break;
                case "Coupons": csv.Prepare<Definitions.Coupon>(Data.Cast<Definitions.Coupon>()); break;
                case "Dividends": csv.Prepare<Definitions.Dividend>(Data.Cast<Definitions.Dividend>()); break;
                case "Instruments": csv.Prepare<Definitions.Instrument>(Data.Cast<Definitions.Instrument>()); break;
                case "Prices": csv.Prepare<Definitions.Price>(Data.Cast<Definitions.Price>()); break;
                //case "Special events": Database.Set<Definitions.Price>(Data); break;
                default:
                    Log.Report(Severity.Error, "Unknown definitions type in ValueEditor.CopyBtn_Click method.", ValueTypeCombo.SelectedItem);
                    break;
            }
            csv.CopyToClipboard();
        }

        private void ValueTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Data.Clear();
            switch (ValueTypeCombo.SelectedItem.ToString())
            {
                case "Transactions":
                    Populate<Definitions.Transaction>(Data, Database.Get<Definitions.Transaction>());
                    gv.Columns.Clear();

                    gv.Columns.Add(new GridViewColumn() { Header = "Type", DisplayMemberBinding = new Binding("Type") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Instrument ID", DisplayMemberBinding = new Binding("InstrumentID") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Trade date", CellTemplate = Common.GetCellTemplate(typeof(DateTime), "TradeDate") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Settlement date", CellTemplate = Common.GetCellTemplate(typeof(DateTime), "SettlementDate") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Count", CellTemplate = Common.GetCellTemplate(typeof(double), "Value", 4) });
                    gv.Columns.Add(new GridViewColumn() { Header = "Currency", DisplayMemberBinding = new Binding("Currency") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Nominal amount", CellTemplate = Common.GetCellTemplate(typeof(double), "Nominal amount") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Price", CellTemplate = Common.GetCellTemplate(typeof(double), "Price") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Fee", CellTemplate = Common.GetCellTemplate(typeof(double), "Fee") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Source account", DisplayMemberBinding = new Binding("AccountSrc") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Destination account", DisplayMemberBinding = new Binding("AccountDst") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Source portfolio", DisplayMemberBinding = new Binding("PortfolioSrc") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Destination portfolio", DisplayMemberBinding = new Binding("PortfolioDst") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Valuation class", DisplayMemberBinding = new Binding("ValuationClass") });
                    gv.Columns.Add(new GridViewColumn() { Header = "FX rate", CellTemplate = Common.GetCellTemplate(typeof(double), "FXRate", 4) });
                    gv.Columns.Add(new GridViewColumn() { Header = "Tax payment", CellTemplate = Common.GetCellTemplate(typeof(bool), "TaxPayment") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Equity account", DisplayMemberBinding = new Binding("EquityAccount") });

                    break;

                case "Accounting schemes":
                    break;
                case "Book accounts":
                    break;
                case "Coupons":
                    break;
                case "Dividends":
                    break;
                case "Instruments":
                    break;
                case "Prices":
                    Populate<Definitions.Price>(Data, Database.Get<Definitions.Price>());
                    gv.Columns.Clear();

                    gv.Columns.Add(new GridViewColumn() { Header = "Instrument ID", DisplayMemberBinding = new Binding("InstrumentID") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Date", CellTemplate = Common.GetCellTemplate(typeof(DateTime), "Date") });
                    gv.Columns.Add(new GridViewColumn() { Header = "Value", CellTemplate = Common.GetCellTemplate(typeof(double), "Value") });
                    break;
                //case "Special events": Database.Set<Definitions.Price>(Data); break;
                default:
                    Log.Report(Severity.Error, "Unknown definitions type in ValueEditor.ValueTypeCombo_SelectionChanged method.", ValueTypeCombo.SelectedItem);
                    break;
            }
        }
    }
}
