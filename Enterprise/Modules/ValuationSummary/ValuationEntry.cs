﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Budziszewski.Enterprise.Modules
{
    public class ValuationEntry
    {
        public long? Index { get; private set; }

        public string Name { get; private set; }

        public string Type { get; private set; }

        public string ValuationClass { get; private set; }

        public string Portfolio { get; private set; }

        public string Account { get; private set; }

        public string Currency { get; private set; }

        public DateTime? PurchaseDate { get; private set; }

        public DateTime? MaturityDate { get; private set; }

        public DateTime? NextCouponDate { get; private set; }

        [PercentageProperty]
        public double? CouponRate { get; private set; }

        public decimal? Count { get; private set; }

        public double? PurchasePrice { get; private set; }

        public double? MarketPrice { get; private set; }

        public double? AmortizedCostPrice { get; private set; }

        public double? Interest { get; private set; }

        public decimal? NominalAmount { get; private set; }

        public decimal? InterestAmount { get; private set; }

        public decimal? PurchaseCleanValue { get; private set; }

        public decimal? PurchaseDirtyValue { get; private set; }

        public decimal? MarketCleanValue { get; private set; }

        public decimal? MarketDirtyValue { get; private set; }

        public decimal? AmortizedCostCleanValue { get; private set; }

        public decimal? AmortizedCostDirtyValue { get; private set; }

        public decimal? BookValue { get; private set; }

        public double? Tenor { get; private set; }

        public double? ModifiedDuration { get; private set; }

        [PercentageProperty]
        public double? MarketYield { get; private set; }

        [PercentageProperty]
        public double? PurchaseYield { get; private set; }

        [PercentageProperty]
        public double? BookYield { get; private set; }

        // Presentation
        [HideProperty]
        public bool IsSum { get; set; }

        public ValuationEntry(long index, DateTime date, Investments.Investment inv)
        {
            Index = index;
            Name = inv.Name;
            Type = inv.Type;
            ValuationClass = inv.ValClass;
            Portfolio = inv.Portfolio;
            Account = inv.CustodyAccount;
            Currency = inv.Currency;
            PurchaseDate = inv.GetPurchaseDate();
            MaturityDate = inv.GetMaturityDate();
            NextCouponDate = inv.GetNextCouponDate(date);
            Count = inv.GetCount(Core.TimeArg.AtDate(date));
            CouponRate = inv.GetCouponRate(date);
            PurchasePrice = inv.GetPurchasePrice(Core.TimeArg.AtDate(date), false);
            MarketPrice = inv.GetMarketPrice(Core.TimeArg.AtDate(date), false);
            AmortizedCostPrice = inv.GetAmortizedCostPrice(Core.TimeArg.AtDate(date), false);
            Interest = inv.GetAccruedInterest(date);
            NominalAmount = inv.GetNominalAmount(Core.TimeArg.AtDate(date), true, false);
            InterestAmount = inv.GetInterestAmount(Core.TimeArg.AtDate(date), true, false);
            PurchaseCleanValue = inv.GetPurchaseAmount(Core.TimeArg.AtDate(date), false, true, false);
            PurchaseDirtyValue = inv.GetPurchaseAmount(Core.TimeArg.AtDate(date), true, true, false);
            MarketCleanValue = inv.GetMarketValue(Core.TimeArg.AtDate(date), false, true, false);
            MarketDirtyValue = inv.GetMarketValue(Core.TimeArg.AtDate(date), true, true, false);
            AmortizedCostCleanValue = inv.GetAmortizedCostValue(Core.TimeArg.AtDate(date), false, true, false);
            AmortizedCostDirtyValue = inv.GetAmortizedCostValue(Core.TimeArg.AtDate(date), true, true, false);
            BookValue = inv.GetValue(date);
            Tenor = inv.GetTenor(date);
            ModifiedDuration = inv.GetModifiedDuration(date);
            MarketYield = inv.GetYieldToMaturity(date, inv.GetMarketPrice(Core.TimeArg.AtDate(date), false));
            PurchaseYield = inv.GetYieldToMaturity(date, inv.GetAmortizedCostPrice(Core.TimeArg.AtDate(date), false));
        }

        public override string ToString()
        {
            return String.Format("{0}: {1:N2}", Name, BookValue);
        }
    }
}
