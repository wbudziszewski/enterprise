﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Budziszewski.Enterprise.Modules
{
    /// <summary>
    /// Interaction logic for ValuationSummary.xaml 
    /// </summary>
    public partial class ValuationSummary : UserControl
    {
        public ValuationSummary() : base()
        {
            InitializeComponent();

            ReportDate.Text = Common.CurrentDate.ToString("yyyy-MM-dd");
        }

        public List<ValuationEntry> Process()
        {
            DateTime date = Conversion.ToDateTime(ReportDate.Text);

            // Generate snapshot entries for the given date.
            var result = new List<ValuationEntry>();
            int index = 1;

            foreach (var i in Common.Books[0].Investments)
            {
                decimal mv = i.GetValue(date);
                if (mv != 0)
                {
                    result.Add(new ValuationEntry(index++, date, i));
                }
            }

            return result;
        }

        public void Present(List<ValuationEntry> content)
        {
            ValuationReport.ItemsSource = content;
        }

        private void ReportDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            var date = Conversion.ToDateTimeOrNull((sender as TextBox).Text);
            if (date != null) Common.CurrentDate = date.Value;
        }

        private void GenerateBtn_Click(object sender, RoutedEventArgs e)
        {            
            Present(Process());
        }

        private void CopyBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {

        }
    }

}
