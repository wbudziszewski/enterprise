﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise
{
    public class ExternalAttribute: Attribute
    {
    }

    public class HidePropertyAttribute : Attribute
    {
    }

    public class PercentagePropertyAttribute : Attribute
    {
    }

    public class PresentationPrecisionAttribute : Attribute
    {
        public int Precision { get; private set; }

        public PresentationPrecisionAttribute(int precision)
        {
            Precision = precision;
        }
    }

    public class NonSortableAttribute : Attribute
    {
    }

    public class HeadersAttribute : Attribute
    {
        public string[] Headers { get; set; }

        public HeadersAttribute(params string[] headers)
        {
            Headers = headers;
        }
    }

    public static class PropertyInfoExtension
    {
        public static bool HasHeader(this PropertyInfo pi, string header)
        {
            if (!Attribute.IsDefined(pi, typeof(HeadersAttribute))) return false;
            return ((HeadersAttribute)Attribute.GetCustomAttribute(pi, typeof(HeadersAttribute))).Headers.FirstOrDefault(x => String.Compare(x, header, true) == 0) != null;
        }

        public static bool IsExternal(this PropertyInfo pi)
        {
            return Attribute.IsDefined(pi, typeof(ExternalAttribute));
        }        
    }

    public static class GetAttributeExtension
    {
        public static T GetAttribute<T>(this Type t) where T:Attribute
        {
            return (T)(t.GetCustomAttributes(typeof(T), true).FirstOrDefault());
        }

        public static bool HasAttribute<T>(this Type t) where T : Attribute
        {
            return t.GetCustomAttributes(typeof(T), true).Count() > 0;
        }

        public static T GetAttribute<T>(this PropertyInfo pi) where T : Attribute
        {
            return (T)(pi.GetCustomAttributes(typeof(T), true).FirstOrDefault());
        }

        public static bool HasAttribute<T>(this PropertyInfo pi) where T : Attribute
        {
            return pi.GetCustomAttributes(typeof(T), true).Count() > 0;
        }
    }

    #region Accounting schemes

    /// <summary>
    /// Specifies order (from highest amount to lowest) in which accounting schemes are called for a given date.
    /// </summary>
    public class PriorityAttribute : Attribute
    {
        public int Priority { get; private set; }

        public PriorityAttribute(int priority)
        {
            Priority = priority;
        }
    }

    #endregion
}
