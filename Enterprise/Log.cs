﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise
{
    /// <summary>
    /// Type (severity) of a single log entry.
    /// </summary>
    public enum Severity { Information, Warning, Error, CriticalError }

    /// <summary>
    /// Keeps log of the program activities.
    /// </summary>
    public static class Log
    {
        static int unreadWarnings = 0;
        static int unreadErrors = 0;
        static int unreadCriticalErrors = 0;

        /// <summary>
        /// Returns a list of log entries.
        /// </summary>
        public static ObservableCollection<LogEntry> Entries { get; private set; } = new ObservableCollection<LogEntry>();

        public static void Report(Severity severity, string message, object data = null, Exception exception = null)
        {
            LogEntry newEntry = new LogEntry(severity, message, data, exception);
            LogEntry similar = Entries.LastOrDefault(x => x.IsSimilar(newEntry));
            if (similar == null)
            {
                Entries.Add(newEntry);
            }
            else
            {
                similar.Count++;
            }
            switch (severity)
            {
                case Severity.Warning: unreadWarnings++; break;
                case Severity.Error: unreadErrors++; break;
                case Severity.CriticalError: unreadCriticalErrors++; break;
                default: break;
            }

            OnReport(newEntry);
        }

        public static void ResetUnread()
        {
            unreadWarnings = 0;
            unreadErrors = 0;
            unreadCriticalErrors = 0;
            OnReport(null);
        }

        #region Event

        public class ReporterEventArgs : EventArgs
        {
            public int UnreadWarnings { get; private set; }
            public int UnreadErrors { get; private set; }
            public int UnreadCriticalErrors { get; private set; }

            public ReporterEventArgs()
            {
                UnreadWarnings = unreadWarnings;
                UnreadErrors = unreadErrors;
                UnreadCriticalErrors = unreadCriticalErrors;
            }
        }

        public static event ReporterHandler Reported;
        public delegate void ReporterHandler(object sender, ReporterEventArgs e);
        static void OnReport(object sender)
        {
            Reported?.Invoke(sender, new ReporterEventArgs());

        }

        #endregion

        public class LogEntry : INotifyPropertyChanged
        {
            public DateTime Time { get; private set; }
            public Severity Severity { get; private set; }
            public string Message { get; private set; }
            public object Data { get; private set; }
            public Exception Exception { get; private set; }

            private int count;
            public int Count {
                get => count;
                set => SetField(ref count, value);
            }

            public LogEntry(Severity severity, string message, object data, Exception exception)
            {
                Time = DateTime.Now;
                Severity = severity;
                Message = message;
                Data = data;
                Exception = exception;
                Count = 1;
            }

            #region INotifyPropertyChanged boilerplate https://stackoverflow.com/questions/1315621/implementing-inotifypropertychanged-does-a-better-way-exist

            public event PropertyChangedEventHandler PropertyChanged;
            protected void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
            {
                if (EqualityComparer<T>.Default.Equals(field, value)) return false;
                field = value;
                OnPropertyChanged(propertyName);
                return true;
            }

            #endregion

            public bool IsSimilar(LogEntry other)
            {
                return Severity == other.Severity && Message == other.Message && (other.Time - Time).TotalSeconds <= 3;
            }
        }
    }


}
