﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Budziszewski.Enterprise
{
    /// <summary>
    /// Contains core references and settings of the program.
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// Reference to the program main window.
        /// </summary>
        public static MainWindow MainWindow { get; set; }

        /// <summary>
        /// Start date for which the books are initialized.
        /// </summary>
        public static DateTime StartDate { get; set; }

        /// <summary>
        /// End date for which the books are initialized.
        /// </summary>
        public static DateTime EndDate { get; set; }

        /// <summary>
        /// Current date, initialized to yesterday.
        /// </summary>
        public static DateTime CurrentDate { get; set; }

        /// <summary>
        /// Default currency that will be treated as local.
        /// </summary>
        public readonly static string DomesticCurrency = "PLN";

        /// <summary>
        /// List of accounting books.
        /// </summary>
        public static List<Accounting.Book> Books { get; set; } = new List<Accounting.Book>();

        public static void Initialize(MainWindow mainWnd)
        {
            MainWindow = mainWnd;
            try
            {
                Books.Add(new Accounting.Book("GAAP") { Priority = 0, CalculateTimeValueOfMoney = true, CalculateUnrealizedFXEffects = true, ApplyTaxRules = false });
                Books.Add(new Accounting.Book("Tax") { Priority = 1, CalculateTimeValueOfMoney = false, CalculateUnrealizedFXEffects = false, ApplyTaxRules = true });
                Books[0].TaxBook = Books[1];
            }
            catch
            {
                MessageBox.Show("Error setting up books. Try setting up accounts first and then restart the program.", "Critical error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            CurrentDate = Calendar.ReachEndOfMonth(DateTime.Now.AddDays(-1), 0);
            EndDate = Calendar.ReachEndOfYear(DateTime.Now, 0);
            StartDate = new DateTime(2014, 12, 31);
        }

        public static void Close()
        {
        }

        public static void ProcessBooks()
        {
            foreach (var book in Books.OrderByDescending(x => x.Priority))
            {
                if (book.IsProcessed) continue;
                book.Clear();
                book.Process();
            }
        }

        public static void ResetBooks()
        {
            Books.ForEach(x => x.Clear());
        }

        public static string GetTempPath(string dirname = "")
        {
            if (string.IsNullOrEmpty(dirname))
                return Path.Combine(Path.GetTempPath(), "Enterprise", Guid.NewGuid().ToString());
            else
                return Path.Combine(Path.GetTempPath(), "Enterprise", dirname, Guid.NewGuid().ToString());
        }

        public static bool IsZip(string path)
        {
            try
            {
                if (!File.Exists(path)) return false;
                using (System.IO.BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    byte[] buffer = new byte[2];
                    int count = br.Read(buffer, 0, 2);
                    if (count < 2) return false;
                    return buffer[0] == 80 && buffer[1] == 75;
                }
            }
            catch
            {
                return false;
            }
        }

        public static DataTemplate GetCellTemplate(Type type, string bindingPath, int precision = 2)
        {
            DataTemplate dt = new DataTemplate();
            dt.DataType = type;

            FrameworkElementFactory tbFactory = new FrameworkElementFactory(typeof(TextBlock));
            Binding b = new Binding(bindingPath);
            if (type == typeof(DateTime)) { b.StringFormat = "yyyy-MM-dd"; }
            if (type == typeof(double)) { b.StringFormat = precision < 1 ? "0" : "0.".PadRight(precision + 2, '0'); }
            tbFactory.SetBinding(TextBlock.TextProperty, b);
            tbFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Right);
            tbFactory.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Right);

            //set the visual tree of the data template
            dt.VisualTree = tbFactory;
            return dt;
        }

        public static class TaxRules
        {
            public static decimal DividendTaxRate { get; set; } = 0.19m;
            public static Core.RoundingRules DividendPreTaxRounding { get; set; } = Core.RoundingRules.Monetary;
            public static Core.RoundingRules DividendPostTaxRounding { get; set; } = Core.RoundingRules.Integer;

            public static decimal CouponTaxRate { get; set; } = 0.19m;
            public static Core.RoundingRules CouponPreTaxRounding { get; set; } = Core.RoundingRules.Monetary;
            public static Core.RoundingRules CouponPostTaxRounding { get; set; } = Core.RoundingRules.Monetary;

            public static decimal DepositTaxRate { get; set; } = 0.19m;
            public static Core.RoundingRules DepositPreTaxRounding { get; set; } = Core.RoundingRules.Monetary;
            public static Core.RoundingRules DepositPostTaxRounding { get; set; } = Core.RoundingRules.Monetary;

            public static decimal SingleYearTaxLossConsumptionRatio { get; set; } = 0.5m;
            public static int TaxLossAccountingYears { get; set; } = 5;

            public static decimal IncomeTaxRate { get; set; } = 0.19m;
            public static Core.RoundingRules IncomePreTaxRounding { get; set; } = Core.RoundingRules.Monetary;
            public static Core.RoundingRules IncomePostTaxRounding { get; set; } = Core.RoundingRules.Integer;
        }
    }

    public static class Dialogs
    {
        public static OpenFileDialog OFD { get; private set; } = new OpenFileDialog()
        {
            DefaultExt = ".csv",
            Filter = "Comma-separated values file (*.csv)|*.csv|All files (*.*)|*.*"
        };

        public static SaveFileDialog SFD { get; private set; } = new SaveFileDialog()
        {
            DefaultExt = ".csv",
            Filter = "Comma-separated values file (*.csv)|*.csv|All files (*.*)|*.*"
        };
    }
}
