﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseDataserver
{
    public class ServerTask
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public bool ProgressVisible { get; set; }
        public double Progress { get; set; }
    }
}
