﻿using System;

namespace Budziszewski.Enterprise.Dataserver
{
    public class MarketEvent : IEquatable<MarketEvent>
    {
        public DateTime Time { get; set; }
        public string Instrument { get; set; }
        public string Type { get; set; }
        public double Value { get; set; }
        public double Value2 { get; set; }
        public string Source { get; set; }

        public bool Equals(MarketEvent other)
        {
            if (Time == DateTime.MinValue || Instrument == null || Type == null) return false;
            return Time.Equals(other.Time) && Instrument.Equals(other.Instrument) && Type.Equals(other.Type);
        }

        public override string ToString()
        {
            return Time.ToString() + ";" + Instrument + ";" + Type;
        }
    }
}
