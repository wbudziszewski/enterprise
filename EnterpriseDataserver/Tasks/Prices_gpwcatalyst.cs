﻿using Budziszewski.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Prices_gpwcatalyst : DataserverTask, IDisposable
    {
        public Prices_gpwcatalyst(Database db) : base("Prices (gpwcatalyst)", "gpwcatalyst", db)
        {
        }

        public override async Task Run()
        {
            try
            {
                ReportInfo?.Invoke(Name + ": Started");
                Status = "Initializing...";
                remotePath = "https://gpwcatalyst.pl/o-instrumentach-instrumenty-notowane";

                DateTime lastUpdate = db.GetLastUpdate("gpwcatalyst");

                Status = "Preparing list of files... " + remotePath;
                var page = Net.Http.DownloadWebPage(remotePath);
                var items = GetListOfItems(page);

                Status = "Working...";
                ProgressVisible = true;
                Progress = 0;

                var instruments = db.DownloadInstruments();
                int index = 0;

                foreach (var item in items)
                {
                    Status = "Downloading remote items... " + item;
                    var id = instruments.FirstOrDefault(x => x.Name == item).ID;
                    if (id == null)
                    {
                        throw new Exception("Cannot find instrument " + item + " on instruments list.");
                    }
                    var headers = new List<KeyValuePair<string, string>>();
                    string data = await Task.Run(() => Budziszewski.Net.Http.DownloadGet("https://gpwcatalyst.pl/chart-json.php?req=[{\"isin\":\"" + id + "\",\"mode\":\"ARCH\"}]", headers, false, false));
                    var json = new Budziszewski.Net.JSON(data);

                    Status = "Processing downloaded item... " + item;
                    var list = await Process(id, json);
                    Status = "Setting up intervals...";
                    list = SetUpIntervals(list).ToList();
                    Status = "Uploading data from item..." + item;
                    await db.Upload(list, Database.ReplacementMethod.Instrument);
                    Progress = index++ / (double)items.Count();
                }

                Progress = 1;
                ProgressVisible = false;
                Status = "Finished";
                ReportInfo?.Invoke(Name + ": Finished");
            }
            catch (DataserverException ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex.InnerException, ex.Command);
            }
            catch (Exception ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex, null);
            }
        }

        private IEnumerable<string> GetListOfItems(string page)
        {
            var currencies = new string[2] { "PLN", "EUR" };
            var html = new HtmlAgilityPack.HtmlDocument();
            html.LoadHtml(page);

            foreach (var currency in currencies)
            {
                var table = html.DocumentNode.SelectNodes(@"//*[@id=""bs-instrument-" + currency + @"""]").FirstOrDefault();
                if (table != null)
                {
                    foreach (var row in table.ChildNodes.Where(x => x.Name == "tr").Skip(1))
                    {
                        if (row.ChildNodes.Count < 6) continue;
                        var name = row.ChildNodes[5].ChildNodes["a"].InnerText.Trim();
                        if (!String.IsNullOrEmpty(name)) yield return name;
                    }
                }
            }
        }

        private async Task<List<Price>> Process(string id, Budziszewski.Net.JSON json)
        {
            List<Price> list = new List<Price>();

            await Task.Run(() =>
            {
                try
                {
                    foreach (var item in json.Root.Children[0].Children[0].Children.Where(x => !(x is JSONAttribute)))
                    {
                        var time = JavaTicksToDateTime(Int64.Parse((item.Children["t"] as Budziszewski.Net.JSONAttribute).Value.ToString()));
                        var price = Convert.ToDouble((item.Children["c"] as Budziszewski.Net.JSONAttribute).Value);

                        Price p = new Price()
                        {
                            Time = time,
                            Instrument = id,
                            Market = "PL_GPW",
                            Value = price,
                            Volume = 0,
                            Source = "gpwcatalyst"
                        };
                        list.Add(p);
                    }
                }
                catch (Exception ex)
                {
                    ReportInfo?.Invoke(ex.Message);
                }
            });

            return list;
        }

        public override void Dispose()
        {
            Status = "Finalized";
        }
    }
}
