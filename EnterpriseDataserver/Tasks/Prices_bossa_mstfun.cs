﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Prices_bossa_mstfun : DataserverTask, IDisposable
    {
        public Prices_bossa_mstfun(Database db) : base("Prices (bossa_mstfun)", "bossa_mstfun", db)
        {
        }

        public override async Task Run()
        {
            try
            {
                ReportInfo?.Invoke(Name + ": Started");
                Status = "Initializing...";
                remotePath = "http://bossa.pl/pub/fundinwest/mstock/";

                DateTime lastUpdate = db.GetLastUpdate(Source);

                Status = "Preparing list of files... " + remotePath;
                var page = Net.Http.DownloadWebPage(remotePath + "mstfun.lst");
                var entries = GetListOfEntries(page, lastUpdate);

                Status = "Working...";
                ProgressVisible = true;
                Progress = 0;

                int index = 0;

                Status = "Downloading remote file...";
                string path = await DownloadRemoteFile("mstfun.zip");
                var unpackedEntries = Budziszewski.Net.Zip.UnpackToStrings(path, Encoding.Default);

                foreach (var entry in entries)
                {
                    var e = unpackedEntries.First(x => x.Name == entry.path);
                    Status = "Processing entry... " + e.Name;
                    var list = await Process(e.Contents);
                    Status = "Setting up intervals...";
                    list = SetUpIntervals(list).ToList();
                    Status = "Uploading data from entry..." + e.Name;
                    await db.Upload(list, Database.ReplacementMethod.Instrument);
                    Status = "Updating status..." + e.Name;
                    db.ReportLastUpdate(Source, entry.timestamp);
                    Progress = index++ / (double)entries.Count();
                }

                Progress = 1;
                ProgressVisible = false;
                Status = "Finished";
                ReportInfo?.Invoke(Name + ": Finished");
            }
            catch (DataserverException ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex.InnerException, ex.Command);
            }
            catch (Exception ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex, null);
            }
        }

        private async Task<List<Price>> Process(string text)
        {
            List<Price> list = new List<Price>();

            await Task.Run(() =>
            {
                foreach (var line in text.Split('\n').Skip(1))
                {
                    var data = line.Split(',');
                    if (data.Length < 6) break;

                    Price p = new Price()
                    {
                        Time = DateTime.ParseExact(data[1], "yyyyMMdd", CultureInfo.InvariantCulture),
                        Instrument = "XXPLBF" + data[0],
                        Market = "PL_BOSFUND",
                        Value = StringToDouble(data[5]),
                        Volume = 0,
                        Source = "bossa_mstfun"
                    };
                    list.Add(p);
                }
            });

            return list;
        }
    }
}
