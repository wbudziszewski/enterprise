﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Prices_bossa_mstall : DataserverTask, IDisposable
    {
        private Dictionary<string, string> tickers;

        public Prices_bossa_mstall(Database db, Dictionary<string, string> tickers) : base("Prices (bossa_mstall)", "bossa_mstall", db)
        {
            this.tickers = tickers;
        }

        public override async Task Run()
        {
            try
            {
                ReportInfo?.Invoke(Name + ": Started");
                Status = "Initializing...";
                remotePath = "https://bossa.pl/pub/intraday/mstock/daily/";

                DateTime lastUpdate = db.GetLastUpdate(Source);

                Status = "Preparing list of files... " + remotePath;

                List<(string path, DateTime timestamp)> files = new List<(string path, DateTime timestamp)>();
                await Task.Run(() =>
                {
                    var page = Net.Http.DownloadWebPage(remotePath);
                    files = GetListOfFiles(page, lastUpdate);
                });

                Status = "Initializing...";
                ProgressVisible = true;
                Progress = 0;

                int index = 0;
                string path = "";

                foreach (var file in files)
                {
                    Status = "Downloading remote files... " + file.path;
                    path = await DownloadRemoteFile(file.path);
                    Status = "Processing downloaded file... " + file.path;
                    var list = await Process(path);
                    Status = "Setting up intervals...";
                    list = SetUpIntervals(list).ToList();
                    Status = "Uploading data from file..." + file.path;
                    await db.Upload(list, Database.ReplacementMethod.Time);
                    Status = "Updating status..." + file.path;
                    db.ReportLastUpdate(Source, file.timestamp);
                    Progress = index++ / files.Count();
                }

                Progress = 1;

                ProgressVisible = false;
                Status = "Finished";
                ReportInfo?.Invoke(Name + ": Finished");
            }
            catch (DataserverException ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex.InnerException, ex.Command);
            }
            catch (Exception ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex, null);
            }
        }

        private async Task<List<Price>> Process(string path)
        {
            List<Price> list = new List<Price>();

            await Task.Run(() =>
            {
                using (StreamReader sr = GetReader(path))
                {
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();
                        if (line.StartsWith("<")) continue;

                        var data = line.Split(',');
                        if (data.Length < 6) break;

                        if (!tickers.ContainsKey(data[0])) continue;

                        Price p;
                        if (data.Length <= 7)
                        {
                            p = new Price()
                            {
                                Time = DateTime.ParseExact(data[1], "yyyyMMdd", CultureInfo.InvariantCulture),
                                Instrument = tickers[data[0]],
                                Market = "PL_GPW",
                                Value = StringToDouble(data[5]),
                                Volume = StringToInt64(data[6]),
                                Source = Source
                            };
                        }
                        else
                        {
                            p = new Price()
                            {
                                Time = DateTime.ParseExact(data[2] + data[3], "yyyyMMddHHmmss", CultureInfo.InvariantCulture),
                                Instrument = tickers[data[0]],
                                Market = "PL_GPW",
                                Value = StringToDouble(data[7]),
                                Volume = StringToInt64(data[8]),
                                Source = Source
                            };
                        }
                        list.Add(p);
                    }
                }
            });

            return list;
        }
    }
}
