﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Prices_bossa_mstobl : DataserverTask, IDisposable
    {
        Dictionary<string, string> tickers;

        public Prices_bossa_mstobl(Database db, Dictionary<string, string> tickers) : base("Prices (bossa_mstobl)", "bossa_mstobl", db)
        {
            this.tickers = tickers;
        }

        public override async Task Run()
        {
            try
            {
                ReportInfo?.Invoke(Name + ": Started");
                Status = "Initializing...";
                remotePath = "https://bossa.pl/pub/intraday/mstock/obl/";

                DateTime lastUpdate = db.GetLastUpdate(Source);

                Status = "Preparing list of files... " + remotePath;
                var page = Net.Http.DownloadWebPage(remotePath);
                var files = GetListOfFiles(page, lastUpdate);

                Status = "Working...";
                ProgressVisible = true;
                Progress = 0;

                int index = 0;

                foreach (var file in files)
                {
                    Status = "Downloading remote files... " + file.path;
                    string path = await DownloadRemoteFile(file.path);
                    Status = "Processing downloaded file... " + file.path;
                    var list = await Process(path);
                    Status = "Setting up intervals...";
                    list = SetUpIntervals(list).ToList();
                    Status = "Uploading data from file..." + file.path;
                    await db.Upload(list, Database.ReplacementMethod.Instrument);
                    Status = "Updating status..." + file.path;
                    db.ReportLastUpdate(Source, file.timestamp);
                    Progress = (double)index++ / (double)files.Count();
                }

                Progress = 1;
                ProgressVisible = false;
                Status = "Finished";
                ReportInfo?.Invoke(Name + ": Finished");
            }
            catch (DataserverException ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex.InnerException, ex.Command);
            }
            catch (Exception ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex, null);
            }
        }

        private async Task<List<Price>> Process(string path)
        {
            List<Price> list = new List<Price>();

            await Task.Run(() =>
            {
                using (StreamReader sr = GetReader(path))
                {
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();

                        var data = line.Split(',');
                        if (data.Length < 6) break;

                        if (!tickers.ContainsKey(data[0])) continue;
                        Price p = new Price()
                        {
                            Time = DateTime.ParseExact(data[2] + data[3], "yyyyMMddHHmmss", CultureInfo.InvariantCulture),
                            Instrument = tickers[data[0]],
                            Market = "PL_CATALYST",
                            Value = StringToDouble(data[7]),
                            Volume = StringToInt64(data[8]),
                            Source = Source
                        };
                        list.Add(p);
                    }
                }
            });

            return list;
        }
    }
}
