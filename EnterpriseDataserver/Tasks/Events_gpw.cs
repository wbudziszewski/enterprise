﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Events_gpw : DataserverTask, IDisposable
    {
        private Dictionary<string, string> tickers;

        public Events_gpw(Database db, Dictionary<string, string> tickers) : base("Events (GPW)", "gpwnotoria", db)
        {
            this.tickers = tickers;
        }

        public override async Task Run()
        {
            try
            {
                ReportInfo?.Invoke(Name + ": Started");
                Status = "Initializing...";

                List<KeyValuePair<string, string>> headers = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Origin", "https://gpw.notoria.pl"),
                    new KeyValuePair<string, string>("Accept", "text/html,application/xhtml + xml,application/xml;q=0.9,*/*;q=0.8"),
                    new KeyValuePair<string, string>("Accept-Language", "pl,en-US;q=0.7,en;q=0.3"),
                    new KeyValuePair<string, string>("Accept-Encoding", "gzip, deflate, br"),
                    new KeyValuePair<string, string>("Pragma", "no-cache"),
                    new KeyValuePair<string, string>("Cache-Control", "no-cache"),
                    new KeyValuePair<string, string>("Referer", "https://gpw.notoria.pl/widgets/ta/index.html")
                };

                DateTime lastUpdate = db.GetLastUpdate(Source);

                Progress = 0;
                ProgressVisible = true;

                int index = 0;

                foreach (var ticker in tickers)
                {
                    Status = "Downloading events information... " + ticker.Key;
                    List<MarketEvent> events = new List<MarketEvent>();

                    var page = await Task.Run(() => Budziszewski.Net.Http.DownloadPost("https://www.gpw.pl/", "ajaxindex.php?start=onpTab&action=GPWListaSp&gls_isin=" + ticker.Value + "&format=html", headers, null, true));

                    var html = new HtmlAgilityPack.HtmlDocument();
                    html.LoadHtml(page);
                    var nodes = html.DocumentNode.SelectNodes("/table/tbody/child::tr/child::td");

                    if (nodes == null) continue;


                    for (int i = 0; i < nodes.Count; i += 3)
                    {
                        try
                        {
                            if (nodes[i + 2].InnerText.IndexOf(":") > 0)
                            {
                                events.Add(new MarketEvent()
                                {
                                    Time = DateTime.ParseExact(nodes[i].InnerText.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture),
                                    Instrument = ticker.Value,
                                    Type = nodes[i + 1].InnerText.Trim(),
                                    Value = Double.Parse(nodes[i + 2].InnerText.Split(':')[0].Trim(), CultureInfo.InvariantCulture),
                                    Value2 = Double.Parse(nodes[i + 2].InnerText.Split(':')[1].Trim(), CultureInfo.InvariantCulture),
                                    Source = Source
                                });
                            }
                            else
                            {
                                events.Add(new MarketEvent()
                                {
                                    Time = DateTime.ParseExact(nodes[i].InnerText.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture),
                                    Instrument = ticker.Value,
                                    Type = nodes[i + 1].InnerText.Trim(),
                                    Value = Double.Parse(nodes[i + 2].InnerText.Trim(), CultureInfo.InvariantCulture),
                                    Value2 = nodes[i + 1].InnerText.Trim().ToLower() == "prawo poboru" ? Double.Parse(nodes[i + 2].InnerText.Trim(), CultureInfo.InvariantCulture) : 0,
                                    Source = Source
                                });
                            }
                        }
                        catch
                        {
                            ReportInfo?.Invoke("Event not parsed: " + ticker.Value + "|" + nodes[i].InnerText.Trim() + "|" + nodes[i + 1].InnerText.Trim() + "|" + nodes[i + 2].InnerText.Trim() + ".");
                        }
                    }


                    await db.Upload(events, Database.ReplacementMethod.Unspecified);

                    Progress = index++ / (double)tickers.Count;
                }

                Progress = 1;

                ProgressVisible = false;
                Status = "Finished";
                ReportInfo?.Invoke(Name + ": Finished");
            }
            catch (DataserverException ex)
            {
                throw new DataserverException("Error importing events from " + Source + " source.", this, ex.InnerException, ex.Command);
            }
            catch (Exception ex)
            {
                throw new DataserverException("Error importing events from " + Source + " source.", this, ex, null);
            }
        }
    }
}

