﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Prices_bossa_mstall_endofday : DataserverTask, IDisposable
    {
        private Dictionary<string, string> tickers;

        public Prices_bossa_mstall_endofday(Database db, Dictionary<string, string> tickers) : base("Prices (bossa_mstall_endofday)", "bossa_mstall", db)
        {
            this.tickers = tickers;
        }

        public override async Task Run()
        {
            try
            {
                ReportInfo?.Invoke(Name + ": Started");
                Status = "Initializing...";
                remotePath = "https://bossa.pl/pub/metastock/mstock/";

                ProgressVisible = true;
                Progress = 0;
                int index = 0;
                string path = "";

                Status = "Downloading end of day files... ";
                path = await DownloadRemoteFile("mstall.zip");
                Status = "Processing downloaded file... ";
                var files = Budziszewski.Net.Zip.UnpackToFiles(path, GetTempLocalFilepath("EnterpriseDataserver"));
                foreach (var file in files)
                {
                    Status = "Processing..." + Path.GetFileName(file);
                    var list = await Process(file);
                    list = SetUpIntervals(list).ToList();
                    await db.Upload(list, Database.ReplacementMethod.IfMissing);
                    Progress = index++ / (double)files.Count();
                }

                Progress = 1;
                ProgressVisible = false;
                Status = "Finished";
                ReportInfo?.Invoke(Name + ": Finished");
            }
            catch (DataserverException ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex.InnerException, ex.Command);
            }
            catch (Exception ex)
            {
                throw new DataserverException("Error importing price from " + Source + " source.", this, ex, null);
            }
        }

        private async Task<List<Price>> Process(string path)
        {
            List<Price> list = new List<Price>();

            await Task.Run(() =>
            {
                using (StreamReader sr = GetReader(path))
                {
                    while (!sr.EndOfStream)
                    {
                        var line = sr.ReadLine();
                        if (line.StartsWith("<")) continue;

                        var data = line.Split(',');
                        if (data.Length < 6) break;

                        if (!tickers.ContainsKey(data[0])) continue;

                        Price p;
                        if (data.Length <= 7)
                        {
                            p = new Price()
                            {
                                Time = DateTime.ParseExact(data[1], "yyyyMMdd", CultureInfo.InvariantCulture),
                                Instrument = tickers[data[0]],
                                Market = "PL_GPW",
                                Value = StringToDouble(data[5]),
                                Volume = StringToInt64(data[6]),
                                Source = Source
                            };
                        }
                        else
                        {
                            p = new Price()
                            {
                                Time = DateTime.ParseExact(data[2] + data[3], "yyyyMMddHHmmss", CultureInfo.InvariantCulture),
                                Instrument = tickers[data[0]],
                                Market = "PL_GPW",
                                Value = StringToDouble(data[7]),
                                Volume = StringToInt64(data[8]),
                                Source = Source
                            };
                        }
                        list.Add(p);
                    }
                }
            });

            return list;
        }
    }
}
