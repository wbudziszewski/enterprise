﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Budziszewski.Enterprise.Dataserver
{
    public static class Import
    {
        private static Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog();

        public static List<Instrument> Load(string path)
        {
            List<Instrument> output = new List<Instrument>();
            try
            {
                using (StreamReader sr = new StreamReader(new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    sr.ReadLine();
                    while (!sr.EndOfStream)
                    {
                        string[] s = sr.ReadLine().Split(';');
                        output.Add(new Instrument()
                        {
                            ID = s[0],
                            Ticker = s[1],
                            LongTicker = s[2],
                            Name = s[3],
                            Type = s[4]
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading instruments from external file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return output;
        }

        public static void Save(IEnumerable<Instrument> instruments)
        {
            if (dialog.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(new FileStream(dialog.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)))
                {
                    try
                    {
                        sw.Write("InstrumentID;Ticker;LongTicker;Name;Source");
                        foreach (var item in instruments)
                        {
                            sw.WriteLine();
                            sw.Write(item.ID);
                            sw.Write(";");
                            sw.Write(item.Ticker);
                            sw.Write(";");
                            sw.Write(item.LongTicker);
                            sw.Write(";");
                            sw.Write(item.Name);
                            sw.Write(";");
                            sw.Write(";");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error writing imported entries into an external file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        public static async Task ImportEquityInfostrefa()
        {
            List<Instrument> instruments = new List<Instrument>();

            OnProgress("Importing equity from infostrefa.pl... 0%");
            string path = "http://infostrefa.com/infostrefa/pl/spolki";

            var html = new HtmlAgilityPack.HtmlDocument();
            await Task.Run(() =>
            {
                html.LoadHtml(Net.Http.DownloadWebPage(path));
                var table = html.DocumentNode.SelectNodes("/html/body/div[1]/div[2]/div/div[1]/div[3]/div/div[2]/div/table/tbody").FirstOrDefault();

                if (table != null)
                {
                    int index = 0;
                    int count = table.ChildNodes.Count;
                    foreach (var row in table.ChildNodes)
                    {
                        OnProgress("Importing equity from infostrefa.pl... " + (int)Math.Round((index++ / (double)count) * 100) + "%");
                        if (row.ChildNodes.Count < 8) continue;
                        if (String.IsNullOrEmpty(row.ChildNodes[7].InnerText)) continue;

                        Instrument instrument = new Instrument()
                        {
                            ID = row.ChildNodes[7].InnerText.Trim(),
                            Ticker = row.ChildNodes[5].InnerText.Trim(),
                            LongTicker = row.ChildNodes[3].InnerText.Trim(),
                            Name = row.ChildNodes[1].InnerText.Trim(),
                            Type = "equity"
                        };
                        instruments.Add(instrument);
                    }
                }
            });

            OnProgress("Importing equity from infostrefa.pl... 100%");

            Save(instruments);
        }

        public static async Task ImportBondsObligacje()
        {
            List<Instrument> instruments = new List<Instrument>();

            OnProgress("Importing bonds from obligacje.pl... 0%");
            string path = "https://obligacje.pl/pl/narzedzia/wyszukiwarka-obligacji-notowanych,s_archiwum-all";

            var html = new HtmlAgilityPack.HtmlDocument();
            await Task.Run(() =>
            {
                html.LoadHtml(Net.Http.DownloadWebPage(path));
                var node = html.DocumentNode.SelectSingleNode("//div[@id='Main']");
                node = node.ChildNodes[5].SelectNodes("//a[@class='icon-arrow-right']").Last();
                if (node == null) throw new Exception("Cannot parse " + path);

                string pagesStr = node.GetAttributeValue("href", "");
                int pages = int.Parse(pagesStr.Substring(pagesStr.LastIndexOf('-') + 1));

                for (int i = 1; i < pages; i++)
                {
                    path = "https://obligacje.pl/pl/narzedzia/wyszukiwarka-obligacji-notowanych,s_archiwum-all,s-" + i;
                    html = new HtmlAgilityPack.HtmlDocument();
                    html.LoadHtml(Net.Http.DownloadWebPage(path));
                    var nodes = html.DocumentNode.SelectNodes("//div[@id='Main'][1]/table[1]/tr");
                    if (nodes == null) throw new Exception("Cannot parse " + path);

                    int index = 0;
                    int count = nodes.Count;
                    foreach (var n in nodes)
                    {
                        OnProgress("Importing bonds from obligacje.pl, page " + i + "/" + pages + "... " + (int)Math.Round((index++ / (double)count) * 100) + "%");

                        if (n.SelectNodes("td") == null) continue;
                        var name = n.SelectSingleNode("td[2]/span/a").InnerText.Trim();

                        string isinSrc = Net.Http.DownloadWebPage("https://obligacje.pl/pl/obligacja/" + name);
                        var isinPos = isinSrc.IndexOf(@"ISIN:");
                        if (isinPos < 0)
                        {
                            continue;
                        }

                        isinPos += 5;
                        while (isinSrc[isinPos] == ' ') isinPos++;

                        var isin = isinSrc.Substring(isinPos, 12);
                        Instrument instrument = new Instrument()
                        {
                            ID = isin,
                            Ticker = name,
                            LongTicker = name,
                            Name = name,
                            Type = "bond"
                        };
                        if (String.IsNullOrEmpty(instrument.ID)) continue;
                        if (instruments.Exists(x => x.ID == instrument.ID)) continue;
                        instruments.Add(instrument);
                    }
                }
            });

            OnProgress("Importing bonds from obligacje.pl... 100%");

            Save(instruments);
        }

        public static async Task ImportFundsBossafund()
        {
            List<Instrument> instruments = new List<Instrument>();

            OnProgress("Importing funds from bossafund.pl... 0%");
            string path = "http://bossa.pl/pub/fundinwest/mstock/mstfun.lst";

            await Task.Run(() =>
            {
                string fundsSrc = Net.Http.DownloadWebPage(path);

                if (fundsSrc != "")
                {
                    var funds = fundsSrc.Split('\n').Skip(3);
                    int index = 0;
                    int count = funds.Count();
                    foreach (var row in funds)
                    {
                        OnProgress("Importing funds from bossafund.pl... " + (int)Math.Round((index++ / (double)count) * 100) + "%");
                        string[] fundInfo = row.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (fundInfo.Length < 6) continue;
                        string fundName = "";
                        for (int i = 5; i < fundInfo.Length; i++)
                        {
                            fundName += fundInfo[i] + " ";
                        }
                        Instrument instrument = new Instrument()
                        {
                            ID = "XXPLBF" + fundInfo[4].Trim().Remove(6, 4),
                            Ticker = fundInfo[4].Trim().Remove(6, 4),
                            LongTicker = fundInfo[4].Trim().Remove(6, 4),
                            Name = fundName.Trim(),
                            Type = "fund"
                        };
                        if (String.IsNullOrEmpty(instrument.ID)) continue;
                        instruments.Add(instrument);
                    }
                }
            });

            OnProgress("Importing funds from bossafund.pl... 100%");

            Save(instruments);
        }

        public static async Task ImportBondsCatalyst()
        {
            // WARNING: Does not import historical entries!!

            List<Instrument> instruments = new List<Instrument>();

            OnProgress("Importing bonds from gpwcatalyst.pl... 0%");
            string path = "https://gpwcatalyst.pl/o-instrumentach-instrumenty-notowane";

            var html = new HtmlAgilityPack.HtmlDocument();
            await Task.Run(() =>
            {
                html = new HtmlAgilityPack.HtmlDocument();
                html.LoadHtml(Net.Http.DownloadWebPage(path));

                var currencies = new string[2] { "PLN", "EUR" };

                foreach (var currency in currencies)
                {
                    var table = html.DocumentNode.SelectNodes(@"//*[@id=""bs-instrument-" + currency + @"""]").FirstOrDefault();
                    if (table != null)
                    {
                        var rows = table.ChildNodes.Where(x => x.Name == "tr").Skip(1);
                        int index = 0;
                        int count = rows.Count();
                        foreach (var row in rows)
                        {
                            OnProgress("Importing " + currency + " bonds from gpwcatalyst.pl... " + (int)Math.Round((index++ / (double)count) * 100) + "%");
                            if (row.ChildNodes.Count < 6) continue;
                            var name = row.ChildNodes[5].ChildNodes["a"].InnerText.Trim();
                            var isinSrc = Net.Http.DownloadWebPage("https://gpwcatalyst.pl/o-instrumentach-instrument?nazwa=" + name);
                            var isinPos = isinSrc.IndexOf(@"id=""isin"" value=""");
                            if (isinPos == 0)
                            {
                                throw new Exception("Cannot find ISIN for bond named: " + name);
                            }
                            else
                            {
                                isinPos += @"id=""isin"" value=""".Length;
                            }

                            var isin = isinSrc.Substring(isinPos, 12);
                            if (String.IsNullOrEmpty(isin)) continue;
                            Instrument instrument = new Instrument()
                            {
                                ID = isin,
                                Ticker = name,
                                LongTicker = name,
                                Name = name,
                                Type = "bond"
                            };
                            instruments.Add(instrument);
                        }
                    }
                }
            });

            OnProgress("Importing bonds from gpwcatalyst.pl... 100%");

            Save(instruments);
        }

        #region Event

        public class ProgressEventArgs : EventArgs
        {
            public string Message { get; private set; }

            public ProgressEventArgs(string message)
            {
                Message = message;
            }
        }

        public static event ProgressHandler Progress;
        public delegate void ProgressHandler(ProgressEventArgs e);

        private static void OnProgress(string message)
        {
            Progress?.Invoke(new ProgressEventArgs(message));
        }

        #endregion
    }
}
