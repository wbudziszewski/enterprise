﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public abstract class DataserverTask : INotifyPropertyChanged, IDisposable
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }
        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        private bool progressVisible;
        public System.Windows.Visibility ProgressVisibility
        {
            get { return progressVisible ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed; }
        }
        public bool ProgressVisible
        {
            set { progressVisible = value; OnPropertyChanged("ProgressVisibility"); }
        }

        private double progress;
        public double Progress
        {
            get { return progress; }
            set { progress = value; OnPropertyChanged("Progress"); }
        }

        public bool IsEnabled { get; set; }
        public string Source { get; set; }

        protected string remotePath = "";

        protected Database db;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public DataserverTask(string name, string source, Database db)
        {
            Name = name;
            Status = "Inactive";
            ProgressVisible = false;
            Progress = 0;
            IsEnabled = true;
            Source = source;
            this.db = db;
        }

        public abstract Task Run();

        protected IEnumerable<Price> SetUpIntervals(List<Price> list)
        {
            if (list.Count == 0) yield break;

            for (int i = 0; i < list.Count - 1; i++)
            {
                if (list[i + 1].Instrument != list[i].Instrument) list[i].Interval = PriceInterval.Month;
                else if (list[i + 1].Time.Date.Year != list[i].Time.Date.Year || list[i + 1].Time.Date.Month != list[i].Time.Date.Month) list[i].Interval = PriceInterval.Month;
                else if (list[i + 1].Time.Date != list[i].Time.Date) list[i].Interval = PriceInterval.Day;
                else if (list[i + 1].Time.Hour != list[i].Time.Hour) list[i].Interval = PriceInterval.Hour;
                else if (list[i + 1].Time.Minute / 15 != list[i].Time.Minute / 15) list[i].Interval = PriceInterval.Quarter;
                else if (list[i + 1].Time.Minute != list[i].Time.Minute) list[i].Interval = PriceInterval.Minute;
                else list[i].Interval = PriceInterval.Second;
            }
            list[list.Count - 1].Interval = PriceInterval.Month;

            if (Granularity == PriceInterval.Second)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    yield return list[i];
                }
            }
            else
            {
                for (int i = 0; i < list.Count - 1; i++)
                {
                    if (list[i].Interval > Granularity && list[i + 1].Instrument == list[i].Instrument)
                    {
                        list[i + 1].Volume += list[i].Volume;
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Interval <= Granularity)
                    {
                        switch (Granularity)
                        {
                            case PriceInterval.Month: list[i].Time = list[i].Time.Date; break;
                            case PriceInterval.Day: list[i].Time = list[i].Time.Date; break;
                            case PriceInterval.Hour: list[i].Time = new DateTime(list[i].Time.Year, list[i].Time.Month, list[i].Time.Day, list[i].Time.Hour, 0, 0); break;
                            case PriceInterval.Quarter: list[i].Time = new DateTime(list[i].Time.Year, list[i].Time.Month, list[i].Time.Day, list[i].Time.Hour, list[i].Time.Minute / 15, 0); break;
                            case PriceInterval.Minute: list[i].Time = new DateTime(list[i].Time.Year, list[i].Time.Month, list[i].Time.Day, list[i].Time.Hour, list[i].Time.Minute, 0); break;
                            case PriceInterval.Second: list[i].Time = new DateTime(list[i].Time.Year, list[i].Time.Month, list[i].Time.Day, list[i].Time.Hour, list[i].Time.Minute, list[i].Time.Second); break;
                            default: break;
                        }
                        yield return list[i];
                    }
                }
            }
        }

        protected List<(string path, DateTime timestamp)> GetListOfFiles(string page, DateTime lastUpdate)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page);

            var nodes = doc.DocumentNode.SelectNodes("/html/body/pre/a");

            List<(string path, DateTime timestamp)> output = new List<(string path, DateTime timestamp)>();

            if (nodes == null) return output;

            foreach (var node in nodes)
            {
                if (node.InnerText.Trim() == "Parent Directory") continue;
                if (node.InnerText.IndexOf("tick") > -1) continue;
                string dtstr = node.NextSibling.InnerText.Trim().Substring(0, 16);
                DateTime dt = DateTime.ParseExact(dtstr, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                if (dt < lastUpdate) continue;
                output.Add((node.GetAttributeValue("href", ""), dt));
            }

            return output.OrderBy(x => x.timestamp).ToList();
        }

        protected List<(string path, DateTime timestamp)> GetListOfEntries(string text, DateTime lastUpdate)
        {
            List<(string path, DateTime timestamp)> output = new List<(string path, DateTime timestamp)>();

            foreach (var line in text.Split('\n').Skip(3))
            {
                var data = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (data.Length < 5) continue;
                string dtstr = data[0] + " " + data[1];
                DateTime dt = DateTime.ParseExact(dtstr, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
                if (dt < lastUpdate) continue;
                output.Add((data[4], dt));
            }

            return output.OrderBy(x => x.timestamp).ToList();
        }

        protected async Task<string> DownloadRemoteFile(string path)
        {
            string localPath = GetTempLocalFilepath("EnterpriseDataserver");
            var dl = new Net.Http.FileDownload(remotePath + path, localPath);
            await dl.DownloadAsync();
            return localPath;
        }

        protected string GetTempLocalFilepath(string dirname)
        {
            return Path.Combine(Path.GetTempPath(), dirname, Guid.NewGuid().ToString());
        }

        protected StreamReader GetReader(string path)
        {
            bool isZip = false;
            using (BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                isZip = br.BaseStream.Length >= 2 && br.ReadChar() == 'P' && br.ReadChar() == 'K';
            }

            if (isZip)
            {
                ZipArchive zip = new ZipArchive(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                return new StreamReader(zip.Entries.First().Open());
            }
            else
            {
                return new StreamReader(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            }
        }

        protected IEnumerable<StreamReader> GetReaders(string path)
        {
            bool isZip = false;
            using (BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                isZip = br.BaseStream.Length >= 2 && br.ReadChar() == 'P' && br.ReadChar() == 'K';
            }

            if (isZip)
            {
                ZipArchive zip = new ZipArchive(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
                foreach (var entry in zip.Entries)
                {
                    yield return new StreamReader(entry.Open());
                }
            }
            else
            {
                yield return new StreamReader(File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            }
        }

        public virtual void Dispose()
        {
            Status = "Finished";
        }

        protected static double StringToDouble(string input)
        {
            Double.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out double output);
            return output;
        }

        protected static long StringToInt64(string input)
        {
            Int64.TryParse(input, NumberStyles.Any, CultureInfo.InvariantCulture, out long output);
            return output;
        }

        protected static long DateTimeToLong(string input, string format)
        {
            DateTime.TryParseExact(input, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime output);
            return output.Ticks;
        }

        protected static DateTime JavaTicksToDateTime(long input)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(input).ToLocalTime();
        }

        public ReportInfoHandler ReportInfo { get; set; }
        public PriceInterval Granularity { get; set; }

        public delegate void ReportInfoHandler(string message);
    }

    public class DataserverException : Exception
    {
        public DataserverTask Task { get; set; }

        public SqlCommand Command { get; set; }

        public DataserverException(string message, DataserverTask task, Exception ex, SqlCommand command) : base(ex == null ? message : message + ": " + ex.Message, ex)
        {
            Task = task;
            Command = command;
        }
    }
}