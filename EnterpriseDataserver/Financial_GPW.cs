﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Budziszewski.Enterprise.Dataserver
{
    //public class Financial_GPW : DataserverTask, IDisposable
    //{
    //    ConcurrentDictionary<string, Timestamp> timestamps;
    //    ConcurrentBag<Subtask> tasks = new ConcurrentBag<Subtask>();

    //    public Financial_GPW(string hostname, NetworkCredential credentials) : base("Financial_GPW", hostname, credentials)
    //    {
    //        source = 0;
    //        this.localUploadTempPath = Path.Combine(Path.GetTempPath(), "EnterpriseDataserver", "Upload", "Financial_GPW");

    //        if (Directory.Exists(localUploadTempPath))
    //            Directory.Delete(localUploadTempPath, true);
    //        Directory.CreateDirectory(localUploadTempPath);
    //    }

    //    public override async void Run()
    //    {
    //        try
    //        {
    //            ReportInfo?.Invoke("Financial_GPW: Started");
    //            Status = "Initializing...";
    //            remoteUploadPath = "www/dataserver/financial";

    //            Net.Sftp ftp = new Net.Sftp(hostname, credentials);
    //            if (!ftp.DirectoryExists(remoteUploadPath))
    //            {
    //                ftp.CreateDirectory(remoteUploadPath);
    //            }

    //            // Timestamps denote last available dividend (date), by ticker, in Ticks format
    //            Status = "Downloading timestamps file... " + remoteUploadPath + "/CONTENTS";
    //            timestamps = GetTimestamps(remoteUploadPath + "/CONTENTS", credentials);

    //            // Check the timestamp of remote data
    //            long remoteFileTimestamp = DateTime.Now.Date.Ticks;
    //            var currentTimestamps = timestamps.Where(x => x.Value.Source == source);
    //            if (currentTimestamps.Count() == 0 || remoteFileTimestamp > currentTimestamps.Max(x => x.Value.SourceDataTimestamp)) // file is newer
    //            {

    //                // Get list of tickers
    //                Status = "Getting list of tickers...";
    //                var tickers = await GetListOfISINs("gpw.notoria.pl", "https://gpw.notoria.pl/widgets/ta/symbols.php");

    //                Progress = 0;
    //                ProgressVisible = true;
    //                List<FinancialReport> reports = new List<FinancialReport>();
    //                for (int i = 0; i < tickers.Count; i++)
    //                {
    //                    Status = "Downloading ESPI information... " + tickers[i].Item1;
    //                    Progress = (float)i / (float)tickers.Count;
    //                    var events = await GetESPI(tickers[i].Item1, tickers[i].Item2);

    //                    foreach (var evt in events)
    //                    {
    //                        var report = new FinancialReport(evt);
    //                        await report.Download();
    //                        reports.Add(report);
    //                    }
    //                }                    
    //            }

    //        }
    //        catch (Exception ex)
    //        {
    //            ReportInfo?.Invoke(ex.Message);
    //        }
    //    }

    //    async Task<List<ESPI>> GetESPI(string ticker, string isin)
    //    {
    //        List<KeyValuePair<string, string>> headers = new List<KeyValuePair<string, string>>
    //        {
    //            new KeyValuePair<string, string>("Origin", "www.gpw.pl"),
    //            new KeyValuePair<string, string>("Accept-Language", "pl,en-US;q=0.7,en;q=0.3"),
    //            new KeyValuePair<string, string>("Accept-Encoding", "gzip, deflate, br"),
    //            new KeyValuePair<string, string>("Referer", "https://www.gpw.pl/komunikaty/")
    //        };

    //        List<ESPI> output = new List<ESPI>();
    //        try
    //        {
    //            var page = await Task.Run(() => Budziszewski.Net.Http.DownloadPost("https://www.gpw.pl/",
    //                "ajaxindex.php?action=GPWEspiReportUnion&start=ajaxSearch&page=komunikaty&format=html&lang=PL&letter=&offset=0&limit=4096&categoryRaports%5B%5D=ESPI&typeRaports%5B%5D=P&typeRaports%5B%5D=Q&typeRaports%5B%5D=O&typeRaports%5B%5D=R&search-xs=&searchText=" + isin + "&date=", headers, null, true));

    //            var html = new HtmlAgilityPack.HtmlDocument();
    //            html.LoadHtml(page);
    //            var nodes = html.DocumentNode.SelectNodes("//li");
    //            if (nodes == null) return new List<ESPI>();

    //            for (int i = 0; i < nodes.Count; i++)
    //            {
    //                DateTime date = DateTime.ParseExact(nodes[i].ChildNodes.FindFirst("span").InnerText.Trim().Substring(0, 19), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
    //                output.Add(new ESPI(ticker, date, nodes[i].ChildNodes.FindFirst("strong").ChildNodes.FindFirst("a").GetAttributeValue("href", "").Trim()));
    //            }

    //            return output;
    //        }
    //        catch
    //        {
    //            return new List<ESPI>();
    //        }
    //    }

    //    //async Task<List<ESPI>> GetESPI(string ticker, string isin)
    //    //{
    //    //    List<KeyValuePair<string, string>> headers = new List<KeyValuePair<string, string>>
    //    //    {
    //    //        new KeyValuePair<string, string>("Origin", "www.gpw.pl"),
    //    //        new KeyValuePair<string, string>("Accept-Language", "pl,en-US;q=0.7,en;q=0.3"),
    //    //        new KeyValuePair<string, string>("Accept-Encoding", "gzip, deflate, br"),
    //    //        new KeyValuePair<string, string>("Referer", "https://www.gpw.pl/spolka?isin=PL11BTS00015")
    //    //    };

    //    //    List<ESPI> output = new List<ESPI>();
    //    //    try
    //    //    {
    //    //        DateTime overallLastEspiDate = new DateTime();
    //    //        DateTime lastEspiDate = new DateTime();
    //    //        int offset = 0;

    //    //        do
    //    //        {
    //    //            overallLastEspiDate = lastEspiDate;
    //    //            List<ESPI> temp = new List<ESPI>();

    //    //            var page = await Task.Run(() => Budziszewski.Net.Http.DownloadPost("https://www.gpw.pl/", "ajaxindex.php?start=reportsTab&action=GPWListaSp&gls_isin=" + isin + "&format=html&offset=" + offset, headers, null, true));

    //    //            var html = new HtmlAgilityPack.HtmlDocument();
    //    //            html.LoadHtml(page);
    //    //            var nodes = html.DocumentNode.SelectNodes("/table/tbody/child::tr/child::td");

    //    //            for (int i = 0; i < nodes.Count; i += 2)
    //    //            {
    //    //                DateTime date = DateTime.ParseExact(nodes[i].InnerText.Trim(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
    //    //                temp.Add(new ESPI(ticker, date, nodes[i + 1].InnerHtml.Trim()));
    //    //                lastEspiDate = date;
    //    //            }

    //    //            if (overallLastEspiDate != lastEspiDate)
    //    //            {
    //    //                output.AddRange(temp);
    //    //            }

    //    //            offset += 5;

    //    //        } while (overallLastEspiDate != lastEspiDate);

    //    //        return output;
    //    //    }
    //    //    catch
    //    //    {
    //    //        return new List<ESPI>();
    //    //    }
    //    //}

    //    public override void Dispose()
    //    {
    //        try
    //        {
    //            Directory.Delete(localUploadTempPath, true);
    //        }
    //        catch (Exception ex)
    //        {
    //            ReportInfo?.Invoke(ex.Message);
    //        }
    //        Status = "Finalized";
    //    }
    //}

    //class ESPI
    //{
    //    public string Ticker;
    //    public DateTime Time;
    //    public string Contents;

    //    public ESPI(string ticker, DateTime time, string contents)
    //    {
    //        Ticker = ticker;
    //        Time = time;
    //        Contents = contents.Trim();
    //    }
    //}

    //class FinancialReport
    //{
    //    public string Ticker;
    //    public string Address;
    //    public int Frequency;
    //    public bool Consolidated;

    //    public decimal SalesIncome;
    //    public decimal NetSalesIncome;
    //    public decimal IncomeBeforeTaxes;
    //    public decimal NetIncome;
    //    public decimal NetIncomeDominatingEntity;
    //    public decimal NetIncomeNonControllingInterest;
    //    public decimal OtherTotalNetRevenue;
    //    public decimal OverallTotalRevenue;
    //    public decimal OverallTotalRevenueDominatingEntity;
    //    public decimal OverallTotalRevenueNonControllingInterest;
    //    public decimal Shares;
    //    public decimal NetIncomePerOrdinaryShareDominatingEntity;
    //    public decimal NetOperatingCashflow;
    //    public decimal NetInvestmentCashflow;
    //    public decimal NetFinancialCashflow;
    //    public decimal NetCashflow;
    //    public decimal FixedAssets;
    //    public decimal CurrentAssets;
    //    public decimal TotalAssets;
    //    public decimal LongTermLiabilities;
    //    public decimal ShortTermLiabilities;
    //    public decimal EquityCapital;
    //    public decimal EquityCapitalDominatingEntity;
    //    public decimal EquityCapitalNonControllingInterest;

    //    public FinancialReport(ESPI espi)
    //    {
    //        Ticker = espi.Ticker;
    //        Address = espi.Contents;
    //    }

    //    public async Task Download()
    //    {
    //        if (Address == "") return;
    //        CultureInfo ci = CultureInfo.GetCultureInfo("pl-PL");

    //        var page = await Task.Run(() => Budziszewski.Net.Http.DownloadWebPage("http://www.gpw.pl/" + Address));

    //        var html = new HtmlAgilityPack.HtmlDocument();
    //        html.LoadHtml(page);
    //        var nodes = html.DocumentNode.SelectNodes("/html/body/section[2]/div[2]/div/div/div/div/h4");

    //        foreach (var titleNode in nodes)
    //        {
    //            if (titleNode.InnerText.Trim() == "Nazwa arkusza: STRONA TYTUŁOWA")
    //            {
    //                foreach (var n in titleNode.SelectNodes("//td"))
    //                {
    //                    if (n.InnerText.Trim() == "Skonsolidowany raport kwartalny QSr") { Consolidated = true; Frequency = 4; }
    //                    if (n.InnerText.Trim() == "Skonsolidowany raport półroczny PSr") { Consolidated = true; Frequency = 2; }
    //                    if (n.InnerText.Trim() == "Skonsolidowany raport roczny RS") { Consolidated = true; Frequency = 1; }
    //                    if (n.InnerText.Trim() == "Raport kwartalny Q") { Consolidated = false; Frequency = 4; }
    //                    if (n.InnerText.Trim() == "Raport półroczny P") { Consolidated = false; Frequency = 2; }
    //                    if (n.InnerText.Trim() == "Raport roczny R") { Consolidated = false; Frequency = 1; }
    //                }
    //            }
    //            if (titleNode.InnerText.Trim() == "Nazwa arkusza: WYBRANE DANE FINANSOWE")
    //            {
    //                if (Frequency == 0) break;

    //                int unit = 1;

    //                foreach (var n in titleNode.NextSibling.NextSibling.SelectNodes("//tr[@class='nTekst']"))
    //                {
    //                    if (n.ChildNodes.Count < 4) continue;
    //                    var header = n.ChildNodes[3].InnerText.Trim();
    //                    if (header == "") continue;
    //                    if (header.IndexOf('.') >= 0) header = header.Substring(header.IndexOf('.') + 1).Trim();

    //                    var text = n.ChildNodes[5].InnerText.Trim();
    //                    Decimal.TryParse(text, out decimal value);

    //                    // Set unit
    //                    if (text.ToLower() == "w mln pln" || text.ToLower() == "mln pln" || text.ToLower() == "w mln zł" || text.ToLower() == "mln zł") unit = 1000000;
    //                    if (text.ToLower() == "w tys. pln" || text.ToLower() == "tys. pln" || text.ToLower() == "w tys. zł" || text.ToLower() == "tys. zł") unit = 1000;

    //                    switch (header.ToLower())
    //                    {
    //                        case "przychody ze sprzedaży":
    //                            SalesIncome = value * unit; break;
    //                        case "zysk netto ze sprzedaży":
    //                            NetSalesIncome = value * unit; break;
    //                        case "zysk przed opodatkowaniem":
    //                            IncomeBeforeTaxes = value * unit; break;
    //                        case "zysk netto":
    //                            NetIncome = value * unit; break;
    //                        case "zysk netto przypadający akcjonariuszom jednostki dominującej ":
    //                            NetIncomeDominatingEntity = value * unit; break;
    //                        case "zysk netto przypadający na udziały niekontrolujące":
    //                            NetIncomeNonControllingInterest = value * unit; break;
    //                        case "pozostałe całkowite dochody netto":
    //                            OtherTotalNetRevenue = value * unit; break;
    //                        case "łączne całkowite dochody":
    //                            OverallTotalRevenue = value * unit; break;
    //                        case "łączne całkowite dochody przypadające akcjonariuszom jednostki dominującej":
    //                            OverallTotalRevenueDominatingEntity = value * unit; break;
    //                        case "łączne całkowite dochody przypadające na udziały niekontrolujące":
    //                            OverallTotalRevenueNonControllingInterest = value * unit; break;
    //                        case "ilość akcji _w mln szt._":
    //                            Shares = value * 1000000; break;
    //                        case "zysk netto na jedną akcję zwykłą przypadający akcjonariuszom jednostki dominującej ":
    //                            NetIncomePerOrdinaryShareDominatingEntity = value * unit; break;
    //                        case "przepływy pieniężne netto z działalności operacyjnej":
    //                            NetOperatingCashflow = value * unit; break;
    //                        case "przepływy pieniężne netto z działalności inwestycyjnej":
    //                            NetInvestmentCashflow = value * unit; break;
    //                        case "przepływy pieniężne netto z działalności finansowej":
    //                            NetFinancialCashflow = value * unit; break;
    //                        case "przepływy pieniężne netto razem":
    //                            NetCashflow = value * unit; break;
    //                        case "aktywa trwałe":
    //                            FixedAssets = value * unit; break;
    //                        case "aktywa obrotowe":
    //                            CurrentAssets = value * unit; break;
    //                        case "aktywa razem":
    //                            TotalAssets = value * unit; break;
    //                        case "zobowiązania długoterminowe":
    //                            LongTermLiabilities = value * unit; break;
    //                        case "zobowiązania krótkoterminowe": ShortTermLiabilities = value * unit; break;
    //                        case "kapitał własny": EquityCapital = value * unit; break;
    //                        case "kapitał przypadający akcjonariuszom Jednostki Dominującej": EquityCapitalDominatingEntity = value * unit; break;
    //                        case "kapitał przypadający na udziały niekontrolujące": EquityCapitalNonControllingInterest = value * unit; break;
    //                        default:
    //                            Debug.Write("def");
    //                            if (unit == -1)
    //                            {
    //                                FixedAssets = 1;
    //                            }
    //                            break;
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
}

