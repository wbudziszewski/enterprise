﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Database : IDisposable
    {
        public string Name { get; set; } = "Enterprise";

        private SqlConnection connection;
        private SqlTransaction transaction;
        private Dictionary<string, string> connectionDetails = new Dictionary<string, string>();

        public Database(string connectionString)
        {
            ParseConnectionString(connectionString);
            if (connectionDetails.ContainsKey("database")) Name = connectionDetails["database"];

            if (connectionDetails.ContainsKey("attachdbfilename") && !File.Exists(connectionDetails["attachdbfilename"]))
            {
                Create();
            }
            else
            {
                Connect(connectionString);
            }
        }

        private void ParseConnectionString(string connectionString)
        {
            connectionDetails = new Dictionary<string, string>();
            var elements = connectionString.Split(';');
            foreach (var item in elements)
            {
                if (item.IndexOf('=') == -1) continue;
                if (String.IsNullOrEmpty(item)) continue;
                connectionDetails.Add(item.Substring(0, item.IndexOf('=')).Trim().Trim('"', '\'').Trim().ToLowerInvariant(), item.Substring(item.IndexOf('=') + 1).Trim().Trim('"', '\'').Trim());
            }
        }

        public async Task Clean()
        {
            await Task.Run(() => Create());
        }

        private void Create()
        {
            string mdfPath = Path.Combine(Path.GetDirectoryName(connectionDetails["attachdbfilename"]), Path.GetFileNameWithoutExtension(connectionDetails["attachdbfilename"]) + ".mdf");
            string ldfPath = Path.Combine(Path.GetDirectoryName(connectionDetails["attachdbfilename"]), Path.GetFileNameWithoutExtension(connectionDetails["attachdbfilename"]) + "_log.ldf");

            string connectionString = "";
            foreach (var item in connectionDetails)
            {
                if (item.Key == "attachdbfilename") continue;
                if (item.Key == "database") continue;
                connectionString += item.Key + "=" + item.Value + ";";
            }
            connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand command;

            command = connection.CreateCommand();
            connection.ChangeDatabase("master");
            command.CommandText = "ALTER DATABASE[" + Name + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
            command.ExecuteNonQuery();

            command.CommandText = "DROP DATABASE " + Name + ";";
            command.ExecuteNonQuery();

            command = connection.CreateCommand();
            command.CommandText = "CREATE DATABASE " + Name +
                " ON PRIMARY" +
                " (NAME = " + Name + "," +
                " FILENAME = '" + mdfPath + "')" +

                " LOG ON" +
                " (NAME = " + Name + "_log," +
                " FILENAME = '" + ldfPath + "')" +
                ";";
            command.ExecuteNonQuery();

            connection.ChangeDatabase("Enterprise");

            // DEFINITIONS TABLE
            command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE [dbo].[Definitions] (" + DEF_DEFINITIONS + ")";
            command.ExecuteNonQuery();

            // INSTRUMENTS TABLE
            command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE [dbo].[Instruments] (" + DEF_INSTRUMENTS + ")";
            command.ExecuteNonQuery();

            // MARKETS TABLE
            command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE [dbo].[Markets] (" + DEF_MARKETS + ")";
            command.ExecuteNonQuery();

            // SOURCES TABLE
            command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE [dbo].[Sources](" + DEF_SOURCES + ")";
            command.ExecuteNonQuery();

            // PRICES TABLE
            command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE [dbo].[Prices] (" + DEF_PRICES + ")";
            command.ExecuteNonQuery();

            // EVENTS TABLE
            command = connection.CreateCommand();
            command.CommandText = "CREATE TABLE [dbo].[Events] (" + DEF_EVENTS + ")";
            command.ExecuteNonQuery();
        }

        private void Connect(string connectionString)
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

        #region Getters

        public IEnumerable<Price> DownloadPrices(string instrument)
        {
            SqlCommand c = new SqlCommand("SELECT * FROM PRICES WHERE InstrumentID = @InstrumentID;", connection);
            c.Parameters.AddWithValue("@InstrumentID", instrument);
            c.Transaction = transaction;

            using (SqlDataReader r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    Price item = new Price()
                    {
                        Time = r.GetDateTime(0),
                        Instrument = r.GetString(1),
                        Market = r.GetString(2),
                        Value = r.GetDouble(3),
                        Volume = r.GetInt64(4),
                        Interval = (PriceInterval)r.GetByte(5)
                    };
                    yield return item;
                }
            }
        }

        public IEnumerable<Instrument> DownloadInstruments(string id = null)
        {
            SqlCommand c;

            if (id == null)
            {
                c = new SqlCommand("SELECT * FROM INSTRUMENTS;", connection);
            }
            else
            {
                c = new SqlCommand("SELECT * FROM INSTRUMENTS WHERE [ID]=@ID;", connection);
                c.Parameters.AddWithValue("@ID", id);
            }
            c.Transaction = transaction;

            using (SqlDataReader r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    Instrument item = new Instrument()
                    {
                        ID = r.GetString(0),
                        Ticker = r.GetString(1),
                        LongTicker = r.GetString(2),
                        Name = r.GetString(3),
                        Type = r.GetString(4)
                    };
                    yield return item;
                }
            }
        }

        public IEnumerable<Market> DownloadMarkets(string id = null)
        {
            SqlCommand c;

            if (id == null)
            {
                c = new SqlCommand("SELECT * FROM MARKETS;", connection);
            }
            else
            {
                c = new SqlCommand("SELECT * FROM MARKETS WHERE [ID]=@ID;", connection);
                c.Parameters.AddWithValue("@ID", id);
            }
            c.Transaction = transaction;

            using (SqlDataReader r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    Market item = new Market()
                    {
                        ID = r.GetString(0),
                        Country = r.GetString(1),
                        Name = r.GetString(2),
                        Type = r.GetString(3)
                    };
                    yield return item;
                }
            }
        }

        public int CountPrices()
        {
            SqlCommand c = new SqlCommand("SELECT COUNT([Value]) FROM PRICES;", connection)
            {
                Transaction = transaction
            };
            using (SqlDataReader r = c.ExecuteReader())
            {
                if (r.Read())
                {
                    return r.GetInt32(0);
                }
                else
                {
                    return -1;
                }
            }
        }

        public DateTime[] GetPriceDates(string instrument)
        {
            List<DateTime> output = new List<DateTime>();

            SqlCommand c = new SqlCommand("SELECT [Time] FROM PRICES WHERE [InstrumentID]=@InstrumentID;", connection);
            c.Parameters.Add("@InstrumentID", SqlDbType.VarChar);
            c.Parameters["@InstrumentID"].Value = instrument;
            c.Transaction = transaction;
            using (SqlDataReader r = c.ExecuteReader())
            {
                while (r.Read())
                {
                    output.Add(r.GetDateTime(0).Date);
                }
            }

            return output.Distinct().ToArray();
        }

        #endregion

        #region Setters

        public enum ReplacementMethod
        {
            IfMissing, Unspecified, Time, Instrument
        }

        public async Task<int> Upload(IEnumerable<Price> list, ReplacementMethod replacementMethod)
        {
            int output = 0;
            SqlCommand c = null;

            if (list.Count() == 0) return 0;

            bool clean = CountPrices() == 0;

            try
            {
                transaction = connection.BeginTransaction();

                await Task.Run(async () =>
                {
                    if (!clean)
                    {
                        switch (replacementMethod)
                        {
                            case ReplacementMethod.IfMissing:

                                string instrument = "";
                                DateTime[] dates = null;
                                List<Price> newList = new List<Price>();
                                foreach (var item in list)
                                {
                                    if (instrument == "" || item.Instrument != instrument)
                                    {
                                        instrument = item.Instrument;
                                        dates = GetPriceDates(instrument);
                                    }
                                    if (dates != null && Array.Exists(dates, x => item.Time.Date == x)) continue;
                                    newList.Add(item);
                                }
                                list = newList;
                                break;
                            case ReplacementMethod.Unspecified:

                                var tmi1 = list.Select(x => new { x.Time.Date, x.Instrument, x.Source }).Distinct();
                                c = new SqlCommand("DELETE FROM PRICES WHERE CAST([Time] AS DATE)=@Time AND [InstrumentID]=@InstrumentID AND [Source]=@Source;", connection);
                                c.Parameters.Add("@Time", SqlDbType.DateTime);
                                c.Parameters.Add("@InstrumentID", SqlDbType.VarChar);
                                c.Parameters.Add("@Source", SqlDbType.VarChar);
                                c.Transaction = transaction;

                                foreach (var i in tmi1)
                                {
                                    c.Parameters["@Time"].Value = i.Date;
                                    c.Parameters["@InstrumentID"].Value = i.Instrument;
                                    c.Parameters["@Source"].Value = i.Source;
                                    output += await c.ExecuteNonQueryAsync();
                                }

                                break;
                            case ReplacementMethod.Time:

                                var tmi2 = list.Select(x => new { x.Time.Date, x.Source }).Distinct();
                                c = new SqlCommand("DELETE FROM PRICES WHERE CAST([Time] AS DATE)=@Time AND [Source]=@Source;", connection);
                                c.Parameters.Add("@Time", SqlDbType.DateTime);
                                c.Parameters.Add("@Source", SqlDbType.VarChar);
                                c.Transaction = transaction;

                                foreach (var i in tmi2)
                                {
                                    c.Parameters["@Time"].Value = i.Date;
                                    c.Parameters["@Source"].Value = i.Source;
                                    output += await c.ExecuteNonQueryAsync();
                                }

                                break;
                            case ReplacementMethod.Instrument:

                                var tmi3 = list.Select(x => new { x.Instrument, x.Source }).Distinct();
                                c = new SqlCommand("DELETE FROM PRICES WHERE [InstrumentID]=@InstrumentID AND [Source]=@Source;", connection);
                                c.Parameters.Add("@InstrumentID", SqlDbType.VarChar);
                                c.Parameters.Add("@Source", SqlDbType.VarChar);
                                c.Transaction = transaction;

                                foreach (var i in tmi3)
                                {
                                    c.Parameters["@InstrumentID"].Value = i.Instrument;
                                    c.Parameters["@Source"].Value = i.Source;
                                    output += await c.ExecuteNonQueryAsync();
                                }

                                break;
                            default:
                                break;
                        }
                    }

                    int index = 0;
                    var data = list.ToArray();
                    c = new SqlCommand
                    {
                        Connection = connection,
                        Transaction = transaction
                    };
                    StringBuilder q = new StringBuilder();

                    const int MAX_PARAMETERS = 256;

                    while (index < data.Length)
                    {
                        if (index % MAX_PARAMETERS == 0)
                        {
                            if (index > 0)
                            {
                                q.Remove(q.Length - 2, 2);
                                q.Append(';');
                                c.CommandText = q.ToString();
                                output += await c.ExecuteNonQueryAsync();
                                q.Clear();
                            }

                            c.Parameters.Clear();
                            q.Append("INSERT INTO PRICES([Time], [InstrumentID], [MarketID], [Value], [Volume], [Interval], [Source]) VALUES ");
                        }

                        int queryIdx = index % MAX_PARAMETERS;
                        q.AppendFormat("(@Time{0}, @InstrumentID{0}, @MarketID{0}, @Value{0}, @Volume{0}, @Interval{0}, @Source{0}), ", queryIdx);
                        if (data[index].Instrument.Length > 12)
                        {
                            throw new Exception("Length exceeded for Price.Instrument" + data[index].Instrument);
                        }
                        if (data[index].Market.Length > 12)
                        {
                            throw new Exception("Length exceeded for Price.Market" + data[index].Market);
                        }
                        c.Parameters.AddWithValue("@Time" + queryIdx, data[index].Time);
                        c.Parameters.AddWithValue("@InstrumentID" + queryIdx, data[index].Instrument);
                        c.Parameters.AddWithValue("@MarketID" + queryIdx, data[index].Market);
                        c.Parameters.AddWithValue("@Value" + queryIdx, data[index].Value);
                        c.Parameters.AddWithValue("@Volume" + queryIdx, data[index].Volume);
                        c.Parameters.AddWithValue("@Interval" + queryIdx, (byte)data[index].Interval);
                        c.Parameters.AddWithValue("@Source" + queryIdx, data[index].Source);
                        index++;
                    }

                    if (q.Length >= 2)
                    {
                        q.Remove(q.Length - 2, 2);
                        q.Append(';');
                        c.CommandText = q.ToString();
                        output += await c.ExecuteNonQueryAsync();
                        q.Clear();
                    }
                });

                transaction.Commit();
                transaction = null;
            }
            catch (SqlException ex)
            {
                throw new DataserverException(ex.Message, null, ex, c);
            }

            return output;
        }

        public async Task<int> Upload(IEnumerable<Instrument> list)
        {
            int output = 0;
            SqlCommand c = null;

            try
            {

                transaction = connection.BeginTransaction();

                c = new SqlCommand
                {
                    Connection = connection,
                    Transaction = transaction,
                    CommandText = "CREATE TABLE [dbo].[InstrumentsNew] (" + DEF_INSTRUMENTS + ")"
                };
                c.ExecuteNonQuery();

                c = new SqlCommand("INSERT INTO InstrumentsNew ([ID], [Ticker], [LongTicker], [Name], [Type]) VALUES (@ID, @Ticker, @LongTicker, @Name, @Type);", connection);
                c.Parameters.AddWithValue("@ID", "");
                c.Parameters.AddWithValue("@Ticker", "");
                c.Parameters.AddWithValue("@LongTicker", "");
                c.Parameters.AddWithValue("@Name", "");
                c.Parameters.AddWithValue("@Type", "");
                c.Transaction = transaction;

                foreach (var li in list)
                {
                    if (li.ID.Length > 12)
                    {
                        throw new Exception("Length exceeded for Instrument.ID:" + li.ID);
                    }
                    if (li.Ticker.Length > 16)
                    {
                        throw new Exception("Length exceeded for Instrument.Ticker:" + li.ID);
                    }
                    if (li.LongTicker.Length > 32)
                    {
                        throw new Exception("Length exceeded for Instrument.LongTicker:" + li.ID);
                    }
                    if (li.Type.Length > 32)
                    {
                        throw new Exception("Length exceeded for Instrument.Type:" + li.ID);
                    }
                    c.Parameters["@ID"].Value = li.ID;
                    c.Parameters["@Ticker"].Value = li.Ticker;
                    c.Parameters["@LongTicker"].Value = li.LongTicker;
                    c.Parameters["@Name"].Value = li.Name;
                    c.Parameters["@Type"].Value = li.Type;
                    output += await c.ExecuteNonQueryAsync();
                }

                c = new SqlCommand
                {
                    Connection = connection,
                    Transaction = transaction,
                    CommandText = @"
                MERGE [dbo].[Instruments] AS O
                USING [dbo].[InstrumentsNew] AS N
                ON O.ID = N.ID
                WHEN MATCHED THEN
                    UPDATE SET O.Ticker = N.Ticker, O.LongTicker = N.LongTicker, O.Name = N.Name, O.Type = N.Type
                WHEN NOT MATCHED THEN
                    INSERT ([ID], [Ticker], [LongTicker], [Name], [Type]) VALUES (N.ID, N.Ticker, N.LongTicker, N.Name, N.Type);
                DROP TABLE [dbo].[InstrumentsNew];"
                };
                await c.ExecuteNonQueryAsync();

                transaction.Commit();

                //instruments = null;
                transaction = null;
            }
            catch (SqlException ex)
            {
                throw new DataserverException(ex.Message, null, ex, c);
            }

            return output;
        }

        public async Task<int> Upload(IEnumerable<Market> list)
        {
            int output = 0;
            SqlCommand c = null;

            try
            {
                var ids = list.Select(x => x.ID).Distinct();

                c = new SqlCommand
                {
                    Connection = connection,
                    Transaction = transaction,
                    CommandText = "CREATE TABLE [dbo].[MarketsNew] (" + DEF_MARKETS + ")"
                };
                c.ExecuteNonQuery();

                c = new SqlCommand("INSERT INTO MarketsNew ([ID], [Country], [Name], [Type]) VALUES (@ID, @Country, @Name, @Type);", connection);
                c.Parameters.AddWithValue("@ID", "");
                c.Parameters.AddWithValue("@Country", "");
                c.Parameters.AddWithValue("@Name", "");
                c.Parameters.AddWithValue("@Type", "");
                c.Transaction = transaction;

                foreach (var li in list)
                {
                    if (li.ID.Length > 12)
                    {
                        throw new Exception("Length exceeded for Market.ID:" + li.ID);
                    }
                    if (li.Country.Length > 2)
                    {
                        throw new Exception("Length exceeded for Market.Country:" + li.ID);
                    }
                    if (li.Type.Length > 32)
                    {
                        throw new Exception("Length exceeded for Market.Type:" + li.ID);
                    }
                    c.Parameters["@ID"].Value = li.ID;
                    c.Parameters["@Country"].Value = li.Country;
                    c.Parameters["@Name"].Value = li.Name;
                    c.Parameters["@Type"].Value = li.Type;
                    output += await c.ExecuteNonQueryAsync();
                }

                c = new SqlCommand
                {
                    Connection = connection,
                    Transaction = transaction,
                    CommandText = @"
                MERGE [dbo].[Markets] AS O
                USING [dbo].[MarketsNew] AS N
                ON O.ID = N.ID
                WHEN MATCHED THEN
                    UPDATE SET O.Country = N.Country, O.Name = N.Name, O.Type = N.Type
                WHEN NOT MATCHED THEN
                    INSERT ([ID], [Country], [Name], [Type]) VALUES (N.ID, N.Country, N.Name, N.Type);
                DROP TABLE [dbo].[MarketsNew];"
                };
                c.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new DataserverException(ex.Message, null, ex, c);
            }

            return output;
        }

        public async Task<int> Upload(IEnumerable<MarketEvent> list, ReplacementMethod replacementMethod)
        {
            int output = 0;
            SqlCommand c = null;

            if (list.Count() == 0) return 0;

            try
            {
                transaction = connection.BeginTransaction();

                await Task.Run(async () =>
                {
                    switch (replacementMethod)
                    {
                        case ReplacementMethod.IfMissing:
                            throw new Exception("ReplacementMethod.IfMissing is invalid for Events upload.");
                        case ReplacementMethod.Unspecified:

                            var tmi1 = list.Select(x => new { x.Time.Date, x.Instrument, x.Source }).Distinct();
                            c = new SqlCommand("DELETE FROM EVENTS WHERE CAST([Time] AS DATE)=@Time AND [InstrumentID]=@InstrumentID AND [Source]=@Source;", connection);
                            c.Parameters.Add("@Time", SqlDbType.DateTime);
                            c.Parameters.Add("@InstrumentID", SqlDbType.VarChar);
                            c.Parameters.Add("@Source", SqlDbType.VarChar);
                            c.Transaction = transaction;

                            foreach (var i in tmi1)
                            {
                                c.Parameters["@Time"].Value = i.Date;
                                c.Parameters["@InstrumentID"].Value = i.Instrument;
                                c.Parameters["@Source"].Value = i.Source;
                                output += await c.ExecuteNonQueryAsync();
                            }

                            break;
                        case ReplacementMethod.Time:
                            throw new Exception("ReplacementMethod.Time is invalid for Events upload.");
                        case ReplacementMethod.Instrument:

                            var tmi3 = list.Select(x => new { x.Instrument, x.Source }).Distinct();
                            c = new SqlCommand("DELETE FROM EVENTS WHERE [InstrumentID]=@InstrumentID AND [Source]=@Source;", connection);
                            c.Parameters.Add("@InstrumentID", SqlDbType.VarChar);
                            c.Parameters.Add("@Source", SqlDbType.VarChar);
                            c.Transaction = transaction;

                            foreach (var i in tmi3)
                            {
                                c.Parameters["@InstrumentID"].Value = i.Instrument;
                                c.Parameters["@Source"].Value = i.Source;
                                output += await c.ExecuteNonQueryAsync();
                            }

                            break;
                        default:
                            break;
                    }

                    foreach (var li in list)
                    {
                        c = new SqlCommand
                        {
                            Connection = connection,
                            CommandText = "INSERT INTO Events ([Time], [InstrumentID], [Type], [Value], [Value2], [Source]) VALUES (@Time, @InstrumentID, @Type, @Value, @Value2, @Source);"
                        };
                        c.Parameters.AddWithValue("@Time", "");
                        c.Parameters.AddWithValue("@InstrumentID", "");
                        c.Parameters.AddWithValue("@Type", "");
                        c.Parameters.AddWithValue("@Value", "");
                        c.Parameters.AddWithValue("@Value2", "");
                        c.Parameters.AddWithValue("@Source", "");
                        c.Transaction = transaction;
                        if (li.Instrument.Length > 64)
                        {
                            throw new Exception("Length exceeded for Event.InstrumentID" + li.Instrument);
                        }
                        if (li.Type.Length > 32)
                        {
                            throw new Exception("Length exceeded for Event.Type" + li.Type);
                        }
                        if (li.Source.Length > 12)
                        {
                            throw new Exception("Length exceeded for Event.Source" + li.Source);
                        }
                        c.Parameters["@Time"].Value = li.Time;
                        c.Parameters["@InstrumentID"].Value = li.Instrument;
                        c.Parameters["@Type"].Value = li.Type;
                        c.Parameters["@Value"].Value = li.Value;
                        c.Parameters["@Value2"].Value = li.Value2;
                        c.Parameters["@Source"].Value = li.Source;

                        output += await c.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                    transaction = null;
                });
            }
            catch (SqlException ex)
            {
                throw new DataserverException(ex.Message, null, ex, c);
            }

            return output;
        }

        public async Task<int> UploadSources(IEnumerable<string> list)
        {
            int output = 0;
            SqlCommand c = null;

            try
            {
                c = new SqlCommand
                {
                    Connection = connection,
                    Transaction = transaction,
                    CommandText = "CREATE TABLE [dbo].[SourcesNew] (" + DEF_SOURCES + ")"
                };
                c.ExecuteNonQuery();

                c = new SqlCommand("INSERT INTO SourcesNew ([ID], [Name]) VALUES (@ID, @Name);", connection);
                c.Parameters.AddWithValue("@ID", "");
                c.Parameters.AddWithValue("@Name", "");
                c.Transaction = transaction;

                foreach (var li in list)
                {
                    if (li.Length > 12)
                    {
                        throw new Exception("Length exceeded for Source.ID:" + li);
                    }
                    c.Parameters["@ID"].Value = li;
                    c.Parameters["@Name"].Value = li;
                    output += await c.ExecuteNonQueryAsync();
                }

                c = new SqlCommand
                {
                    Connection = connection,
                    Transaction = transaction,
                    CommandText = @"
                MERGE [dbo].[Sources] AS O
                USING [dbo].[SourcesNew] AS N
                ON O.ID = N.ID
                WHEN MATCHED THEN
                    UPDATE SET O.Name = N.Name
                WHEN NOT MATCHED THEN
                    INSERT ([ID], [Name]) VALUES (N.ID, N.Name);
                DROP TABLE [dbo].[SourcesNew];"
                };
                c.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new DataserverException(ex.Message, null, ex, c);
            }

            return output;
        }

        #endregion

        public DateTime GetLastUpdate(string name)
        {
            SqlCommand c = new SqlCommand("SELECT [Value] FROM DEFINITIONS WHERE [Key]=@Key;", connection);
            c.Parameters.AddWithValue("@Key", "LastUpdate_" + name + "");

            using (SqlDataReader r = c.ExecuteReader())
            {
                if (r.Read())
                {
                    if (DateTime.TryParse(r.GetString(0), out DateTime result))
                        return result;
                    else
                        return DateTime.MinValue;
                }
                else
                {
                    return DateTime.MinValue;
                }
            }
        }

        public void ReportLastUpdate(string name, DateTime timestamp)
        {
            SqlCommand c;

            c = new SqlCommand
            {
                Connection = connection,
                Transaction = transaction,
                CommandText = "CREATE TABLE [dbo].[DefinitionsNew] (" + DEF_DEFINITIONS + ")"
            };
            c.ExecuteNonQuery();

            c = new SqlCommand("INSERT INTO DefinitionsNew ([Key], [Value]) VALUES (@Key, @Value);", connection);
            c.Parameters.AddWithValue("@Key", "LastUpdate_" + name);
            c.Parameters.AddWithValue("@Value", timestamp.ToString());
            c.Transaction = transaction;
            c.ExecuteNonQuery();

            c = new SqlCommand
            {
                Connection = connection,
                Transaction = transaction,
                CommandText = @"
                MERGE [dbo].[Definitions] AS O
                USING [dbo].[DefinitionsNew] AS N
                ON O.[Key] = N.[Key]
                WHEN MATCHED THEN
                    UPDATE SET O.[Value] = N.[Value]
                WHEN NOT MATCHED THEN
                    INSERT ([Key], [Value]) VALUES (N.[Key], N.[Value]);
                DROP TABLE [dbo].[DefinitionsNew];"
            };
            c.ExecuteNonQuery();
        }

        public void Dispose()
        {
            connection.Close();
        }

        private const string DEF_DEFINITIONS = @"
                [Key] VARCHAR(32) NOT NULL PRIMARY KEY, 
                [Value] NVARCHAR(MAX) NULL";
        private const string DEF_INSTRUMENTS = @"
                [ID] VARCHAR(64) NOT NULL PRIMARY KEY, 
                [Ticker] VARCHAR(16) NULL, 
                [LongTicker] VARCHAR(32) NULL, 
                [Name] NVARCHAR(MAX) NULL, 
                [Type] VARCHAR(32) NULL";
        private const string DEF_MARKETS = @"
                [ID] VARCHAR(12) NOT NULL PRIMARY KEY, 
                [Country] CHAR(2) NULL, 
                [Name] NVARCHAR(MAX) NULL, 
                [Type] VARCHAR(32) NULL";
        private const string DEF_SOURCES = @"
	            [ID] VARCHAR(12) NOT NULL PRIMARY KEY,
                [Name] NVARCHAR(MAX) NULL";
        private const string DEF_PRICES = @"
                [Time] DATETIME2(0) NOT NULL, 
                [InstrumentID] VARCHAR(64) NOT NULL, 
                [MarketID] VARCHAR(12) NOT NULL,                 
	            [Value] FLOAT NULL, 
                [Volume] BIGINT NULL, 
                [Interval] TINYINT NULL, 
                [Source] VARCHAR(12) NOT NULL, 
                CONSTRAINT [FK_Prices_ToInstruments] FOREIGN KEY ([InstrumentID]) REFERENCES [Instruments]([ID]),
                CONSTRAINT [FK_Prices_ToMarkets] FOREIGN KEY ([MarketID]) REFERENCES [Markets]([ID]),
                CONSTRAINT [FK_Prices_ToSources] FOREIGN KEY ([Source]) REFERENCES [Sources]([ID])";
        private const string DEF_EVENTS = @"
                [Time] DATETIME2(0) NOT NULL,
                [InstrumentID] VARCHAR(64) NOT NULL,
                [Type] VARCHAR(32) NOT NULL,
                [Value] FLOAT NULL,
                [Value2] FLOAT NULL,
                [Source] VARCHAR(12) NOT NULL
                CONSTRAINT [FK_Events_ToInstruments] FOREIGN KEY ([InstrumentID]) REFERENCES [Instruments]([ID]),
                CONSTRAINT [FK_Events_ToSources] FOREIGN KEY ([Source]) REFERENCES [Sources]([ID])";
    }

    public static class StringExtensions
    {
        public static string Truncate(this string input, int length)
        {
            if (input.Length <= length)
                return input;
            else
                return input.Substring(0, length);
        }
    }
}
