﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Market : IEquatable<Market>
    {
        string id;
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value?.Substring(0, Math.Min(value.Length, 12)) ?? "";
            }
        }

        string country;
        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value?.Substring(0, Math.Min(value.Length, 2)) ?? "";
            }
        }

        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value?.Substring(0, Math.Min(value.Length, 128)) ?? "";
            }
        }

        string type;
        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value?.Substring(0, Math.Min(value.Length, 12)) ?? "";
            }
        }

        public override string ToString()
        {
            return ID;
        }

        public bool Equals(Market other)
        {
            if (ID==null) return false;
            return ID.Equals(other);
        }
    }
}
