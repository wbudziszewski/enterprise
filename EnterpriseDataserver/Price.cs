﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public enum PriceInterval
    {
        Month,
        Day,
        Hour,
        Quarter,
        Minute,
        Second
    }

    public class Price : IEquatable<Price>
    {
        public DateTime Time { get; set; }
        public string Instrument { get; set; }
        public string Market { get; set; }
        public double Value { get; set; }
        public long Volume { get; set; }
        public PriceInterval Interval { get; set; }
        public string Source { get; set; }

        public bool Equals(Price other)
        {
            if (Time == DateTime.MinValue || Instrument == null || Market == null) return false;
            return Time.Equals(other.Time) && Instrument.Equals(other.Instrument) && Market.Equals(other.Market);
        }

        public override string ToString()
        {
            return Time.ToString() + ";" + Market + Instrument;
        }
    }
}
