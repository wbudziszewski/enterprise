﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Budziszewski.Enterprise.Dataserver
{
    public class Instrument : IEquatable<Instrument>, IEqualityComparer<Instrument>
    {
        string id;
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value?.Substring(0, Math.Min(value.Length, 12)) ?? "";
            }
        }

        string ticker;
        public string Ticker
        {
            get
            {
                return ticker;
            }
            set
            {
                ticker = value?.Substring(0, Math.Min(value.Length, 50)) ?? "";
            }
        }

        string longticker;
        public string LongTicker
        {
            get
            {
                return longticker;
            }
            set
            {
                longticker = value?.Substring(0, Math.Min(value.Length, 50)) ?? "";
            }
        }

        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value?.Substring(0, Math.Min(value.Length, 128)) ?? "";
            }
        }

        string type;
        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value?.Substring(0, Math.Min(value.Length, 12)) ?? "";
            }
        }

        public override string ToString()
        {
            return ID;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Instrument item))
            {
                return false;
            }

            return Equals(item);
        }

        public bool Equals(Instrument other)
        {
            if (other == null) return false;
            if (ID == null) return false;
            return ID.Equals(other.ID);
        }

        public bool Equals(Instrument x, Instrument y)
        {
            if (x.ID == null) return false;
            if (y == null) return false;
            return x.ID.Equals(y.ID);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public int GetHashCode(Instrument obj)
        {
            int a = ID?.GetHashCode() ?? 0;
            int b = obj?.ID?.GetHashCode() ?? 0;
            return a ^ b;
        }
    }
}
