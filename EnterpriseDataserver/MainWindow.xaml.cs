﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Budziszewski.Enterprise.Dataserver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //"Server=localhost\SQLEXPRESS;AttachDbFilename='C:\Users\Witold\Documents\Programowanie\Bazy danych\Enterprise.mdf';Database=Enterprise;Trusted_Connection=True;User Instance=True;"
        public ObservableCollection<DataserverTask> Tasks { get; set; } = new ObservableCollection<DataserverTask>();
        public ObservableCollection<string> Info { get; set; } = new ObservableCollection<string>();

        public Database DB { get; set; }
        public System.Timers.Timer StatusResetTimer = new System.Timers.Timer()
        {
            Interval = 1000,
            AutoReset = false
        };

        public int Granularity { get; set; } = 1;

        public List<Instrument> Instruments = new List<Instrument>();
        public List<Market> Markets = new List<Market>();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            StatusResetTimer.Elapsed += StatusResetTimer_Elapsed;
        }

        private void StatusResetTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher?.Invoke(() => Status.Text = "");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Directory.Exists(Path.Combine(Path.GetTempPath(), "EnterpriseDataserver")))
                    Directory.Delete(Path.Combine(Path.GetTempPath(), "EnterpriseDataserver"), true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error deleting temporary files: " + ex, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            try
            {
                SetUpCLA();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in command line arguments: " + ex, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            CreateMarkets();
            Granularity = 1;
            Import.Progress += Import_Progress;
        }

        private void CreateMarkets()
        {
            Markets = new List<Market>() {
                new Market() { ID = "PL_GPW", Country = "PL", Name = "Giełda Papierów Wartościowych w Warszawie S.A.", Type = "regmarket" },
                new Market() { ID = "PL_CATALYST", Country = "PL", Name = "Rynek obligacji Catalyst", Type = "mltplfmarket" },
                new Market() { ID = "PL_BOSFUND", Country = "PL", Name = "Platforma obrotu Bossafund.pl", Type = "issuerprice" }
            };
        }

        private void Import_Progress(Import.ProgressEventArgs e)
        {
            Status.Text = e.Message;
            StatusResetTimer.Start();
        }

        private void ReportInfo(string message)
        {
            Dispatcher.Invoke(() => Info?.Add(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + message));
        }

        private void SetUpCLA()
        {
            var cla = Environment.GetCommandLineArgs();
            foreach (var arg in cla)
            {
                if (arg.ToLower().StartsWith("/connectionstring="))
                {
                    ConnectionString.Text = arg.Remove(0, "/connectionstring=".Length).Trim();
                }
                if (arg.ToLower().StartsWith("/instruments="))
                {
                    InstrumentsPath.Text = arg.Remove(0, "/instruments=".Length).Trim();
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            int failsafe = 10;
            while (Directory.Exists(Path.Combine(Path.GetTempPath(), "EnterpriseDataserver")) && failsafe-- > 0)
            {
                try
                {
                    Thread.Sleep(100);
                    if (Directory.Exists(Path.Combine(Path.GetTempPath(), "EnterpriseDataserver")))
                        Directory.Delete(Path.Combine(Path.GetTempPath(), "EnterpriseDataserver"), true);
                }
                catch (Exception ex)
                {
                    if (failsafe == 0)
                        MessageBox.Show("Could not delete temporary files: " + ex, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private async void Equity1Import_Click(object sender, RoutedEventArgs e)
        {
            await Import.ImportEquityInfostrefa();
        }

        private async void Bonds1Import_Click(object sender, RoutedEventArgs e)
        {
            await Import.ImportBondsObligacje();
        }

        private async void Funds1Import_Click(object sender, RoutedEventArgs e)
        {
            await Import.ImportFundsBossafund();
        }

        private async void CleanDatabase_Click(object sender, RoutedEventArgs e)
        {
            Status.Text = "Database cleanup in progress...";
            await DB.Clean();
            ReportInfo("Database cleanup completed.");
            Status.Text = "Database cleanup completed.";
            StatusResetTimer.Start();
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            if (DB == null)
            {
                if (String.IsNullOrEmpty(ConnectionString.Text))
                {
                    MessageBox.Show("No connection string specified.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                try
                {
                    DB = new Database(ConnectionString.Text);
                    ReportInfo("Connected to the database.");
                }
                catch (Exception ex)
                {
                    ReportInfo("Error connecting to the database: " + ex.Message);
                    return;
                }

                try
                {
                    Instruments = Import.Load(InstrumentsPath.Text);
                    var dictAll = BuildInstrumentsDictionary(Instruments);
                    var dictEquity = BuildInstrumentsDictionary(Instruments.Where(x => x.Type == "equity").ToList());
                    var dictBonds = BuildInstrumentsDictionary(Instruments.Where(x => x.Type == "bond").ToList());
                    Tasks.Add(new Prices_bossa_mstall(DB, dictEquity));
                    Tasks.Add(new Prices_bossa_mstall_endofday(DB, dictEquity));
                    Tasks.Add(new Prices_bossa_mstobl(DB, dictBonds));
                    Tasks.Add(new Prices_bossa_mstfun(DB));
                    Tasks.Add(new Events_gpw(DB, dictEquity));
                }
                catch (Exception ex)
                {
                    ReportInfo("Error creating tasks: " + ex.Message);
                    ReportInfo(ex.Message);
                }

                Connect.Header = "Disconnect";
                CleanDatabase.IsEnabled = true;
                Start.IsEnabled = true;
            }
            else
            {
                DB = null;
                Connect.Header = "Connect";
                CleanDatabase.IsEnabled = false;
                Start.IsEnabled = false;
            }
        }

        private Dictionary<string, string> BuildInstrumentsDictionary(List<Instrument> instruments)
        {
            Dictionary<string, string> output = new Dictionary<string, string>();
            foreach (var item in instruments)
            {
                if (output.ContainsKey(item.Ticker)) continue;
                output.Add(item.LongTicker, item.ID);
            }
            return output;
        }

        private async void Start_Click(object sender, RoutedEventArgs e)
        {
            if (DB == null) return;
            try
            {
                Start.IsEnabled = false;

                ReportInfo("Database update started.");

                await DB.Upload(Markets);
                await DB.UploadSources(new string[] { "bossa_mstall", "bossa_mstobl", "bossa_mstfun", "gpwnotoria" });
                await DB.Upload(Instruments.Distinct());

                ReportInfo("General maintenance completed.");

                foreach (var t in Tasks.Where(x => x.IsEnabled))
                {
                    t.ReportInfo = ReportInfo;
                    t.Granularity = (PriceInterval)Granularity;
                    await t.Run();
                }

                ReportInfo("Database commit finished");
            }
            catch (DataserverException ex)
            {
                ReportInfo(ex.Message);
                (ex.Task).Progress = 0;
                (ex.Task).Status = "Error";
            }
            finally
            {
                Start.IsEnabled = true;
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void InstrumentsBrowse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();

            if (dialog.ShowDialog() == true)
            {
                InstrumentsPath.Text = dialog.FileName;
            }
        }

        private void GranularityChange(object sender, RoutedEventArgs e)
        {
            int count = ((MenuItem)((Control)e.Source).Parent).Items.Count;
            for (int i = 0; i < count; i++)
            {
                MenuItem mi = (MenuItem)((MenuItem)((Control)e.Source).Parent).Items[i];
                if (mi == e.Source)
                {
                    Granularity = i;
                    mi.IsChecked = true;
                }
                else
                {
                    mi.IsChecked = false;
                }
            }
        }
    }
}
